<?php 
include('../includes/header.php');
// $route_histories = vcGetRouteHistories($_SESSION['access-token']);
// $history = vcGetRouteHistory($_SESSION['access-token'], $_GET['id']);
$hoy = date("D");
//$route = vcGetRouteSchedule($_SESSION['access-token'], $_GET['id']);
$sites = vcGetSitesWhitFrequency($_SESSION['access-token'], $_GET['id'],$hoy);
$branches = $sites['branches'];
$user = $sites['user'][0];
$today_sites = $sites['sites'];
$clients = $sites['clients'];

// $branches = vcGetBranches($_SESSION['access-token']);
// $clients = vcGetClients($_SESSION['access-token']);
//$users = vcGetUsers($_SESSION['access-token']);

// $filter = 
// $routes_today = getSitesWhitFrequency($_SESSION['access-token'], $_GET['id'],$filter)

?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
  <!-- <pre>
    <?php print_r($sites)  ?>
  </pre> -->
  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">
           Details of Route
            <a class="btn btn-primary" onclick="javascript:window.print();">Imprimir</a>
          </h3>
        </div>
      </div>
    </div>
    <div class="m-subheader ">
      <div class="d-flex align-items-center" style="background-color: white;padding: 10px;box-shadow: 0px 3px 14px #9a9caf;">
        <div class="mr-auto">
        <h3>Sites For Today:</h3>
        </div>
      </div>
    </div>


    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      <div class="row">
        <div class="col-xl-12">

          <!-- <input type="hidden" id="route_schedule_id" value="<?php echo $_GET['id'] ?>"> -->

          <!-- <div class="form-group m-form__group">
						<label for="">Days</label>
                        <select class="form-control m-input" id="frequency_filter" name="frequency_filter" onchange="filterRouteSchedule(event)">
                            <option value="">Select option</option>
                            <option value="Mon">Monday</option>
                            <option value="Tue">Tuesday</option>
							<option value="Wen">Wednesday</option>
							<option value="Thu">Thursday</option>
							<option value="Fri">Friday</option>
							<option value="Sat">Saturday</option>
							<option value="Sun">Sunday</option>
                        </select>
                     </div> -->

          <!--Begin::Section-->
          <div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
              <div class="row m-row--no-padding m-row--col-separator-xl">
              
                <!--begin:: ROW -->
        <?php if(!empty($today_sites)) {  ?>
          <?php foreach ($today_sites as $site) { ?>
                <div class="col-md-4 col-lg-4 col-xl-4" style="border-bottom: 10px solid #ddd;">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Site:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            
                            <?= $site['name'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Max Time:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            
                            <?php $time = $site['max_time'] ?>
                            <?= $time ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>
                <!--end:: Col-md-4 -->

                <div class="col-md-4 col-lg-4 col-xl-4" style="border-bottom: 10px solid #ddd;">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Days for week:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $site['weekly_frequency'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Addess:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $site['address'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>
                <!--end:: Col-md-4 -->

                <div class="col-md-4 col-lg-4 col-xl-4" style="border-bottom: 10px solid #ddd;">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Status:</span>
                          <h3 class="m-widget1__title m--font-brand"><?= $site['status']?></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Cleaner:</span>
                          <h3 class="m-widget1__title m--font-brand"><?= $user['first_name']?></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>
                <!--end:: Col-md-4 -->

                <?php } ?>
              <?php }else{ ?>
               <div>
                  <h4 style="padding:10px; color:red">The Sites for today not exist.</h4>
               </div>
              <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    All sites for this Route
                                </h3>
                            </div>
                        </div>
                        
                    </div>

                    <div class="m-portlet__body">

                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Site Code</th>
                                            <th>Name</th>
                                            <th>Client</th>
                                            <th>Assigned Employee</th>
                                            <th>Weekly frequency</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($branches as $key => $branch) {
                                        ?>
                                        <tr>                                           
                                            <td scope="row">
                                                <a href="../branches/show.php?id=<?= $branch['id'] ?>">
                                                    TSP9
                                                </a>
                                            </td>
                                            <td>
                                                <a href="../branches/show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="../branches/show.php?id=<?= $branch['id'] ?>">
                                              <?php
                                                $client_name;
                                                foreach ($clients as $client) {
                                                  if ($client['id'] == $branch['client_id'] ) {
                                                    $client_name = $client['name'];
                                                  }
                                                }?>
                                                    <?= $client_name ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="../branches/show.php?id=<?= $branch['id'] ?>">
                                                    <?= $user['first_name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                              <p> <?= $branch['weekly_frequency'] ?> </p>
                                            </td>
                                              
                                            <td>
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                        data-toggle="dropdown" aria-expanded="true">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="../branches/show.php?id=<?= $branch['id'] ?>"><i class="la la-crosshairs"></i>
                                                            View</a>
                                                        <a class="dropdown-item" href="../branches/update.php?id=<?= $branch['id'] ?>"><i class="la la-edit"></i>
                                                            Update</a>
                                                        <!-- <a onclick="deleteBranch(event, <?= $branch['id'] ?>)" class="dropdown-item"><i class="la la-trash"></i>
                                                            Delete</a> -->
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>
  <!--End::Content-->
  <!-- end:: Page -->

  <!-- end::Quick Sidebar -->
  <?php include('../includes/footer.php');?>

  <!--begin::CLIENTS -->
  <!-- <script src="../assets/js/route_schedules/filter.js" type="text/javascript"></script> -->
  <!--end::CLIENTS -->