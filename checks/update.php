<?php
date_default_timezone_set('America/Chicago');
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3) {
    $branch = vcGetBranch($_SESSION['access-token'], $_GET['id']);
    $clients = vcGetClients($_SESSION['access-token']);
    $service_types = vcGetServiceTypes($_SESSION['access-token']);
    $cleaners = vcGetCleaners($_SESSION['access-token']);
    $area_managers = vcGetAreaManagers($_SESSION['access-token']);
    $supervisors = vcGetSupervisors($_SESSION['access-token']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Site: <?php echo $branch['name'] ?> </h3>
                <?php if($cleaners == null){ ?>
                    <div class="alert alert-danger" role="alert">
                        Cleaners should exist
                    </div>
                    <input type="text" id="cleaners_exists" value="0" style="display:none">
                <?php }else{ ?>
                    <input type="text" id="cleaners_exists" value="1" style="display:none">
                <?php } ?>

                <?php if($area_managers == null){ ?>
                    <div class="alert alert-danger" role="alert">
                        Area Managers should exist
                    </div>
                    <input type="text" id="area_managers_exists" value="0" style="display:none">
                <?php }else{ ?>
                    <input type="text" id="area_managers_exists" value="1" style="display:none">
                <?php } ?>

                <?php if($supervisors == null){ ?>
                    <div class="alert alert-danger" role="alert">
                        Supervisors should exist
                    </div>
                    <input type="text" id="supervisors_exists" value="0" style="display:none">
                <?php }else{ ?>
                    <input type="text" id="supervisors_exists" value="1" style="display:none">
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__body">
                        <!--begin::Form-->
                        <form class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group">
                                    <label for="name">Site Code</label>
                                    <input type="text" class="form-control m-input" id="site_code"
                                        placeholder="Site Code" value="<?php echo $branch['site_code'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="clients">Clients</label>                      
                                    <select class="form-control m-input" id="client_id" name="client_id">  
                                        <?php foreach ($clients as $key => $client) { ?>                                     
                                            <option value="<?php echo $client['id'] ?>"                                                    
                                                <?php if($branch['client']['id'] == $client['id']){ ?>
                                                    selected
                                                <?php } ?>
                                            ><?php echo $client['name'] ?></option>
                                        <?php } ?>
                                    </select>                
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="clients">Cleaners</label>
                                    <p id="cleaner_should_exist" style="display:none; color:red">Cleaners should exist</p>                      
                                    <select class="form-control m-input" id="cleaner_id" name="cleaner_id">  
                                        <?php foreach ($cleaners as $key => $cleaner) { ?>                                     
                                            <option value="<?php echo $cleaner['id'] ?>"                                                    
                                                <?php if(!empty($branch['cleaner']['id']) && ($branch['cleaner']['id'] == $cleaner['id'])){ ?>
                                                    selected
                                                <?php } ?>
                                            ><?php echo $cleaner['first_name'].' '.$cleaner['last_name'] ?></option>
                                        <?php } ?>
                                    </select>                 
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="clients">Area Managers</label>
                                    <p id="area_manager_should_exist" style="display:none; color:red">Area Manager should exist</p>                         
                                    <select class="form-control m-input" id="area_manager_id" name="area_manager_id">  
                                        <?php foreach ($area_managers as $key => $area) { ?>                                     
                                            <option value="<?php echo $area['id'] ?>"                                                    
                                                <?php if(!empty($branch['area_manager']['id']) && ($branch['area_manager']['id'] == $area['id'])){ ?>
                                                    selected
                                                <?php } ?>
                                            ><?php echo $area['first_name'].' '.$area['last_name'] ?></option>
                                        <?php } ?>
                                    </select>                
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="clients">Supervisors</label>       
                                    <p id="supervisor_should_exist" style="display:none; color:red">Supervisors should exist</p>                 
                                    <select class="form-control m-input" id="supervisor_id" name="supervisor_id">  
                                        <?php foreach ($supervisors as $key => $sup) { ?>                                     
                                            <option value="<?php echo $sup['id'] ?>"                                                    
                                                <?php if(!empty($branch['supervisor']['id']) && ($branch['supervisor']['id'] == $sup['id'])){ ?>
                                                    selected
                                                <?php } ?>
                                            ><?php echo $sup['first_name'].' '.$sup['last_name'] ?></option>
                                        <?php } ?>
                                    </select>                
                                </div>
                                <div class="m-form__group form-group">
                                    <h4>Frequency</h4>
                                    <div class="m-checkbox-list">                                 
                                        <?php $frequencies = explode(",", $branch['frequency']); ?>  
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="1"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 1){
                                                    echo "checked";
                                                }}?>                                         
                                                >Monday
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="2"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 2){
                                                    echo "checked";
                                                }}?>                                         
                                                >Tuesday
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="3"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 3){
                                                    echo "checked";
                                                }}?>                                         
                                                >Wednesday
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="4"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 4){
                                                    echo "checked";
                                                }}?>                                         
                                                >Thursday
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="5"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 5){
                                                    echo "checked";
                                                }}?>                                         
                                                >Friday
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="6"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 6){
                                                    echo "checked";
                                                }}?>                                         
                                                >Saturday
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" class="check_frequency" data-id="7"                                        
                                            <?php foreach($frequencies as $key => $freq){ ?>                        
                                                <?php if($freq == 7){
                                                    echo "checked";
                                                }}?>                                         
                                                >Sunday
                                            <span></span>
                                        </label>                                                                                                                                                       														
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="weekly_frequency">Weekly Frequency</label>
                                    <input type="number" class="form-control m-input" id="weekly_frequency" placeholder="Weekly Frequency"
                                        value="<?php echo $branch['weekly_frequency'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control m-input" id="name"
                                        placeholder="name" value="<?php echo $branch['name'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="address">Address</label>
                                    <input type="text" class="form-control m-input" id="address"
                                        placeholder="address" value="<?php echo $branch['address'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="address">Address 2</label>
                                    <input type="text" class="form-control m-input" id="address2"
                                        placeholder="address2"  value="<?php echo $branch['address2'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="address">State</label>
                                    <input type="text" class="form-control m-input" id="state" placeholder="state"  value="<?php echo $branch['state'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="address">City</label>
                                    <input type="text" class="form-control m-input" id="city" placeholder="city"  value="<?php echo $branch['city'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="address">ZipCode</label>
                                    <input type="text" class="form-control m-input" id="zipcode"
                                        placeholder="zipcode"  value="<?php echo $branch['zipcode'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control m-input" id="phone" placeholder="phone"
                                    value="<?php echo $branch['phone'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="address">Email</label>
                                    <input type="email" class="form-control m-input" id="email" placeholder="email"
                                        value="<?php echo $branch['email'] ?>">
                                </div>
                                <div class="form-group m-form__group" style="text-align:left">
                                    <label for="max_time">Max Time</label>
                                    <input type="text" class="form-control m-input timepicker" 
                                    id="max_time" placeholder="Max Time" value="<?= substr($branch['max_time'], 11, 8); ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="duration_min">Min Duration (in minutes)</label>
                                    <input type="number" class="form-control m-input" 
                                    id="duration_min" placeholder="MinTimeonSite" value="<?php echo $branch['duration_min'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="duration_max">Max Duration (in minutes)</label>
                                    <input type="number" class="form-control m-input" 
                                    id="duration_max" placeholder="MaxTimeonSite" value="<?php echo $branch['duration_max'] ?>">
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="site_type">Site Type</label>
                                    <input type="text" class="form-control m-input" id="site_type"
                                        placeholder="Site Type" value="<?php echo $branch['site_type'] ?>">
                                </div>
                                    <div class="m-form__group form-group">
                                        <div class="m-checkbox-list">
                                            <label class="m-checkbox">
                                                <input type="checkbox" id="active"  
                                                    <?php if($branch['active'] == 1){ ?> 
                                                        <?php echo "checked"; ?> 
                                                    <?php } ?>                                      
                                                >Site Active
                                                <span></span>
                                            </label>        																	
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group">
                                        <div class="m-checkbox-list">
                                            <label class="m-checkbox">
                                                <input type="checkbox" id="muted"  
                                                    <?php if($branch['muted'] == 1){ ?> 
                                                        <?php echo "checked"; ?> 
                                                    <?php } ?>                                 
                                                >Site Muted
                                                <span></span>
                                            </label>        																	
                                        </div>
                                    </div>
                                <div class="m--space-40"></div>
                                <h4>Service Types:</h4>                            
                                <?php foreach ($service_types as $key => $serv) { ?>
                                    <div class="m-form__group form-group">
                                        <div class="m-checkbox-list">
                                            <label class="m-checkbox">
                                                <input type="checkbox" class="check_service" data-id="<?= $serv['id'] ?>"                                      
                                                <?php foreach ($branch['service_types'] as $key => $active_service_type) {           
                                                    if($active_service_type['id'] == $serv['id']){
                                                        echo "checked";
                                                    } 
                                                }?>                                         
                                                ><?= $serv['name'] ?>
                                                <span></span>
                                            </label>        																	
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="m--space-40"></div>
                                <h4>Select the Site direction in the Map:</h4>
                                <a id="setAddress" class="btn btn-primary">Search address in map</a>
                                <div id="dvMap" style="width: 100%; height: 500px"></div>

                                <input type="text" id="latitude" class="form-control m-input" value="<?php echo $branch['latitude'] ?>">
                                <input type="text" id="longitude" class="form-control m-input" value="<?php echo $branch['longitude'] ?>">

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <a onclick="updateBranch(event, <?= $_GET['id'] ?>)" class="btn btn-primary" >Update</a>
                                </div>
                                
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
        <!--END XL12-->
    </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->


<?php include('../includes/footer.php');}?>

<!--begin::BRANCHES -->
<script src="../assets/js/branches/update.js" type="text/javascript"></script>
<!--end::BRANCHES -->
<script>
    function setAddressMap(geocoder, map){
      var address = $("#address").val();
      var address2 = $("#address2").val();
      var zipcode = $("#zipcode").val();
      var city = $("#city").val();
      var state = $("#state").val();
      var address_search = address + ' ' + address2 + ' ' + zipcode + ' ' + city + ' ' + state;
        geocoder.geocode( { 'address': address_search}, function(results, status) {
          if (status == 'OK') {
            clearMarkers();
            map.setCenter(results[0].geometry.location);
            map.setZoom(16);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location

            });
            markers.push(marker);
            $('#latitude').val(results[0].geometry.location.lat());
            $('#longitude').val(results[0].geometry.location.lng());
          } else {
            alert('Address not found: ' + status);
          }
        });
    }
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
    }

      // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }
    var map;
    var geocoder;
    var markers = [];
    function initMap() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('dvMap'), {
          center: {lat: parseFloat($('#latitude').val()), lng: parseFloat($('#longitude').val())},
          zoom: 16
        });
        var marker = new google.maps.Marker({
            map: map,
            position: {lat: parseFloat($('#latitude').val()), lng: parseFloat($('#longitude').val())}

        });
        markers.push(marker);
        document.getElementById('setAddress').addEventListener('click', function() {
          setAddressMap(geocoder, map);
        });
        google.maps.event.addListener(map, 'click', function (e) {
            clearMarkers()
            //Determine the location where the user has clicked.
            var location = e.latLng;
            var latitude = location.lat();
            var longitude = location.lng();

            console.log(longitude)

            $('#latitude').prop({'value': latitude});
            $('#longitude').prop({'value': longitude});

            if(latitude && longitude){
                $(".btn-primary").css('display','block')
            }
            
            console.log(location.lat())
            console.log(location.lng())
            //verificar si location es igual a null agregalo, si ya tiene algo no hace nada.
     
            //Create a marker and placed it on the map.
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            //Attach click event handler to the marker.
            google.maps.event.addListener(marker, "click", function (e) {
                var infoWindow = new google.maps.InfoWindow({
                    content: 'Latitude: ' + location.lat() + '<br />Longitude: ' + location.lng()
                });
                infoWindow.open(map, marker);
            });
        });
    }
</script>
<!--end::GET LATITUDE AND LONGITUDE OF GOOGLE MAPS -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo86o_PxdPQqtFTP8_byHVF8ExqWfnqP4&callback=initMap"
async defer></script>

<script >
window.onload = function () {

   
};
</script>