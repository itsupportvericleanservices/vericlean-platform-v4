<?php 
include('../includes/header.php');
$route_history = vcGetRouteHistory($_SESSION['access-token'], $_GET['id']);
$branch = vcGetBranch($_SESSION['access-token'], $route_history['branch_id']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

 <!-- END: Subheader -->
	<div class="m-content"> 
		<div class="m-content">
			<div class="col-xl-12" style="display: flex;justify-content: center;">
				<img src="../assets/images/users/user4.jpg?v=1" class="m--img-rounded m--marginless" alt="">
			</div>
		</div>	
		<!--Begin::Section-->
		<div class="row">
			<div class="col-xl-12">
						<!--Begin::Section-->
						<div class="m-portlet">
							<!--PRINT SECTION-->
							 <div class="m-portlet__head " style="padding:0px;">
								<div class="m-portlet__head-caption" style="width: 100%;">
									<div class="m-portlet__head-title" style="width: 100%;">
										<div class="col-md-12 col-lg-12 col-xl-12 col-sm-6">
											 <div class="filter float-rigth" style="float:right; margin-left: 10px;">
											
												<a class="btn btn-primary" style="color: #fff !important; padding:12px;" onclick="printInfo()" data-toggle="tooltip" data-placement="top" title="Print"><i class="fas fa-print"></i></a>
						   					</div>
					   					</div>
									</div>
								</div>
							  </div>

							<div class="m-portlet__body m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Client:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['client']['name'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Address:</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= $branch['address'] ?>
														</h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Checkin Started At:</span>
														<h3 class="m-widget1__title m--font-brand">
															<?php 
																 $strdate = $route_history['started_at'];																
																$newDate = utc_to_cst($strdate);
																 echo $newDate; ?>	
														</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>

									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Name:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['name'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">City,State:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['city'].", ".$branch['state'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Checkin Finished At:</span>
														<h3 class="m-widget1__title m--font-brand"><?php
															$strdateEnd = $route_history['finished_at'];
															$newDateEnd = utc_to_cst($strdateEnd);
															echo $newDateEnd;
														?></h3>
													</div>
												</div>
											</div>

										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>
									
									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Site Code:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['site_code'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Zip Code:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['zipcode'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Time on Site:</span>
														<h3 class="m-widget1__title m--font-brand"><?php
															$date1 = new DateTime($strdate);
															$date2 = new DateTime($strdateEnd);
															$diff = $date1->diff($date2);
															echo $diff->format('%H:%i:%s');
														?></h3>
													</div>
												</div>
											</div>

										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>
									<?php 
										if($_GET['a']!=='i')
										{
									 ?>
									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Phone:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['phone'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Email:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['email'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">MaxTime:</span>
														<h3 class="m-widget1__title m--font-brand">
															<?php $time = substr($branch['max_time'], 11, 8); ?>
															<?= $time ?>	
														</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>

									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Latitude:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['latitude'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Longitude:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $branch['longitude'] ?></h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>

									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Cleaner:</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= $branch['cleaner']['first_name']. " " . $branch['cleaner']['last_name'] ?>
														</h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Area Manager:</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= $branch['area_manager']['first_name']. " " . $branch['area_manager']['last_name'] ?>
														</h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
														<span class="m-widget1__title">Supervisor:</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= $branch['supervisor']['first_name']. " " . $branch['supervisor']['last_name'] ?>
														</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>
									<?php
										}
									  ?>                                    
								</div>
							</div>
						</div>
					</div>
				</div>
		<!--END XL12-->	
	</div>
	<!--END ROW-->
	<div class="m-content">	

		<div id=""  >
			&nbsp;
		</div>

		<div>
			<div id="dvMapCheck" style="width: 100%; height: 500px"></div>								
		</div>		
		</div>
	</div>
	<style>
#legendII{font-family:Arial,sans-serif;background:#ffffffc4;padding:10px;margin:10px;border:3px solid #000;border-radius:5px}#legendII h3{margin-top:0}#legendII img{vertical-align:middle}.item-legendII{padding-bottom:5px}
	</style>
<!--End::Content-->
<!-- end:: Page -->
<?php include('../includes/footer.php');?> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo86o_PxdPQqtFTP8_byHVF8ExqWfnqP4&callback=initMap"
	async defer></script>
  <script>
  const branch = <?php echo json_encode($branch) ?>;
	const lat_in = <?php echo  ( ! empty($route_history['latitude_checkin'])) ? $route_history['latitude_checkin'] : '""' ?>;
	const lng_in = <?php echo ( ! empty($route_history['longitude_checkin'])) ?  $route_history['longitude_checkin'] : '""' ?>;
	const lat_out = <?php echo ( !empty($route_history['latitude_checkout'])) ? $route_history['latitude_checkout'] : '""' ?>;
	const lng_out = <?php echo ( !empty($route_history['longitude_checkout'])) ? $route_history['longitude_checkout'] : '""' ?>;
 	var strhtml = '';
	var legend = document.getElementById('legend');//Create legend inside the map
	
  function initMap() {   	
		var legendII = document.createElement('div');
		legendII.id = 'legendII';
		var content = [];
 	  content.push('<h3>Legend</h3>');

		var location = new google.maps.LatLng(Number(branch.latitude), Number(branch.longitude));
   
	  map = new google.maps.Map(document.getElementById('dvMapCheck'), {
		center: {
		  lat: Number(branch.latitude),
		  lng: Number(branch.longitude)
		},
		zoom: 17
	  });
		//Create a marker and placed it on the map.
		var marker = new google.maps.Marker({
		  position: location,
		  map: map,		 
		});
	
		var contentString = '<div id="content">'+
			'<div id="siteNotice">'+
			'</div>'+
			'<h1 id="firstHeading" class="firstHeading" style="font-size: 1.5rem;">'+branch.client.name+'</h1>'+
			'<div id="bodyContent">'+            
			'</div>'+
			'</div>';
			var infowindow = new google.maps.InfoWindow({
		  content: contentString
		});
		marker.addListener('mouseover', () => infowindow.open(map, marker));
		marker.addListener('mouseout', () => infowindow.close());
		marker.addListener('click', function() {
          infowindow.open(map, marker);
				});
				
		content.push('<p><img src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png"> '+branch.client.name+'</p>');

		if(lat_in!=="" &&  lng_in!==""){
			strhtml =strhtml.concat('<div class="row"><div class="col-md-4" style="padding-bottom:5px;"><img src="http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|5fafff"/> Check-in</div></div>');
			var Last_location = new google.maps.LatLng(Number(lat_in), Number(lng_in));
			var markerUpin = new google.maps.Marker({
			  	position: Last_location,
			  	map: map,
			  	icon: {
						url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|5fafff"
		  		//	url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
				}
			});
			var strCheckIn = '<div id="content">'+
			'<div id="siteNotice">'+
			'</div>'+
			'<h1 id="firstHeading" class="firstHeading" style="font-size: 1.5rem;">Check-in</h1>'+
			'<div id="bodyContent">'+            
			'</div>'+
			'</div>';
				var infCheckIn = new google.maps.InfoWindow({
		  		content: strCheckIn
			});
			markerUpin.addListener('mouseover', () => infCheckIn.open(map, markerUpin));
			markerUpin.addListener('mouseout', () => infCheckIn.close());

			content.push('<p><img src="http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|5fafff"> Check-in</p>');
		}
		if(lat_out!=="" &&  lng_out!==""){
			strhtml =strhtml.concat('<div class="row"><div class="col-md-4" style="padding-bottom:5px;"><img src="http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|2690fb" /> Check-out</div></div>');
			var Last_locationOut = new google.maps.LatLng(Number(lat_out), Number(lng_out));
			var markerUpOut = new google.maps.Marker({
			  	position: Last_locationOut,
			  	map: map,
			  	icon: {
		  			url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|2690fb'
				}
			});
			var strCheckOut = '<div id="content">'+
			'<div id="siteNotice">'+
			'</div>'+
			'<h1 id="firstHeading" class="firstHeading" style="font-size: 1.5rem;">Check-out</h1>'+
			'<div id="bodyContent">'+            
			'</div>'+
			'</div>';
				var infCheckOut = new google.maps.InfoWindow({
		  		content: strCheckOut
			});
			markerUpOut.addListener('mouseover', () => infCheckOut.open(map, markerUpOut));
			markerUpOut.addListener('mouseout', () => infCheckOut.close());

			content.push('<p><img src="http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|2690fb"> Check-out</p>');
		}
		//ADD icons to LEGENF IF EXISTS
		$("#markersMap").append(strhtml);

		legendII.innerHTML = content.join('');
  	legendII.index = 1;
   
		 map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legendII);
  }
  function printInfo(){
   javascript:window.print();	   
	}  	
</script> 
