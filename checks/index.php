<?php
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 10) {
    if (!isset($_GET['date'])) {
        $date = date('N', strtotime(date('l')));
        $date_value = date('Y-m-d');
    }else{
        $date = date('N',strtotime($_GET['date']));
        $date_value = $_GET['date'];
    }

?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Checking</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__head " style="padding:0px;">
                        <div class="m-portlet__head-caption" style="width: 100%;">
                            <div class="m-portlet__head-title" style="width: 100%;">
                                <div class="col-md-4">
                                    <div style="float:left; margin-left:10px;">
                                        <label>From</label>
                                        <input type="date" id="from_date_filter" value="<?= $date_value ?>">
                                      
                                    </div>
                                    <div style="float:left; margin-left:10px;padding-left:19px;">
                                        <label>to</label>
                                        <input type="date" id="to_date_filter" value="<?= $date_value ?>">
                                      
                                    </div>
                                    
                                  </div>
                                <h6 class="col-md-5 m-portlet__head-text">
                                  Filters

                                 <div class="">
                                     <div style="float:left; margin-left:10px;width: 120px;">
                                      <select name="filter_by" class="filter_by form-control" onchange="branches_filter_by();">
                                        <option value="" selected>Show All</option>
                                        <option value="client">Client</option>
                                        <option value="customer">Customer</option>
                                        <option value="customer_for_client">Customers for Client</option>
                                        <option value="checking_by">Checking by</option>
                                        <option value="cleaner">Cleaner</option>
                                        <option value="supervisor">Supervisor</option>
                                        <option value="supervisor_sites">Supervisor Sites</option>
                                        <option value="state">State</option>
                                        <option value="city">City</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_client" style="display:none;float:left;width: 120px;">
                                      <select name="client_id" class="client_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_customer" style="display:none;float:left;width: 120px;">
                                      <select name="customer_id" class="customer_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_cleaner" style="display:none;float:left;width: 120px;;">
                                      <select name="cleaner_id" class="cleaner_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_supervisor" style="display:none;float:left;width: 120px;">
                                      <select name="supervisor_id" class="supervisor_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_state" style="display:none;float:left;width: 120px;">
                                      <select name="state" class="state form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_city" style="display:none;float:left;width: 120px;">
                                      <select name="city" class="city form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_client_for_customer" style="display:none;float:left;width: 120px;">
                                      <select name="client_for_customer_id" class="client_for_customer_id form-control">
                                        <option value="" selected>Select Client</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_checking_by" style="display:none;float:left;width: 120px;">
                                      <select name="filter_by_checking_by_type" class="filter_by_checking_by_type form-control">
                                        <option value="" selected>Select type</option>
                                        <option value="cleaner" >Cleaner</option>
                                        <option value="supervisor" >Supervisor</option>
                                        <option value="reassined" >Reassigned</option>
                                      </select>
                                    </div>
                                    
                                    
                               
                                    <div style="clear:both;"></div>
                                  </div>
                                </h6>
                                <div class="col-md-3">
                                <div class="filter float-rigth" style="float:left; margin-left: 10px;">
                                        <button onclick="check_filter(0)" class="btn btn-info" style="padding:12px;" data-toggle="tooltip" data-placement="top" title="Search"><i class="fas fa-search"></i></button>
                                        <button onclick="location.reload()" class="btn btn-danger" style="padding:12px;" data-toggle="tooltip" data-placement="top" title="Clean search"><i class="fas fa-redo-alt"></i></button>
                                        <a class="btn btn-primary" style="color: #fff !important; padding:12px;" onclick="check_filter(true)" data-toggle="tooltip" data-placement="top" title="Print"><i class="fas fa-print"></i></a>
                                      </div>
                                </div>
                            </div> 
                        </div>
                        
                        
                    
                    </div>

                    <div class="m-portlet__body" id="m-portlet__body">
                    <!--begin::Section-->
                    <!--end::Section-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
        <!--END XL12-->
    </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->

<input type="hidden" class="searchstr" name="searchstr" value="" />
<!-- Modal -->
<div class="modal fade" id="check_map" tabindex="-1" role="dialog" aria-labelledby="check_mapModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Check in / out</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="map" style="width: 100%; height: 500px"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>



<!--begin::CHECKS -->
<script src="../assets/js/checks/index.js" type="text/javascript"></script>
<!--end::CHECKS -->

<?php include('../includes/footer.php');?>

<script>
$( document ).ready(function() {
    document.addEventListener('DOMContentLoaded',getBranches(1));
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
<script>

function adminCheckOut(lat_out,lon_out,di,br){
     $.ajax({
      type: "POST",
      url: "../../../router/route_histories/update.php?id=" + di,
      data: JSON.stringify({
        status: 2,
        finished_at: new Date()
      }),
      dataType: "json"
    }).done(function (data) {   
      swal({
        title: "Success!",
        text: "CHECK OUT",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
      //  window.location.href = "/checks/index.php";
      check_filter(0);
      });
      
    });

  return false;

}
    function showMap(lat_in, lng_in, lat_out, lng_out, brn_lat, brn_lng){
        clearMarkers()
        map.setZoom(7);    
        if (lat_in != '') {
            var location = new google.maps.LatLng(lat_in, lng_in);
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            map.setCenter(location);
            map.setZoom(16);
            setTimeout(function(){
              map.setCenter(location);
              map.setZoom(16);
            },500)

        }
        if (lat_out != '') {
            var location = new google.maps.LatLng(lat_out, lng_out);
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            setTimeout(function(){
              map.setCenter(location);
              map.setZoom(16);
            },500)
        }

        if (lat_in == '' && lat_out == '' && brn_lat !='') {
          var location = new google.maps.LatLng(brn_lat, brn_lng);
          var marker = new google.maps.Marker({
              position: location,
              map: map
          });
          markers.push(marker);
          setTimeout(function(){
            map.setCenter(location);
            map.setZoom(16);
          },500)
        }

        
        $('#check_map').modal('show');
    }
    function setAddressMap(geocoder, map){
      var address = $("#address").val();
      var address2 = $("#address2").val();
      var zipcode = $("#zipcode").val();
      var city = $("#city").val();
      var state = $("#state").val();
      var address_search = address + ' ' + address2 + ' ' + zipcode + ' ' + city + ' ' + state;
        geocoder.geocode( { 'address': address_search}, function(results, status) {
          if (status == 'OK') {
            clearMarkers();
            map.setCenter(results[0].geometry.location);
            map.setZoom(16);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location

            });
            markers.push(marker);
            $('#latitude').val(results[0].geometry.location.lat());
            $('#longitude').val(results[0].geometry.location.lng());
          } else {
            alert('Address not found: ' + status);
          }
        });
    }
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
    }

      // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }
    var map;
    var geocoder;
    var markers = [];
    function initMap() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 32.124127, lng: -100.4939386},
          zoom: 5
        });
        
        
    }
</script>
<!--end::GET LATITUDE AND LONGITUDE OF GOOGLE MAPS -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo86o_PxdPQqtFTP8_byHVF8ExqWfnqP4&callback=initMap"
async defer></script>
<?php } ?>
