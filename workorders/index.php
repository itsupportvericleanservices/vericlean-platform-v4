<?php 
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 10 || $_SESSION['role_id'] == 5) {
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">Workorders</h3>
        </div>
    </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">

            <!--begin::Portlet-->
            <div class="m-portlet">
            
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Filters 
                                  <div>
                                    <div style="float:left; margin-left:10px;width:120px;">
                                      <select name="filter_by" class="filter_by form-control" onchange="workorders_filter_by();">
                                        <option value="" selected>Show All</option>
                                        <option value="status">Status</option>
                                        <option value="priority">Priority</option>
                                        <option value="main_type">Type</option>
                                        <option value="client">Client</option>
                                        <option value="customer">Customer</option>
                                        <option value="customer_for_client">Customers for Client</option>
                                        <option value="cleaner">Cleaner</option>
                                        <option value="supervisor">Supervisor Sites</option>
                                        <option value="state">State</option>
                                        <option value="city">City</option>
                                        <option value="address">Address</option>
                                        <option value="site_name">Site Name</option>
                                        <option value="site_code">Site code</option>
                                        <option value="customer_work_order">Customer Work Order#</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_custom filter_hide" style="margin-left: 20px;display:none;float:left;width:120px;">
                                      <input type="text" id="filter_custom" class="form-control">
                                    </div>
                                    <div class="filter_by_client filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="client_id" class="client_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_customer filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="customer_id" class="customer_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_cleaner filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="cleaner_id" class="cleaner_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_supervisor filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="supervisor_id" class="supervisor_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_state filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="state" class="state form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_city filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="city" class="city form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_client_for_customer filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="client_for_customer_id" class="client_for_customer_id form-control">
                                        <option value="" selected>Select Client</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_checking_by filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="filter_by_checking_by_type" class="filter_by_checking_by_type form-control">
                                        <option value="" selected>Select type</option>
                                        <option value="cleaner" >Cleaner</option>
                                        <option value="supervisor" >Supervisor</option>
                                        <option value="reassined" >Reassigned</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_status filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="status" class="status form-control">
                                        <option value="" selected>Select One</option>
                                        <option value="0">Opened</option>
                                        <option value="1">Quoted</option>
                                        <option value="2">Approved</option>
                                        <option value="3">Scheduled</option>
                                        <option value="4">Assigned</option>
                                        <option value="5">Dispatched</option>
                                        <option value="6">In Progress</option>
                                        <option value="7">Completed</option>
                                        <option value="8">Refused</option>
                                        <option value="9">Rejected</option>
                                        <option value="10">Invoiced</option>
                                        <option value="11">Payed</option>
                                      </select>
                                    </div>

                                    <div class="filter_by_priority filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="priority" class="priority form-control">
                                        <option value="" selected>Select One</option>
                                        <option value="1">P1</option>
                                        <option value="2">P2</option>
                                        <option value="3">P3</option>
                                        <option value="4">P4</option>
                                        <option value="5">P5</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_main_type filter_hide" style="display:none;float:left;width:120px;">
                                      <select name="main_type" class="main_type form-control">
                                        <option value="" selected>Select One</option>
                                        <option value="emergency">Emergency</option>
                                        <option value="complain">Complain</option>
                                        <option value="rush_request">Rush Request</option>
                                        <option value="request">Request</option>
                                        <option value="periodics">Periodics</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_delayed filter_hide" style="display:none;float:left;">
                                      <select name="delayed" class="delayed form-control">
                                        <option value="" selected>Select One - Customer Work Order#</option>
                                      </select>
                                    </div>
                                    <div class="filter" style="display:none;float:left; margin-left: 10px;">
                                      <button onclick="workorders_filter()" class="btn btn-info">Filter</button>
                                      <button onclick="location.reload()" class="btn btn-danger">Clear</button>
                                    </div>
                                    <div style="clear:both;"></div>
                                  </div>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                <!-- <button onclick="getWorkorders(1)" class="btn btn-danger">Show all</button> -->
                                    <a href="create.php" class="btn btn-primary" style="margin-left:10px;">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>New WorkOrder</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item">
                                <!-- <button onclick="getWorkorders(1)" class="btn btn-danger">Show all</button> -->
                                    <a href="create_file.php" class="btn btn-info" style="margin-left:10px;">
                                        <span>
                                            <i class="la la-file"></i>
                                            <span>WorkOrder file</span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                <div class="m-portlet__body" id="m-portlet__body"> </div>

            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
    <!--END XL12-->
</div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->


<input type="hidden" class="searchstr" name="searchstr" value="" />
<!-- end::Quick Sidebar -->

<!--begin::WORKORDERS -->
<script src="../assets/js/workorders/delete.js" type="text/javascript"></script>
<script src="../assets/js/workorders/index.js" type="text/javascript"></script>
<!--end::WORKORDERS -->
<?php include('../includes/footer.php');?>

<script>
// $( document ).ready(function() {
//     document.addEventListener('DOMContentLoaded',getWorkorders(1));
// });

// $(document).ready(function () {
//       var searchstr = JSON.parse(window.localStorage.getItem('filtro_works'))
//       if ( searchstr == null) {
//         console.log("object");
         document.addEventListener('DOMContentLoaded',getWorkorders(1));
        
//       }else{
//           var page = 1
//           console.log(searchstr);

//           // if (searchstr != null) {
//           //   console.log("object")
//           $.ajax({
//             type: "POST",
//             url: "../../../router/workorders/index.php",
//             data: JSON.stringify({
//               page: page,
//               searchstr: encodeURI(searchstr)
//             }),
//             dataType: "html"
//           }).done(function (data) {
//             $("#m-portlet__body").html(data);
//           });
//       }
      
//     });
</script>

<?php } ?>
