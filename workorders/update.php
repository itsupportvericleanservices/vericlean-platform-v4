<?php
include('../includes/header.php');
$clients = vcGetClients($_SESSION['access-token']);
$cleaners = vcGetUsers($_SESSION['access-token']);//vcGetCleaners($_SESSION['access-token']);
$active_sites = vcGetActiveBranches($_SESSION['access-token']);
$workorder = vcGetWorkorder($_SESSION['access-token'], $_GET['id']);
$files = vcGetWorkorderfiles($_SESSION['access-token'], $_GET['id']);
$sites = vcGetSitesForClient($_SESSION['access-token'],$workorder['client']['id']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');

?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">Workorder <?php echo $workorder['internal_id'] ?></h3>
            <input type="text" value="<?php echo $_SESSION['id'] ?>" id="user_id" style="display:none">
        </div>
    </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">

            <!--begin::Portlet-->
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">

                            <div class="form-group m-form__group">
                                <label for="clients">Type of Client</label>
                                <select class="form-control m-input" id="type_client" name="type_client" onchange="selectClient()">
                                    <option value="">Select One</option>
                                    <option value="client" <?= ($workorder['client']['indirect'] == 0) ? "selected" : ""; ?>>Client</option>
                                    <option value="customer" <?= ($workorder['client']['indirect'] == 1) ? "selected" : ""; ?>>Customer</option>
                                </select>
                            </div>

                            <div class="form-group m-form__group" id="client_content" style="<?= ($workorder['client']['indirect'] == 1) ? 'display: none' : '' ?>" onchange="sitesForClient()">
                                <label>Clients</label>
                                <select class="form-control m-input" id="client_id" name="client_id">
                                    <option value="">Select One</option>
                                    <?php
                                        foreach ($clients as $key => $client) {
                                            if ($client['indirect'] == 0) {
                                                $name = '';
                                                $name .= ($client['site_code']) ? $client['site_code'] : '';
                                                $name .= ($client['name']) ? ' - '.$client['name'] : '';
                                                $name .= ($client['address']) ? ' - '.$client['address'] : '';
                                                $selected = ($client['id'] == $workorder['client']['id']) ? 'selected' : '';
                                                echo "<option value='".$client['id']."' ".$selected.">".$name."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group" id="customer_content" style="<?= ($workorder['client']['indirect'] == 0) ? 'display: none' : '' ?>" onchange="sitesForClient()">
                                <label>Customers</label>
                                <select class="form-control m-input" id="customer_id" name="customer_id">
                                    <option value="" >Select One</option>
                                    <?php
                                        foreach ($clients as $key => $client) {
                                            if ($client['indirect'] == 1) {
                                                $name = '';
                                                $name .= ($client['site_code']) ? $client['site_code'] : '';
                                                $name .= ($client['name']) ? ' - '.$client['name'] : '';
                                                $name .= ($client['address']) ? ' - '.$client['address'] : '';
                                                $selected = ($client['id'] == $workorder['client']['id']) ? 'selected' : '';
                                                echo "<option value='".$client['id']."' ".$selected.">".$name."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group" id="branch_id_container" >
                                <label>Sites</label>
                                <select class="form-control m-input" id="branch_id" name="branch_id">
                                    <option value="" selected>Select One</option>
                                    <?php
                                        foreach ($sites as $key => $site) {


                                            $selected = ($site['id'] == $workorder['branch_id']) ? 'selected' : '';
                                            echo "<option  class='option_remove' value='".$site['id']."' ".$selected.">".$site['site_code']." - " .$site['name']." - ".$site['address']."</option>";

                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label>Customer Work Order#</label>
                                <input type="text" class="form-control m-input" id="internal_id" placeholder="Internal ID" value="<?php echo $workorder['internal_id'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                            
                                <label>Cleaners </label>
                                <select class="form-control m-input" id="cleaner_id" name="cleaner_id">
                                <option>Select one</option>
                                    <?php foreach ($cleaners as $key => $cleaner) { ?>
                                        
                                        <option value="<?php echo $cleaner['id'] ?>"
                                            <?php if(!empty($workorder['cleaner']['id']) && ($workorder['cleaner']['id'] == $cleaner['id'])){ ?>
                                                 selected
                                            <?php } ?>
                                        ><?php echo $cleaner['first_name'] ?> <?php echo $cleaner['last_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label>Request Date</label>
                                <input type="date" class="form-control m-input" id="requested_date" placeholder="Request Date" value="<?= substr($workorder['requested_date'], 0, 10); ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Due Date</label>
                                <input type="date" class="form-control m-input" id="due_date" placeholder="Due date" value="<?= substr($workorder['due_date'], 0, 10); ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Site Contact</label>
                                <input type="text" class="form-control m-input" id="site_contact" placeholder="Site Contact" value="<?php echo $workorder['site_contact'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Site Phone</label>
                                <input type="text" class="form-control m-input" id="site_phone" placeholder="Site Phone" value="<?php echo $workorder['site_phone'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Request Contact</label>
                                <input type="text" class="form-control m-input" id="request_contact" placeholder="Request Contact" value="<?php echo $workorder['request_contact'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Request Contact Email</label>
                                <input type="text" class="form-control m-input" id="request_contact_email" placeholder="Site Contact email" value="<?php echo $workorder['request_contact_email'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Request Phone</label>
                                <input type="text" class="form-control m-input" id="request_phone" placeholder="Request Phone" value="<?php echo $workorder['request_phone'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Task Name</label>
                                <input type="text" class="form-control m-input" id="task_name" placeholder="Task Name" value="<?php echo $workorder['task_name'] ?>">
                            </div>
                            <div class="form-group m-form__group">
                                <label>Task Description</label>
                                <textarea class="form-control m-input" id="task_description" placeholder="Task Description" rows="4" cols="50"><?php echo $workorder['task_description'] ?></textarea>
                            </div>
                            <div class="form-group m-form__group">
                                <label>Status</label>
                                <input type="hidden" id="actual_status" value="<?php echo $workorder['status'] ?>">
                                <select class="form-control m-input" id="status" name="status" onchange="verifydispatched()">
                                    <option value="0" <?php if($workorder['status'] == 0){ echo "selected"; } ?>>Opened</option>
                                    <option value="1" <?php if($workorder['status'] == 1){ echo "selected"; } ?>>Quoted</option>
                                    <option value="2" <?php if($workorder['status'] == 2){ echo "selected"; } ?>>Approved</option>
                                    <option value="3" <?php if($workorder['status'] == 3){ echo "selected"; } ?>>Scheduled</option>
                                    <option value="4" <?php if($workorder['status'] == 4){ echo "selected"; } ?>>Assigned</option>
                                    <option value="5" <?php if($workorder['status'] == 5){ echo "selected"; } ?>>Dispatched</option>
                                    <option value="6" <?php if($workorder['status'] == 6){ echo "selected"; } ?>>In Progress</option>
                                    <option value="12" <?php if($workorder['status'] == 12){ echo "selected"; } ?>>Ready to verify</option>
                                    <option value="7" <?php if($workorder['status'] == 7){ echo "selected"; } ?>>Completed</option>
                                    <option value="8" <?php if($workorder['status'] == 8){ echo "selected"; } ?>>Refused</option>
                                    <option value="9" <?php if($workorder['status'] == 9){ echo "selected"; } ?>>Rejected</option>
                                    <option value="10" <?php if($workorder['status'] == 10){ echo "selected"; } ?>>Invoiced</option>
                                    <option value="11" <?php if($workorder['status'] == 11){ echo "selected"; } ?>>Payed</option>
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                              <label>Types</label>
                              <select class="form-control m-input" id="type" name="type">
                                <!-- Foreach: Listar el type activo -->
                                <option value="null">Select One</option>
                                <?php $types = ['emergency', 'complain', 'rush_request', 'request', 'periodics'] ?>
                                <?php foreach($types as $type) { ?>
                                  <option value="<?= $type ?>" <?php if($workorder['main_type'] == $type) {echo "selected";} ?>>
                                    <?= ucwords(str_replace("_", " ", $type)) ?>
                                  </option>
                                <?php }?>
                                <!-- End Foreach: Listar el type activo -->
                              </select>
                            </div>
                            <div class="form-group m-form__group">
                              <label>Priority</label>

                              <select class="form-control m-input" id="priority" name="priority">
                                <!-- Foreach: Listar el periodity activo -->
                                <option value="null">Select One</option>
                                <?php $priorities = ['1', '2', '3', '4', '5'] ?>
                                <?php foreach($priorities as $priority) { ?>
                                  <option value="<?= $priority ?>" <?php if($priority == $workorder['priority']) { echo "selected"; } ?>>
                                    P<?= $priority ?>
                                  </option>
                                <?php }?>
                                <!-- End Foreach: Listar el type activo -->
                              </select>
                            </div>
                            <div class="form-group m-form__group">
                              <label>Fee</label>
                              <input type="number" class="form-control m-input" id="fee" placeholder="Fee" value="<?php echo $workorder['fee'] ?>">
                            </div>
                            <label>Schedule At</label>
                            <div class="form-group m-form__group row">
                                
                                <div class="col-md-6">
                                <label>Date</label>
                                <input type="date" class="form-control m-input" id="scheduled_at_date" placeholder="YYYY-MM-DD" value="<?= substr($workorder['scheduled_date'],0 ,10) ?>" >
                                </div>
                                <div class="col-md-6">
                                <label>Time (in 24 hour format)</label>
                                <input type="datetime" class="form-control m-input" id="scheduled_at_time" placeholder="23:59" value="<?= substr($workorder['scheduled_date'],10 ,6) ?>" >
                                </div>
                                
                            </div>
                            <div class="form-group m-form__group">
                                <label>Comments</label>
                                <textarea class="form-control m-input" id="comments" placeholder="Comments" rows="4" cols="50" ><?=$workorder['comments'] ?></textarea  >
                            </div>
                            <div class="form-group m-form__group">
                                <label>Instructions</label>
                                <textarea class="form-control m-input" id="instructions" placeholder="Instructions" rows="4" cols="50" ><?=$workorder['instructions'] ?></textarea  >
                            </div>
                            <div class="form-group m-form__group form_document_group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Documents</label>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-success" style="float: right; widht: 25px !important; height: 25px !important; line-height:0" onclick="addAttachment('document');">Add a Document</a>
                                    </div>
                                </div>
                                <div class="documents-container">
                                </div>
                            </div>

                            <hr />
                            <div class="form-group m-form__group form_document_group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Photos</label>
                                    </div>
                                    <div class="col-md-6 text-right">
                                      <a class="btn btn-success" style="height: 25px !important; line-height:0" onclick="addAttachment('photo');">Add a Photo</a>
                                    </div>
                                </div>
                                <div class="photos-container">
                                </div>
                            </div>
                            <div class="form-group m-form__group">
                              <h4 class="page-header">Documents</h4>
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Custom Name</th>
                                    <th scope="col">Filename</th>
                                    <th scope="col">Options</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($workorder['documents'] as $key => $file) { ?>
                                  <tr>
                                    <td></td>
                                    <td><?= $file['attachments'][0]['blob']['filename']; ?></td>
                                    <td><?= $file['attachments'][0]['blob']['filename']; ?></td>
                                    <td>
                                      <a href="<?= $file['attachments'][0]['blob']['service_url']; ?>" target="_blank">
                                        <i class='flaticon-interface-11'></i> Download
                                      </a>
                                    </td>
                                  </tr>
                                <?php } ?>
                                <tbody>
                              </table>

                            </div>
                            <div class="form-group m-form__group">
                              <h4 class="page-header">Photos</h4>
                              <?php foreach ($workorder['photos'] as $key => $file) { ?>
                                <a class="thumbnail" href="<?= $file['attachments'][0]['blob']['service_url']; ?>" data-title="<?= $file['attachments'][0]['blob']['filename']; ?>" data-lightbox="photos">
            											<img class="img-responsive img-thumbnail" src="<?= $file['attachments'][0]['blob']['service_url']; ?>" height="200px" />
            										</a>
                              <?php } ?>
                            </div>
                          </div>

                          
                          <div class="form-group m-form__group">
                              <h4 class="page-header">Invoice</h4>
                              <div class="form-group m-form__group form_invoices_group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Invoice</label>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-success" style="float: right; widht: 25px !important; height: 25px !important; line-height:0" onclick="addAttachment('invoice');">Add Invoice</a>
                                    </div>
                                </div>
                                <div class="invoices-container">
                                </div>
                              </div>
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Custom Name</th>
                                    <th scope="col">Filename</th>
                                    <th scope="col">Options</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($workorder['invoices'] as $key => $file) { ?>
                                  <tr>
                                    <td></td>
                                    <td><?= $file['attachments'][0]['blob']['filename']; ?></td>
                                    <td><?= $file['attachments'][0]['blob']['filename']; ?></td>
                                    <td>
                                      <a href="<?= $file['attachments'][0]['blob']['service_url']; ?>" target="_blank">
                                        <i class='flaticon-interface-11'></i> Download
                                      </a>
                                    </td>
                                  </tr>
                                <?php } ?>
                                <tbody>
                              </table>

                            </div>
                          <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                              <a onclick="b64Files(event, <?= $_GET['id'] ?>);" class="btn btn-primary">Update</a>
                              <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a>
                            </div>
                          </div>
                        </div>

                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
    <!--END XL12-->
</div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->
<?php include('../includes/footer.php');?>

<!--begin::CLIENTS -->
<script src="../assets/js/plugins/cleave.min.js"></script>
<script src="../assets/js/workorders/update.js" type="text/javascript"></script>
<!--end::CLIENTS -->

<script>
  var cleave = new Cleave('#scheduled_at_date', {
    date: true,
    delimiter: '-',
    datePattern: ['Y', 'm', 'd']
  });
  
  var cleave = new Cleave('#scheduled_at_time', {
    time: true,
    timePattern: ['h', 'm']
  });
</script>
<script>
    $("#cleaner_id").chosen({no_results_text: "Oops, nothing found!"}); 
</script>