<?php 
include('../includes/header.php');
$id = $_GET['id']
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>
  <div id="content_filter">

  </div>
  <!-- END: Left Aside -->
  
  <!--End::Content-->
  <!-- end:: Page -->

  <!-- end::Quick Sidebar -->
  <?php include('../includes/footer.php');?>

  <!--begin::CLIENTS -->
  <script src="../assets/js/route_schedules/filter.js" type="text/javascript"></script>

  <script>
    var id_site = <?php echo json_encode($id) ?>;
    console.log(id_site)
    document.addEventListener('DOMContentLoaded',loadFilter(id_site));
  </script>
