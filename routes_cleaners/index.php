<?php
date_default_timezone_set('America/Chicago');
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6| $_SESSION['role_id'] == 10) {

    if (!isset($_GET['date'])) {
        $date = date('N', strtotime(date('l')));
        $date_value = date('Y-m-d');
    }else{
        $date = date('N',strtotime($_GET['date']));
        $date_value = $_GET['date'];
    }

    $cleaners = vcGetCleanersByFrecuency($_SESSION['access-token'], $date, $date_value);
    //$all_u = vcGetUsers($_SESSION['access-token']);
    $all_cleaners =vcGetCleaners($_SESSION['access-token']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php'); 

//var_dump($cleaners);
// echo "<br>";
 
?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Routes</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Date
                                  <div>
                                    <div style="float:left; margin-left:10px">
                                      <input type="date" id="date_filter" value="<?= $date_value ?>">
                                      
                                    </div>
                                    
                                  </div>
                                </h3>
                            </div>
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Delayed
                                  <div>
                                    <div style="float:left; margin-left:10px">
                                      <input type="checkbox" id="delay_check" onchange="showDaleyed(event)">
                                      
                                    </div>
                                    
                                  </div>
                                </h3>
                            </div>
                        </div>
                        
                    </div>

                    <div class="m-portlet__body">

                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Cleaner</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Sites</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                         // counter es un ID artificial de records para poder diferenciar los rows
                                         // y aplicarle operaciones sobre ese record 
                                            
                                            foreach ($cleaners as $key => $cleaner) {
                                                
                                                $class_branch = '';
                                                $class_cleaner = '';
                                                // var_dump($cleaner['warning']);
                                                if ($cleaner['warning'] && strtotime($date_value) <= time()) {
                                                    
                                                    $max_time = explode('.000', explode('T', $cleaner['min_time'])[1])[0];
                                                    //$max_time = $cleaner['min_time'];
                                                    //$max_time = strtotime($max_time);
                                                    $now_time = date('H:i:s');
                                                    $now_time = strtotime($now_time);
                                                    //echo "___________<br>";
                                                    $max_time = DateTime::createFromFormat('H:i:s',$cleaner['min_time'])->format('H:i:s');
                                                    $max_time = strtotime($max_time);
                                                    // var_dump(date('H:i:s'));
                                                    //  var_dump($now_time);
                                                    //  var_dump($cleaner['min_time']);
                                                    //  var_dump($max_time);
                                                    
                                                    if ( ($now_time - $max_time)/60 > 1 ) {
                                                        $class_cleaner = 'warning_yellow';
                                                    }
                                                    if(($now_time - $max_time)/60 > 29 && ($now_time - $max_time)/60 < 59){
                                                        $class_cleaner = 'warning_orange';
                                                    }
                                                    if(($now_time - $max_time)/60 > 59 || strtotime($date_value) < strtotime(date('Y-m-d'))){
                                                        $class_cleaner = 'warning_red';
                                                    }
                                                    
                                                }
                                        ?>
                                        <tr class="tr_cleaner <?= $class_cleaner ?>" data-toggle="collapse" href="#cleaner_<?=$cleaner['cleaner']['id'] ?>" role="button" aria-expanded="false" aria-controls="cleaner_<?=$cleaner['cleaner']['id'] ?>">
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                            <td >
                                                <?php if(empty($cleaner['cleaner'])){ ?>
                                                    not assigned
                                                <?php }else{ ?>
                                                    <?= $cleaner['cleaner']['first_name'].' '.$cleaner['cleaner']['last_name'] ?>
                                                <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <?= $cleaner['cleaner']['phone'] ?>
                                            </td>
                                            <td>
                                                <?= $cleaner['cleaner']['email'] ?>
                                            </td>
                                            <td>
                                                <?= count($cleaner['branches']) ?>
                                            </td>
                                            
                                            
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="12">
                                            <table class="table table-bordered table-hover table table-bordered collapse" id="cleaner_<?=$cleaner['cleaner']['id'] ?>">
                                                <thead>
                                                    <tr>
                                                        <th width="70px">Site Code</th>
                                                        <th>Name</th>
                                                        <th>Client</th>
                                                        <th>Address</th>
                                                        <th>City</th>
                                                        <th>State</th>
                                                        <th>Zipcode</th>
                                                        <th>Time</th>
                                                        <th>check-in</th>
                                                        <th>check-out</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach ($cleaner['branches'] as $key => $branch) {
 
                                                                
                                                            date_default_timezone_set('America/Chicago');
                                                            $max_time = explode('.000Z', explode('T', $branch['branch']['max_time'])[1])[0];
                                                            if ($branch['branch']['max_time'] == null) {
                                                                $max_time = '23:59:59';
                                                            }
                                                            $max_time = strtotime($max_time);
                                                            $now_time = date('H:i:s');
                                                            $now_time = strtotime($now_time);
                                                            //var_dump($branch);
                                                            //var_dump($now_time);
                                                            $class_branch = '';
                                                           
                                                            if (strtotime($date_value) <= time() ) {
                                                               
                                                                if ( ($now_time - $max_time)/60 > 1 && $branch['branch']['history'][0]['started_at'] == null) {
                                                                    $class_branch = 'warning_yellow';
                                                                }
                                                                if(($now_time - $max_time)/60 > 29 && ($now_time - $max_time)/60 < 59 && $branch['branch']['history'][0]['started_at'] == null){
                                                                    $class_branch = 'warning_orange';
                                                                }
                                                                if(($now_time - $max_time)/60 > 59 && $branch['branch']['history'][0]['started_at'] == null){
                                                                    $class_branch = 'warning_red';
                                                                }
                                                            }
                                                            

                                                           
                                                    ?>
                                                    <tr class="tr_branch <?= $class_branch ?>">
                                                       
                                                        <td scope="row">
                                                            
                                                            <?= $branch['branch']['site_code'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= $branch['branch']['name'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= $branch['branch']['client']['name'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= $branch['branch']['address'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= $branch['branch']['city'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= $branch['branch']['state'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= $branch['branch']['zipcode'] ?>
                                                            
                                                        </td>
                                                        <td>
                                                           
                                                            <?=($branch['branch']['max_time'] == null)? '' : date('h:i A', strtotime($branch['branch']['max_time'])); ?>
                                                            
                                                            
                                                        </td>
                                                        
                                                        <td>
                                                            
                                                            <?= ($branch['branch']['history'][0]['started_at'] != null) ? date( 'Y-m-d H:i:s',strtotime($branch['branch']['history'][0]['started_at'])) : '' ?>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                            <?= ($branch['branch']['history'][0]['finished_at']!= null) ? date( 'Y-m-d H:i:s',strtotime($branch['branch']['history'][0]['finished_at']) ) : ''; ?>
                                                            
                                                        </td>
                                                        <td>
                                                            <span class="dropdown">
                                                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                                    data-toggle="dropdown" aria-expanded="true">
                                                                    <i class="la la-ellipsis-h"></i>
                                                                </a>
                                                                <!-- Se pasa el record a getCleaners para poder mostrar el select listCleanerReassigned que corresponde -->
                                                                <div class="dropdown-menu dropdown-menu-right" id="action_menu">
                                                                    <a onclick="reassigneModal(event,<?= $cleaner['cleaner']['id'] ?>,<?= $branch['branch']['id']?>,'<?= $branch['branch']['name']?>')" class="dropdown-item"><i class="la la-crosshairs"></i> Reassigne</a>
                                                                    <a onclick="rescheduleConfirm(event,<?= $cleaner['cleaner']['id'] ?>,<?= $branch['branch']['id']?>) " class="dropdown-item"><i class="la la-crosshairs"></i>  Reschedule</a>
                                                              
                                                                </div>
                                                                <div class="dropdown-menu dropdown-menu-right" id="action_menu">
                                                                    
                                                                <!--
                                                                    <a onclick="rescheduledSite(event, <?= $counter ?>)" class="dropdown-item"><i class="la la-crosshairs"></i>
                                                                        Rescheduled</a>
                                                                        -->
                                                                </div>
                                                               
                                                               
                                                            </span>
                                                        </td>
                                                        
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
        <!--END XL12-->
    </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->



<!-- begin::Quick Sidebar -->
<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
    <div class="m-quick-sidebar__content m--hide">
        <span id="m_quick_sidebar_close" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
        <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger"
                    role="tab">Messages</a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings"
                    role="tab">Settings</a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
                <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                    <div class="m-messenger__messages m-scrollable">
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Hi Bob. What time will be the meeting ?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Hi Megan. It's at 2.30PM
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Will the development team be joining ?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Yes sure. I invited them as well
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__datetime">2:30PM</div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Noted. For the Coca-Cola Mobile App project as well ?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Yes, sure.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Please also prepare the quotation for the Loop CRM project as
                                            well.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__datetime">3:15PM</div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-no-pic m--bg-fill-danger">
                                    <span>M</span>
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Noted. I will prepare it.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Thanks Megan. I will see you later.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Sure. See you in the meeting soon.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__seperator"></div>
                    <div class="m-messenger__form">
                        <div class="m-messenger__form-controls">
                            <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
                        </div>
                        <div class="m-messenger__form-tools">
                            <a href="" class="m-messenger__form-attachment">
                                <i class="la la-paperclip"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
                <div class="m-list-settings m-scrollable">
                    <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">General Settings</div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Email Notifications</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" checked="checked" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Site Tracking</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">SMS Alerts</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Backup Storage</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Audit Logs</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" checked="checked" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">System Settings</div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">System Logs</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Error Reporting</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Applications Logs</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Backup Servers</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" checked="checked" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Audit Logs</span>
                            <span class="m-list-settings__item-control">
                                <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                    <label>
                                        <input type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
                <div class="m-list-timeline m-scrollable">
                    <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                            System Logs
                        </div>
                        <div class="m-list-timeline__items">
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">12 new users registered <span
                                        class="m-badge m-badge--warning m-badge--wide">important</span></a>
                                <span class="m-list-timeline__time">Just now</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                <span class="m-list-timeline__time">11 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                <a href="" class="m-list-timeline__text">New invoice received</a>
                                <span class="m-list-timeline__time">20 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                <a href="" class="m-list-timeline__text">Database overloaded 89% <span
                                        class="m-badge m-badge--success m-badge--wide">resolved</span></a>
                                <span class="m-list-timeline__time">1 hr</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">System error</a>
                                <span class="m-list-timeline__time">2 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">Production server down <span class="m-badge m-badge--danger m-badge--wide">pending</span></a>
                                <span class="m-list-timeline__time">3 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">Production server up</a>
                                <span class="m-list-timeline__time">5 hrs</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                            Applications Logs
                        </div>
                        <div class="m-list-timeline__items">
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--info m-badge--wide">urgent</span></a>
                                <span class="m-list-timeline__time">7 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">12 new users registered</a>
                                <span class="m-list-timeline__time">Just now</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                <span class="m-list-timeline__time">11 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                <a href="" class="m-list-timeline__text">New invoices received</a>
                                <span class="m-list-timeline__time">20 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                                <span class="m-list-timeline__time">1 hr</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">System error <span class="m-badge m-badge--info m-badge--wide">pending</span></a>
                                <span class="m-list-timeline__time">2 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">Production server down</a>
                                <span class="m-list-timeline__time">3 hrs</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                            Server Logs
                        </div>
                        <div class="m-list-timeline__items">
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">Production server up</a>
                                <span class="m-list-timeline__time">5 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">New order received</a>
                                <span class="m-list-timeline__time">7 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">12 new users registered</a>
                                <span class="m-list-timeline__time">Just now</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                <span class="m-list-timeline__time">11 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                <a href="" class="m-list-timeline__text">New invoice received</a>
                                <span class="m-list-timeline__time">20 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                                <span class="m-list-timeline__time">1 hr</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">System error</a>
                                <span class="m-list-timeline__time">2 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">Production server down</a>
                                <span class="m-list-timeline__time">3 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">Production server up</a>
                                <span class="m-list-timeline__time">5 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">New order received</a>
                                <span class="m-list-timeline__time">1117 hrs</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cleanersModal" tabindex="-1" role="dialog" aria-labelledby="cleanersModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reassigne</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>Site:</label>  
        <input type="text" id="branch_reassinged_name" disabled>
        <input type="hidden" id="branch_reassinged_id" disabled>
        <br>
        <label>Cleaners</label>
        <select id="selectCleanerReassinged" onchange="selectCleanerReassinged(event)">
            <?php 
                foreach ($all_cleaners as $key => $cleaner) {
                    echo "<option value='".$cleaner['id']."'>".$cleaner['first_name']." ".$cleaner['last_name']."</option>";
                }
            ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="reassignedCleaners(event)">Reassigne</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end::Quick Sidebar -->

<!--begin::BRANCHES -->
<script src="../assets/js/branches/delete.js" type="text/javascript"></script>
<script src="../assets/js/branches/reassigned.js" type="text/javascript"></script>
<script src="../assets/js/branches/rescheduled.js" type="text/javascript"></script>
<!--end::BRANCHES -->

<?php include('../includes/footer.php');?>


<?php } ?>
