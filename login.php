<!DOCTYPE html>

<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />

        <title>VeriClean Services | Login</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->

        <!--begin::Global Theme Styles -->
                    <link href="./assets/stylesheet/vendors/vendors.bundle.css" rel="stylesheet" type="text/css" />
                    <link href="./assets/stylesheet/demo/style.bundle.css" rel="stylesheet" type="text/css" />
                <!--end::Global Theme Styles -->




        <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
    </head>
    <!-- end::Head -->


    <!-- begin::Body -->
    <body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >



    	<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">


				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
	<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside" style="margin: 0px 25%;">
		<div class="m-stack m-stack--hor m-stack--desktop">
				<div class="m-stack__item m-stack__item--fluid">

					<div class="m-login__wrapper">

						<div class="m-login__logo">
							<a href="#">
							<img src="./assets/images/logo/logo.png">
							</a>
						</div>

						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">Login</h3>

															</div>

							<form class="m-login__form m-form" method="POST" action="includes/login.php">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email / Username" name="email" autocomplete="off">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
								</div>

								<div class="m-login__form-action">
									<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air" style="background:#2f67f1 !important; border-color: #2e89f0">Sign In</button>
								</div>
							</form>
              <center>
								<b><a href="http://vericleanirs.htdevs.com">Subcontractors: Get your 1099 Form Here!</a></b>
							</center>
						</div>

					</div>

				</div>
				<div class="m-stack__item m-stack__item--center">

					<div class="m-login__account">

					</div>

				</div>
		</div>
	</div>
</div>


</div>
</body>
</html>
