function showSelect(by) {
  console.log(by);
  var val = $('.filter_by').val()
  if (val == null) {
    val = by
  }
  console.log(val);
  if (val == 'sites') {
    $('.filter_by_client').hide()
    $('.filter_by_sup').hide()
    $('.filter_by_cleaner').hide()
    $('.filter_by_sites').show()
  } else if (val == 'client') {
    $('.filter_by_sites').hide()
    $('.filter_by_sup').hide()
    $('.filter_by_cleaner').hide()
    $('.filter_by_client').show()
  }else if (val == 'supervisor') {
    $('.filter_by_sup').show()
    $('.filter_by_cleaner').hide()
    $('.filter_by_client').hide()
    $('.filter_by_sites').hide()
  }else if (val == 'cleaner') {
    $('.filter_by_sup').hide()
    $('.filter_by_cleaner').show()
    $('.filter_by_client').hide()
    $('.filter_by_client').hide()
  }
}

function moveTo() {
  var go = $('.goto').val
  location.href = `#${go}`
  console.log(go)
}

function showButton() {
  $('.filter').show()
}

function filtro() {


  console.log(mas)
}

function getReports(by, id) {
  var today = moment().format('DD-MM-YYYY');
  var filtro_ = $('#from_date_filter').val()
  var filtro = $('#from_date_filter').val()
console.log(filtro)
  var start
  var end
  if (filtro == null) {
    start = today
    end = today
  } else {
    var m_1 = filtro.substr(0, 2);
    var d_1 = filtro.substr(3, 2);
    var y_1 = filtro.substr(6, 4);

    var m_2 = filtro.substr(13, 2);
    var d_2 = filtro.substr(16, 2);
    var y_2 = filtro.substr(19, 4);

    filtro = `${d_1}-${m_1}-${y_1} - ${d_2}-${m_2}-${y_2}`;

    start = filtro.substr(0, 10)
    end = filtro.substr(13, 10)
  }

  var by_ = ''
  var id_ = ''
  var filter_id
  var filter_by = $('.filter_by').val()
  if (filter_by == 'client') {
    filter_id = $('.client_id').val()

  } else if(filter_by == 'sites') {
    filter_id = $('.site_id').val()
  } else if(filter_by == 'supervisor'){
    filter_id = $('.supervisor_id').val()
  }else if(filter_by == 'cleaner'){
    filter_id = $('.cleaner_id').val()
  }
  console.log(filter_id)
  if (filter_by == null || filter_id == null || filter_by == '' || filter_id == '') {
    by_ = by
    id_ = id
  } else {
    by_ = filter_by
    id_ = filter_id
  }
  console.log(by_);
  console.log(id_);

  $(".loading").show()
  $.ajax({
    type: "POST",
    url: "../../../router/all_reports/index.php",
    data: JSON.stringify({
      start: start,
      finished: end,
      by: by_,
      id: id_
    }),
    dataType: "html",
  }).done(function (data) {

    $(".loading").hide()
    $("#m-portlet__body").html(data);
    showSelect(by_)
    if (filtro_ != null) {
      console.log(filtro_);
      $('#from_date_filter').val(filtro_)
    }
  });
}