function createFrequency(event) {
  event.preventDefault();

  var name = $("#name").val();
  var days = $("#days").val();

  $.ajax({
    type: "POST",
    url: "../../../router/frequencies/create.php",
    data: JSON.stringify({
      name: name,
      days: days,
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Frequency created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../frequencies/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}