function createUser(event) {
  event.preventDefault();

  var first_name = $("#first_name").val();
  var last_name = $("#last_name").val();
  var username = $("#username").val();
  var password = $("#password").val();
  var password_repeat = $("#password_repeat").val();
  var phone = $("#phone").val();
  var address = $("#address").val();
  var status = $("#status").val();
  var email = $("#email").val();
  var role_id = $("#role_id").val();

  var quantityDigitsPassword = password.length

  //VALIDATE FORM
  if (first_name == "" || last_name == "" || username == "" || password == "" || password_repeat == ""
    || phone == "" || address == "" || status == "" || email == "" || role_id == ""
    || password !== password_repeat || quantityDigitsPassword < 8) {
    if (first_name == "") {
      $("#first_name_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#first_name_error").css('display', 'none');
    }
    if (last_name == "") {
      $("#last_name_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#last_name_error").css('display', 'none');
    }
    if (username == "") {
      $("#username_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#username_error").css('display', 'none');
    }
    if (password == "") {
      $("#password_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#password_error").css('display', 'none');
    }
    if (password_repeat == "") {
      $("#password_repeat_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#password_repeat_error").css('display', 'none');
    }
    if (phone == "") {
      $("#phone_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#phone_error").css('display', 'none');
    }
    if (address == "") {
      $("#address_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#address_error").css('display', 'none');
    }
    if (status == "") {
      $("#status_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#status_error").css('display', 'none');
    }
    if (email == "") {
      $("#email_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#email_error").css('display', 'none');
    }
    if (role_id == "") {
      $("#roles_error").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#roles_error").css('display', 'none');
    }
    if (password != password_repeat) {
      $("#password_not_equal").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#password_not_equal").css('display', 'none');
    }
    if (quantityDigitsPassword < 8) {
      $("#password_quantity_not_valid").css('display', 'block');
      $("#errors").css('display', 'block');
    } else {
      $("#password_quantity_not_valid").css('display', 'none');
    }
  } else {
    $.ajax({
      type: "POST",
      url: "../../../router/users/create.php",
      data: JSON.stringify({
        first_name: first_name,
        last_name: last_name,
        username: username,
        password: password,
        password_repeat: password_repeat,
        address: address,
        phone: phone,
        status: status,
        email: email,
        role_id: role_id,
      }),
      dataType: "json"
    }).done(function (data) {    
      if(data !== null){
        if(data.email == "has already been taken" || data.status == 500){
          if(data.email == "has already been taken"){
            swal({
              title: 'Error!',
              text: 'The email is registered with another user',
              type: 'error',
              confirmButtonText: 'Cool'
            })
          }
          if(data.status == 500){
            swal({
              title: 'Error!',
              text: 'The username is registered with another user',
              type: 'error',
              confirmButtonText: 'Cool'
            })
          }
        }
      }else{
        swal({
          title: "Success!",
          text: "User created",
          icon: "success",
          button: "Ok",
          closeOnEsc: true
        }).then(result => {
          window.location.href = "../../../users/index.php";
        });
      }
    }).fail(function (err) {
      console.log(err)
      swal({
        title: 'Error!',
        text: 'Something happened!',
        type: 'error',
        confirmButtonText: 'Cool'
      })
    });
  }
}


//en conjunto con el parametro pattern del input en html5 no permite agregar caracteres especiales
document.getElementById('username').onkeyup = function(event) {
  this.value = this.value.replace(/[^a-z\d]/, '');
}

