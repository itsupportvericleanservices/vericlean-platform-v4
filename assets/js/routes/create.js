function createRoute(event) {
  event.preventDefault();

  var name = $("#name").val();
  var user_id = $("#user_id").val();
  var status = $("#status").val();

  $.ajax({
    type: "POST",
    url: "../../../router/routes/create.php",
    data: JSON.stringify({
      name: name,
      user_id: user_id,
      status: status
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Route created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../routes/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}