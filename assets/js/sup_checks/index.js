function getSupChecks(where) {
  var fil
  var obj = {}
  if (where == 'home') {
    var today = moment().format('DD-MM-YYYY');
    obj = {
      start: today,
      finished: today,
      id: 0,
      full: today
    }
  } else if (where == 'modal') {
    var id = $('.sup_id').val()
    var filtro = $('#from_date_filter').val()
    fil = $('#from_date_filter').val()
    var m_1 = filtro.substr(0, 2);
    var d_1 = filtro.substr(3, 2);
    var y_1 = filtro.substr(6, 4);

    var m_2 = filtro.substr(13, 2);
    var d_2 = filtro.substr(16, 2);
    var y_2 = filtro.substr(19, 4);

    filtro = `${d_1}-${m_1}-${y_1} - ${d_2}-${m_2}-${y_2}`;
    obj = {
      start: filtro.substr(0, 10),
      finished: filtro.substr(13, 10),
      id: id,
      full: filtro
    }
  } else if (where == 'form') {
    var id = $('.sup_id_form').val()
    var filtro = $('#from_date_filter_form').val()
    fil = $('#from_date_filter_form').val()
    var m_1 = filtro.substr(0, 2);
    var d_1 = filtro.substr(3, 2);
    var y_1 = filtro.substr(6, 4);

    var m_2 = filtro.substr(13, 2);
    var d_2 = filtro.substr(16, 2);
    var y_2 = filtro.substr(19, 4);

    filtro = `${d_1}-${m_1}-${y_1} - ${d_2}-${m_2}-${y_2}`;
    obj = {
      start: filtro.substr(0, 10),
      finished: filtro.substr(13, 10),
      id: id,
      full: filtro
    }
  }

  $(".loading").show()
  $.ajax({
    type: "POST",
    url: "../../../router/sup_checks/index.php",
    data: JSON.stringify(obj),
    dataType: "html",
  }).done(function (data) {

    $(".loading").hide()

    $("#m-portlet__body").html(data);

    if (fil != null) {
      $("#from_date_filter_form").val(fil);
    }

    if (obj.id == 0) {
      $('#myModal').modal({
        backdrop: 'static',
        keyboard: false
      })
      $('#myModal').modal('show')

      $('#myModal').on('hidden.bs.modal', function (e) {
        getSupChecks('modal')
      })
    }
  });
}