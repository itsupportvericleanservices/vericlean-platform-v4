function createComments(event, id) {
  event.preventDefault();

  var user_id = $("#user_id").val();
  var comment = $("#comment").val();

  $.ajax({
    type: "POST",
    url: "../../../router/comments/create.php",
    data: JSON.stringify({
      user_id: user_id,
      service_id: id,
      comment: comment
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Comment created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      location.reload()
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });

}