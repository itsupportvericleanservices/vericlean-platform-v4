$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
function getWorkorders(page) {

  window.localStorage.removeItem('filtro_works');

  $.ajax({
    type: "POST",
    url: "../../../router/workorders/index.php",
    data: JSON.stringify({
      page: page,
    }),
    dataType: "html"
  }).done(function (data) {
    $("#m-portlet__body").html(data);
  });

}

function workorders_filter_by() {
  var filter_by = $(".filter_by").val();
  
  if (filter_by == "address" || filter_by == "site_name" || filter_by == "site_code" || filter_by == "customer_work_order") {

    $(".filter_hide").hide();
    $(".filter_by_custom").show();
     
    $(".filter").show();
  }
  if(filter_by == "client") {
    $(".option_remove").remove()
    $(".filter_hide").hide();
    $(".filter_by_client").show();
    
    
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json",
    }).done(function (data) {
      $('.client_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        $('.client_id').append('<option value="' + element.id + '"class="option_remove" >' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "customer") {
    $(".option_remove").remove()
    $(".filter_hide").hide();
    $(".filter_by_customer").show();
    
    
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/indirect_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      $('.customer_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        $('.customer_id').append('<option value="' + element.id + '" class="option_remove">' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "cleaner") {
    $(".filter_hide").hide();
    $(".filter_by_cleaner").show();
   
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/cleaners/index.php",
      dataType: "json"
    }).done(function (data) {
      $('.cleaner_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        $('.cleaner_id').append('<option value="'+element.id+'">'+element.first_name+' '+element.last_name+'</option>');
      });
    });
  }
  if(filter_by == "supervisor") {
    $(".filter_hide").hide();
    $(".filter_by_supervisor").show();
   
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/supervisors/index.php",
      dataType: "json"
    }).done(function (data) {
      $('.supervisor_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        $('.supervisor_id').append('<option value="'+element.id+'">'+element.first_name+' '+element.last_name+'</option>');
      });
    });
  }
  if(filter_by == "state") {
    $(".filter_hide").hide();
    $(".filter_by_state").show();
    
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/branches/states.php",
      dataType: "json"
    }).done(function (data) {
      $('.state').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        if(element.state !== null){
          $('.state').append('<option value="'+element.state+'">'+element.state+'</option>');
        }
      });
    });
  }
  if(filter_by == "city") {
    $(".filter_hide").hide();
    $(".filter_by_city").show();
    
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/branches/cities.php",
      dataType: "json"
    }).done(function (data) {
      $('.city').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        if(element.city !== null){
          $('.city').append('<option value="'+element.city+'">'+element.city+'</option>');
        }
      });
    });
  }
  if (filter_by == "customer_for_client") {

    //limpia el select para tomar nuevos datos
    $(".option_remove").remove()

    $(".filter_hide").hide();
    $(".filter_by_client_for_customer").show();
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      $('.client_for_customer_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        $('.client_for_customer_id').append('<option value="' + element.id + '" class="option_remove">' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "") {
    $(".filter_hide").hide();
    $(".filter_by_client").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    
    $(".filter").hide();
  }
  if(filter_by == "status") {
    $(".filter_hide").hide();
    $(".filter_by_status").show();
    $(".filter").show();
  }

  if(filter_by == "priority") {
    $(".filter_hide").hide();
    $(".filter_by_priority").show();
    $(".filter").show();
  }

  if(filter_by == "main_type") {
    $(".filter_hide").hide();
    $(".filter_by_main_type").show();
    $(".filter").show();
  }

  if(filter_by == "delayed") { // este es calculado
    $(".filter_hide").hide();
    $(".filter_by_delayed").show();
    $(".filter").show();

    var delayed = JSON.parse(localStorage.getItem('delayed'));
    delayed.forEach(element => {
      $('.delayed').append('<option value="'+element+'">'+element+'</option>');
    });  
 
  }
  if(filter_by == "") {
    $(".filter_hide").hide();
    $(".filter_by_delayed").hide();
    $(".filter_by_main_type").hide();
    $(".filter_by_priority").hide();
    $(".filter_by_status").hide();
    $(".filter").hide();
  }
}

function workorders_filter() {
  var filter_by = $(".filter_by").val();
  var status = $(".status").val()
  var priority =  $(".priority").val();
  var main_type = $(".main_type").val();
  var delayed = $(".delayed").val();
  var filter_by_custom = $("#filter_custom").val();
  var client_id = $(".client_id").val();
  var cleaner_id = $(".cleaner_id").val();
  var supervisor_id = $(".supervisor_id").val();
  var state = $(".state").val();
  var city = $(".city").val();
  var customer_id = $(".customer_id").val();
  var customer_for_client_id = $(".client_for_customer_id").val();
  var page = document.getElementsByClassName("current_page")[0].value;
  
  if(filter_by == "status") {
    var searchstr = "filter=true&filter_by="+filter_by+"&status="+status+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "priority") {
    var searchstr = "filter=true&filter_by="+filter_by+"&priority="+priority+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "main_type") {
    var searchstr = "filter=true&filter_by="+filter_by+"&main_type="+main_type+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "delayed") {
    var searchstr = "filter=true&filter_by="+filter_by+"&delayed="+delayed+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if (filter_by == "address") {
    var searchstr = "filter=true&filter_by="+filter_by+"&address="+filter_by_custom+"&page="+page;
  }
  if (filter_by == "site_name") {
    var searchstr = "filter=true&filter_by="+filter_by+"&site_name="+filter_by_custom+"&page="+page;
  }
  if (filter_by == "site_code") {
    var searchstr = "filter=true&filter_by="+filter_by+"&site_code="+filter_by_custom+"&page="+page;
  }
  if (filter_by == "customer_work_order") {
    var searchstr = "filter=true&filter_by="+filter_by+"&customer_work_order="+filter_by_custom+"&page="+page;
  }
  if(filter_by == "client") {
    var searchstr = "filter=true&filter_by="+filter_by+"&client_id="+client_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "customer") {
    var searchstr = "filter=true&filter_by="+filter_by+"&client_id="+customer_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "customer_for_client") {
    var searchstr = "filter=true&filter_by="+filter_by+"&client_id="+customer_for_client_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "cleaner") {
    var searchstr = "filter=true&filter_by="+filter_by+"&cleaner_id="+cleaner_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "supervisor") {
    var searchstr = "filter=true&filter_by="+filter_by+"&supervisor_id="+supervisor_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "state") {
    var searchstr = "filter=true&filter_by="+filter_by+"&state="+state+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "city") {
    var searchstr = "filter=true&filter_by="+filter_by+"&city="+city+"&page="+page;
    $(".searchstr").val(searchstr);
  }

  window.localStorage.setItem('filtro_works', JSON.stringify(searchstr));

  $.ajax({
    type: "POST",
    url: "../../../router/workorders/index.php",
    data: JSON.stringify({
      page: page,
      searchstr: encodeURI(searchstr)
    }),
    dataType: "html"
  }).done(function (data) {
    $("#m-portlet__body").html(data);
  });
}

function sendWoEmail(event, wo_id, invoice){
  
  $.ajax({
    type: "GET",
    url: "../../../router/workorders/get_single.php?id="+wo_id,
    
  }).done(function (data) {
    data = JSON.parse(data);

    console.log(data)
    $('#wo_id').val(wo_id);
    $('#email_to').val(data.request_contact_email);
    $('#internal_id').val(data.internal_id);
    $('#client_name').val(data.client.name);
    $('#client_address').val(data.client.address);
    $('#request_contact').val(data.request_contact);
    $('#email_subject').val('Vericlean Work Order #'+data.internal_id+' has been finished');
    html_photos = '';
    data.photos.forEach(function(photo, index){
      //console.log(photo);
      html_photos += '<img class="img-responsive img-thumbnail" src="'+photo['attachments'][0]['blob']['service_url']+'" height="200px" />'
      
    })

    html_docs = '';
    data.documents.forEach(function(doc, index){
      //console.log(doc);
      html_docs += '<a href="'+doc['attachments'][0]['blob']['service_url']+'" target="_blank"><i class="flaticon-interface-11"></i>'+doc['attachments'][0]['blob']['filename']+'</a><br>'
    })
    html_signatures = '';
    data.signatures.forEach(function(sign, index){
      //console.log(sign);
      html_signatures += '<img class="img-responsive img-thumbnail" src="'+sign['attachments'][0]['blob']['service_url']+'" height="200px" />'
    })
    html_invoices = '';
    data.invoices.forEach(function(inv, index){
      //console.log(doc);
      html_invoices += '<a href="'+inv['attachments'][0]['blob']['service_url']+'" target="_blank"><i class="flaticon-interface-11"></i>'+inv['attachments'][0]['blob']['filename']+'</a><br>'
    })
    html_summernote = 'Hi '+data.request_contact+',<br>The Work Order #'+data.internal_id
      +' has been finished.<br>'
      +'<strong>Client:</strong> '+data.client.name
      +'<br><strong>Site:</strong> '+data.client.address
      +'<br><strong>Task:</strong> '+data.task_name
      +'<br><strong>Task description:</strong> '+data.task_description
      +'<br><strong>Photos:</strong><br> '+html_photos
      +'<br><strong>documents:</strong><br> '+html_docs
      +'<br><strong>Signatures:</strong><br> '+html_signatures

    status_alt=1;

    if (invoice) {
      html_summernote += '<br><strong>Invoice:</strong><br> '+html_invoices;
      status_alt=2;
    }
    $('#summernote').html(html_summernote);
    $('#summernote').summernote({
      
      tabsize: 2,
      height: 200
    });
    $('#sendWoEmailModal .btn-primary').attr('onclick','sendEmail('+status_alt+')')
    $('#sendWoEmailModal').modal('show');
  });

  
}
$("#sendWoEmailModal").on('hidden.bs.modal', function(){
    $('#summernote').summernote('destroy');
});
function sendEmail(status_alt){
  $('.loading').show();
  var wo_id = $("#wo_id").val();
  var internal_id = $("#internal_id").val();
  var client_name = $("#client_name").val();
  var client_address = $("#client_address").val();
  var request_contact = $("#request_contact").val();
  var internal_id = $("#internal_id").val();
  var email_to = $("#email_to").val();

  $.ajax({
    type: "POST",
    url: "../../../router/workorders/email.php",
    data: JSON.stringify({
        id: wo_id,
        internal_id:internal_id,
        client_name:client_name,
        client_address:client_address,
        request_contact:request_contact,
        request_contact_email:email_to,
        body: $('#summernote').summernote('code'),
        status_alt: status_alt
      }),
    dataType: "json"
  }).done(function (data) {
    
    $('.loading').hide();
    swal({
      title: "Success!",
      text: "Email Sent",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      $('#sendWoEmailModal').modal('hide');
      getWorkorders(1)
    });
  });  
}