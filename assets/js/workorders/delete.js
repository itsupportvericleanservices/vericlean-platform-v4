function deleteWorkorder(event, id) {
    event.preventDefault();
  
    Swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "../../../router/workorders/delete.php?id=" + id,
          dataType: "json"
        }).done(function (data) {
            swal({
              title: "Success!",
              text: "Workoder Deleted",
              icon: "success",
              button: "Ok",
              closeOnEsc: true
            }).then(result => {
              location.reload()
            });
        }).fail(function (err) {
          swal({
            title: 'Error!',
            text: 'Something happened!',
            type: 'error',
            confirmButtonText: 'Cool'
          })
        });
      }
  
    })
  }

