
function addAttachment(type) {
  html = '<div class="'+type+'-container">'+
            '<hr style="border-top: dotted 1px;width:90%;align:center;" />'+
            '<div class="row">'+
              '<div class="col-md-6">'+
                '<input type="text" class="form-control m-input attachment_name" placeholder="Enter an optional custom name...">'+
              '</div>'+
              '<div class="col-md-4">'+
                '<input type="file" class="custom-file-input attachments" data-category="'+type+'" onchange="b64FilenameFix(event, this);">'+
                '<label class="custom-file-label" for="'+type+'s">Choose one...</label>'+
              '</div>'+
              '<div class="col-md-2 text-right">'+
                '<button class="btn btn-danger" onclick="removeFileInput(event, this);">Cancel</button>'+
              '</div>'+
            '</div>'+
          '</div>';
  $("."+type+"s-container").append(html);
}

function b64FilenameFix(event, elm) {
  event.preventDefault();

  if($(elm)[0].files.length == 0) {
    file_name = "Choose One..."
  } else {
    file_name = $(elm)[0].files[0].name;
  }

  $(elm).parent().find('.custom-file-label').html(file_name);
}

function removeFileInput(event, elm) {
  event.preventDefault();
  $(elm).parent().parent().parent().hide('slow', function() {
    $(this).remove();
  });
}

documents_arr = [];
photos_arr = [];
async function b64Files(event) {
  event.preventDefault();
  $('.loading').show();
  attachments_count = 0;
  attachments_total = $('.attachments').length;
  if(attachments_total == 0) {
    createWorkorder(event);
    return;
  }
  $('.attachments').each(function() {
    category = $(this).data('category');
    custom_name = $(this).parent().parent().find('.attachment_name').val();

    var files = $(this)[0].files;
    if(files.length > 0) {
      for (var i = 0; i < files.length; i++) {
        // Persistent category FIX
        var category = category;
        var file = files[i];
        if (file.type == '') {
          content_type = 'application/'+file.name.split('.').pop();
        } else {
          content_type = file.type;
        }

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
          if (category == "document") {
            files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
            documents_arr.push(files_arr_aux);
          } else {
            files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
            photos_arr.push(files_arr_aux);
          }
          attachments_count++;
          if(attachments_count >= attachments_total) {
            createWorkorder(event);
          }
        };
        reader.onerror = function (error) {
          return JSON.stringify({"response": "error", "error": error});
        };
      }
    } else {
      attachments_total--;
    }

    // Create Work Order
    if(attachments_count >= attachments_total) {
      createWorkorder(event);
    }
  });
}

async function getBase64(file, category, content_type, custom_name) {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    if (category == 'documents') {
      files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
      documents_arr.push(files_arr_aux);
    } else {
      files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
      photos_arr.push(files_arr_aux);
    }
    response = JSON.stringify({"response": "encoded", "file": files_arr_aux});
    return response;
  };
  reader.onerror = function (error) {
    return JSON.stringify({"response": "error", "error": error});
  };

}

function createWorkorder(event, from='workorders') {
  event.preventDefault();
  var ticket_id = (from == 'tickets') ? $("#ticket_id").val() : null;
  var internal_id = $("#internal_id").val();
  var user_id = $("#user_id").val();
  var branch_id = $("#branch_id").val();
  var cleaner_id = $("#cleaner_id").val();
  var requested_date = $("#requested_date").val();
  var due_date = $("#due_date").val();
  var site_contact = $("#site_contact").val();
  var request_contact_email = $("#request_contact_email").val();
  var site_phone = $("#site_phone").val();
  var request_contact = $("#request_contact").val();
  var request_phone = $("#request_phone").val();
  var task_name = $("#task_name").val();
  var task_description = $("#task_description").val();
  var status = $("#status").val();
  var type = $("#type").val();
  var priority = $("#priority").val();
  var fee = $("#fee").val();
  var date = $("#scheduled_at_date").val();
  var time = $("#scheduled_at_time").val();
  var scheduled_at = `${date} ${time}`
  var comments = $("#comments").val();

  var instructions = $("#instructions").val();

    var client_id = $("#client_id").val();
    var customer_id = $("#customer_id").val();
    var type_client_id = 0

    var type_client = $("#type_client").val();
    if(type_client == "client"){
      type_client_id = client_id
    }
    if(type_client == "customer"){
      type_client_id = customer_id
    }

  $.ajax({
    type: "POST",
    url: "../../../router/workorders/create.php",
    data: JSON.stringify({
      ticket_id: ticket_id,
      internal_id: internal_id,
      user_id: user_id,
      branch_id: branch_id,
      client_id: type_client_id,
      cleaner_id: cleaner_id,
      requested_date: requested_date,
      due_date: due_date,
      site_contact: site_contact,
      site_phone: site_phone,
      request_contact: request_contact,
      request_contact_email: request_contact_email,
      request_phone: request_phone,
      task_name: task_name,
      task_description: task_description,
      status: status,
      main_type: type,
      priority: priority,
      fee: fee,
      scheduled_date: scheduled_at,
      comments: comments,
      instructions: instructions,
      documents: documents_arr,
      photos: photos_arr
    }),
    dataType: "json"
  }).done(function (data) {
    $('.loading').hide();
    swal({
      title: "Success!",
      text: "Workorder created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      if (from == 'workorders') {
        window.location.href = "../../../workorders/index.php";
      }else{
        window.location.reload();
      }
    });
  }).fail(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}

function selectClient(){

  var type_client = $("#type_client").val();

  if(type_client == "client"){
    $("#client_content").show();
    $(".option_remove").remove()

    //enviar ajax para llenar el select de clients
    $.ajax({
      type: "POST",
      url: "../../../router/clients/all_clients_or_customers.php",
      data: JSON.stringify({
        type: "client",
        indirect: 0
      }),
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('#client_id').append('<option value="'+element.id+'" class="option_remove">'+element.name+'</option>');
      });
    });
  }else{
    $("#client_content").hide();
  }

  if(type_client == "customer"){
    $("#customer_content").show();
    $(".option_remove").remove()

    //enviar ajax para llenar el select de clients
    $.ajax({
      type: "POST",
      url: "../../../router/clients/all_clients_or_customers.php",
      data: JSON.stringify({
        type: "customer",
        indirect: 1
      }),
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('#customer_id').append('<option value="'+element.id+'" class="option_remove">'+element.name+'</option>');
      });
    });
  }else{
    $("#customer_content").hide();
  }
}

function sitesForClient(){
  var client_id = $("#client_id").val();
  var customer_id = $("#customer_id").val();
  $("#branch_id_container").show();

  var type = (client_id != '') ? "client" : "customer";
  client_id = (client_id != '') ? client_id : customer_id;

  $.ajax({
    type: "POST",
    url: "../../../router/clients/sites_for_client_or_customer.php",
    data: JSON.stringify({
      type: type,
      client_id: client_id
    }),
    dataType: "json",
  }).done(function (data) {
    data.forEach(element => {
      var name_desc = '';
      name_desc += (element.site_code) ? element.site_code : '';
      name_desc += (element.name) ? ' - '+element.name : '';
      name_desc += (element.address) ? ' - '+element.address : '';
      $('#branch_id').append('<option value="' + element.id + '" class="option_remove">' + name_desc + '</option>');

    });
  });

}

function enableNext(elem, last){

  if ($(elem).val() != '') {

    $(elem).parent().next('.m-form__group.hidden').show()

    $(elem).parent().next('.m-form__group').nextAll('.m-form__group.hidden').each(function(){

      if ($(this).find('.form-control.m-input').val() != '') {
        $(this).next('.m-form__group.hidden').show();
        $(this).show();
      }

    });

    if (last == 1) {
      $('.btn-primary.hidden').show();
    }

  }else{
    $(elem).parent().nextAll('.m-form__group.hidden').hide();
    if (last == 1) {
      $('.btn-primary.hidden').hide();
    }
  }


}
function parseHTMLFile(event, evt){

  var f = event.target.files[0];

  if (f) {
      var r = new FileReader();
      r.onload = function (e) {
          var contents = e.target.result;
          $("#html_wo").html(contents);
          // First branch by site code.
          var site_code = $.trim($($('#html_wo').find('.woprintLabel2')[7]).parent().next().text()).split('BOA-')[1];
          $.ajax({
            type: "GET",
            url: "../../../router/branches/get_by_code.php?site_code="+site_code,
            dataType: "html"
          }).done(function (data) {
            //console.log(data);
            data = JSON.parse(data)
            console.log(data)
            if (data.length == 0) {
              swal({
                title: "Success!",
                text: "Site with code:\""+site_code+"\" has no found\nPlease updaload other file",
                icon: "warning",
                button: "Ok",
                closeOnEsc: true
              }).then(result => {
                
              });
            }else{
              $('#branch_id').val(data[0]['id']);
            }
          });

          var file = f;
          if (file.type == '') {
            content_type = 'application/'+file.name.split('.').pop();
          } else {
            content_type = file.type;
          }

          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function () {
            
            files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': ''}
            documents_arr.push(files_arr_aux);
            
          };
          reader.onerror = function (error) {
            return JSON.stringify({"response": "error", "error": error});
          };

      }
      r.readAsText(f);
  } else {
      alert("Failed to load file");
  }
  
}

function saveWoFile(event){
  event.preventDefault();
  $('.loading').show();
  var user_id = $("#user_id").val();
  var branch_id = $("#branch_id").val();
  var internal_id = $.trim($($($('#html_wo').find('.woprintHeading2')[1]).find('big')).text());
  var extra_id = $.trim($($('#html_wo').find('.woprintHeading2')[3]).text());

  var site_code = $.trim($($('#html_wo').find('.woprintLabel2')[7]).parent().next().text()).split('BOA-')[1];
  var site_contact = $.trim($($('#html_wo').find('.woprintLabel2')[8]).parent().next().text());
  var request_contact_email = $.trim($($('#html_wo').find('.woprintLabel2')[14]).parent().next().text());
  var site_contact_phone = $.trim($($('#html_wo').find('.woprintLabel2')[10]).parent().next().text());
  var site_address = $($('#html_wo').find('.woprintLabel2')[11]).parent().next().html().split('&nbsp;&nbsp')[0];
  var requested_date = $.trim($($('#html_wo').find('.woprintLabel2')[16]).parent().text().split("Created Date:")[1]).replace(/\s+/g,' ');
  var due_date = $.trim($($('#html_wo').find('.woprintLabel2')[20]).parent().text().split("Please Service By:")[1]).replace(/\s+/g,' ');
  

  var task_data= $($('#html_wo').find('.woprintLabel2')[22]).parent().parent().next().children();
  var task_name = $(task_data[1]).text();
  var task_description = $(task_data[2]).text();
  var instructions =$.trim($($('#html_wo').find('.woprintLabel2')[48]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[49]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[50]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[51]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[52]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[53]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[54]).text())+'\n'+
                    $.trim($($('#html_wo').find('.woprintLabel2')[55]).text());

  $.ajax({
    type: "POST",
    url: "../../../router/workorders/create.php",
    data: JSON.stringify({
      internal_id: internal_id,
      extra_id: extra_id,
      user_id: user_id,
      branch_id: branch_id,
      client_id: 2,
      
      requested_date: requested_date,
      due_date: due_date,
      site_contact: site_contact,
      request_contact_email: request_contact_email,
      site_phone: site_contact_phone,
      request_contact: site_contact,
      request_phone: site_contact_phone,
      task_name: task_name,
      task_description: task_description,
      status: 0,
      main_type: 'request',
      priority: 1,
      instructions: instructions,
      documents: documents_arr
    }),
    dataType: "json"
  }).done(function (data) {
    $('.loading').hide();
    swal({
      title: "Success!",
      text: "Workorder created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../workorders/index.php";
    });
  }).fail(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
  console.log(instructions)
  //console.log(task_description)
}