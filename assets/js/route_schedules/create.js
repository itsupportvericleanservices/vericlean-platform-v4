function createRouteSchedule(event) {
    event.preventDefault();
  
    var user_id = $("#user_id").val();
  
    $.ajax({
      type: "POST",
      url: "../../../router/route_schedules/create.php",
      data: JSON.stringify({
        user_id: user_id,
      }),
      dataType: "json"
    }).done(function (data) {
      swal({
        title: "Success!",
        text: "Cleaner Associated With Route",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
        window.location.href = "../../../route_schedules/index.php";
      });
    }).error(function (err) {
      swal({
        title: 'Error!',
        text: 'Something happened!',
        type: 'error',
        confirmButtonText: 'Cool'
      })
    });
  }