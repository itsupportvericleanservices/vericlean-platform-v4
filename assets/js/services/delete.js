function deleteService(event, id) {
  event.preventDefault();

  Swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../../../router/services/delete.php?id=" + id,
        dataType: "json"
      }).done(function (data) {
        if (data == true) {
          swal({
            title: "Success!",
            text: "Service Deleted",
            icon: "success",
            button: "Ok",
            closeOnEsc: true
          }).then(result => {
            location.reload()
          });
        } else {
          Swal({
            type: 'error',
            title: 'You can´t delete the service',
            text: 'The service is in use',
          })
        }
      }).error(function (err) {
        swal({
          title: 'Error!',
          text: 'Something happened!',
          type: 'error',
          confirmButtonText: 'Cool'
        })
      });
    }

  })
}