function updateService(event, id) {
  event.preventDefault();

  var branch_id = $("#branch_id").val();
  var route_schedule_id = $("#route_schedule_id").val();
  var started_at = $("#started_at").val();
  var finished_at = $("#finished_at").val();
  var status = $("#status").val();

  $.ajax({
    type: "POST",
    url: "../../../router/services/update.php?id=" + id,
    data: JSON.stringify({
      branch_id: branch_id,
      route_schedule_id: route_schedule_id,
      started_at: started_at,
      finished_at: finished_at,
      status: status
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Service updated",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../services/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}