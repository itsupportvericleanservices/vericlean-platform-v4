function createService(event) {
  event.preventDefault();

  service_type_id = []
  $('.check_service').each(function (i, serv) {
    if ($(serv).is(':checked')) {
      service_type_id.push($(serv).data('id'));
    }
  });

  var branch_id = $("#branch_id").val();
  var route_schedule_id = $("#route_schedule_id").val();
  var started_at = $("#started_at").val();
  var finished_at = $("#finished_at").val();
  var status = $("#status").val();

  $.ajax({
    type: "POST",
    url: "../../../router/services/create.php",
    data: JSON.stringify({
      branch_id: branch_id,
      route_schedule_id: route_schedule_id,
      started_at: started_at,
      finished_at: finished_at,
      status: status,
      service_types: service_type_id
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Service created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../services/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}