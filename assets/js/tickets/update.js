function addAttachment(type) {
  html = '<div class="'+type+'-container">'+
            '<hr style="border-top: dotted 1px;width:90%;align:center;" />'+
            '<div class="row">'+
              '<div class="col-md-6">'+
                '<input type="text" class="form-control m-input attachment_name" placeholder="Enter an optional custom name...">'+
              '</div>'+
              '<div class="col-md-4">'+
                '<input type="file" class="custom-file-input attachments" data-category="'+type+'" onchange="b64FilenameFix(event, this);">'+
                '<label class="custom-file-label" for="'+type+'s">Choose one...</label>'+
              '</div>'+
              '<div class="col-md-2 text-right">'+
                '<button class="btn btn-danger" onclick="removeFileInput(event, this);">Cancel</button>'+
              '</div>'+
            '</div>'+
          '</div>';
  $("."+type+"s-container").append(html);
}

function b64FilenameFix(event, elm) {
  event.preventDefault();

  if($(elm)[0].files.length == 0) {
    file_name = "Choose One..."
  } else {
    file_name = $(elm)[0].files[0].name;
  }

  $(elm).parent().find('.custom-file-label').html(file_name);
}

function removeFileInput(event, elm) {
  event.preventDefault();
  $(elm).parent().parent().parent().hide('slow', function() {
    $(this).remove();
  });
}


function updateTicket(event, ticket_id) {
  event.preventDefault();
  
  var branch_id = $("#branch_id").val();
  var event_date = $("#event_date").val();
  var due_date = $("#due_date").val();
  var request_contact = $("#request_contact").val();
  var request_contact_email = $("#request_contact_email").val();
  var request_contact_phone = $("#request_contact_phone").val();
  var short_description = $("#short_description").val();
  var description = $("#description").val();
  var status = $("#status").val();

  //var documents = $("#document").val() - ver como hacer aqui
  json = JSON.stringify({
    branch_id: branch_id,
      event_date: event_date,
      due_date: due_date,
      request_contact: request_contact,
      request_contact_email: request_contact_email,
      request_contact_phone: request_contact_phone,
      short_description: short_description,
      description: description,
      status: status,
  });


  $.ajax({
    type: "POST",
    url: "../../../router/tickets/update.php?id=" + ticket_id,
    data: json,
    dataType: "json"
  }).done(function (data) {
    $('.loading').hide();
    // if(data.id > 0) {
      swal({
        title: "Success!",
        text: "Ticket updated",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
       // window.location.href = "../../../tickets/index.php";
      });
    // } else {
    //   swalError();
    // }
  }).fail(function (err) {
    $('.loading').hide();
    swalError();
  });
}

function verifydispatched() {
  var status = $("#status").val();
  var actual_status = $("#actual_status").val();

  if (actual_status == 3 && status == 4) {
    $("#status").val(5);
  }
  if (actual_status == 4 && status == 3) {
    $("#status").val(5);
  }
}


function selectClient(){

  var type_client = $("#type_client").val();

  if(type_client == "client"){
    $('.type_'+0).show();
    $('.type_'+1).hide();
  }else{
    $('.type_'+0).hide();
    $('.type_'+1).show();
  }
  $('#lbl_client_customer').html(type_client+'s');
  $("#client_id").val('');
  
}

function sitesForClient(){
  console.log("entrio");
  var client_id = $("#client_id").val();
  var customer_id = $("#customer_id").val();
  $("#branch_id_container").show();

  var type = (client_id != '') ? "client" : "customer";
  client_id = (client_id != '') ? client_id : customer_id;

  $.ajax({
    type: "POST",
    url: "../../../router/clients/sites_for_client_or_customer.php",
    data: JSON.stringify({
      type: type,
      client_id: client_id
    }),
    dataType: "json",
  }).done(function (data) {
    $('#branch_id').html('<option value="" selected>Select One</option>');
    data.forEach(element => {
      var name_desc = '';
      name_desc += (element.site_code) ? element.site_code : '';
      name_desc += (element.name) ? ' - '+element.name : '';
      name_desc += (element.address) ? ' - '+element.address : '';
      $('#branch_id').append('<option value="' + element.id + '" class="option_remove">' + name_desc + '</option>');

    });
  });

}

function saveComment(){
  $('.loading').show();
  var comment = $("#comment").val();
  var ticket_id = $("#ticket_id").val();
  $.ajax({
    type: "POST",
    url: "../../../router/tickets/add_comment.php",
    data: JSON.stringify({id: ticket_id,comment: comment}),
    dataType: 'json'
  }).done(function (data) {
    $('.loading').hide();
    // if(data.id > 0) {
      swal({
        title: "Success!",
        text: "Comment saved",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
        window.location.reload();
      });
    // } else {
    //   swalError();
    // }
  }).fail(function (err) {
    $('.loading').hide();
    swalError();
  });
}