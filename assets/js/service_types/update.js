function updateServiceType(event, id) {
  event.preventDefault();

  var name = $("#name").val();
  var min_time = $("#min_time").val();
  var max_time = $("#max_time").val();

  $.ajax({
    type: "POST",
    url: "../../../router/service_types/update.php?id=" + id,
    data: JSON.stringify({
      name: name,
      min_time: min_time,
      max_time: max_time,
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Service Type created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../service_types/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}