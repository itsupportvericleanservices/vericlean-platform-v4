function deleteServiceType(event, id) {
  event.preventDefault();

  Swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../../../router/service_types/delete.php?id=" + id,
        dataType: "json"
      }).done(function (data) {
        if (data == true) {
          swal({
            title: "Success!",
            text: "Service Type Deleted",
            icon: "success",
            button: "Ok",
            closeOnEsc: true
          }).then(result => {
            location.reload()
          });
        } else {
          Swal({
            type: 'error',
            title: 'You can´t delete the service type',
            text: 'The service type is in use',
          })
        }
      }).error(function (err) {
        swal({
          title: 'Error!',
          text: 'Something happened!',
          type: 'error',
          confirmButtonText: 'Cool'
        })
      });
    }

  })
}