function createClient(event) {
  event.preventDefault();

  var name = $("#name").val();
  var address = $("#address").val();
  var address2 = $("#address2").val();
  var state = $("#state").val();
  var city = $("#city").val();
  var zipcode = $("#zipcode").val();
  var phone = $("#phone").val();
  var email = $("#email").val();
  var contact_name = $("#contact_name").val();

  var active = 0
  if ($("#active").is(':checked')) {
    active = 1;
  }

  var indirect_id = $("#indirect_id").val();
  var indirect = 0;

  if(indirect_id == null || indirect_id == ""){
    indirect_id = 0
  }
  
  $.ajax({
    type: "POST",
    url: "../../../router/clients/create.php",
    data: JSON.stringify({
      name: name,
      address: address,
      address2: address2,
      state: state,
      city: city,
      zipcode: zipcode,
      phone: phone,
      email: email,
      contact_name: contact_name,
      active: active,
      indirect: indirect,
      indirect_id: indirect_id,
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Customer created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../clients/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}


function selectClient(){

  var type_client = $("#type_client").val();
  if(type_client == "customer"){

    $("#indirect_id_content").show();

    //enviar ajax para llenar el select de clients
    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients.php",
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('#indirect_id').append('<option value="'+element.id+'">'+element.name+'</option>');
      });    
    });  
  }else{
    $("#indirect_id_content").hide();
  }
}