
function getActiveClients() {
  window.localStorage.removeItem('filtro_clients');
  $(".loading").show()
  $.ajax({
    type: "POST",
    url: "../../../router/clients/index.php",
    dataType: "html"
  }).done(function (data) {
    $(".loading").hide()
    $("#m-portlet__body").html(data);
  });

}

function clients_filter_by() {
  var filter_by = $(".filter_by").val();

  if (filter_by == "client") {

    //limpia el select para tomar nuevos datos
    $(".option_remove").remove()

    $(".filter_by_client").show();
    $(".filter_by_customer").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json",
    }).done(function (data) {
      data.forEach(element => {
        $('.client_id').append('<option value="' + element.id + '" >' + element.name + '</option>');
      });
    });
  }
  if (filter_by == "customer") {

    //limpia el select para tomar nuevos datos
    $(".option_remove").remove()

    $(".filter_by_customer").show();
    $(".filter_by_client").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/indirect_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('.customer_id').append('<option value="' + element.id + '" class="option_remove">' + element.name + '</option>');
      });
    });
  }
  if (filter_by == "customer_for_client") {

    //limpia el select para tomar nuevos datos
    $(".option_remove").remove()

    $(".filter_by_client_for_customer").show();
    $(".filter_by_customer").hide();
    $(".filter_by_client").hide();
    $(".filter").show();


    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('.client_for_customer_id').append('<option value="' + element.id + '" class="option_remove">' + element.name + '</option>');
      });
    });
  }
}

function clients_filter() {

  var filter_by = $(".filter_by").val();
  var client_id = $(".client_id").val();
  var customer_id = $(".customer_id").val();
  var customer_for_client_id = $(".client_for_customer_id").val();


  //Se envia los tipos de dato (client, customer and customer_for_client) por si se necesita
  //enviar otros datos en el futuro y ya tenerlo clasificado.
  if(filter_by == "client") {
    $.ajax({
      type: "POST",
      url: "../../../router/clients/index.php",
      data: JSON.stringify({
        id: client_id,
        type: 'client',
      }),
      dataType: "html"
    }).done(function (data) {
      $("#m-portlet__body").html(data);
    });
  }
  if(filter_by == "customer") {
    $.ajax({
      type: "POST",
      url: "../../../router/clients/index.php",
      data: JSON.stringify({
        id: customer_id,
        type: 'customer'
      }),
      dataType: "html"
    }).done(function (data) {
      $("#m-portlet__body").html(data);
    });
  }
  if(filter_by == "customer_for_client") {
    
    $.ajax({
      type: "POST",
      url: "../../../router/clients/index.php",
      data: JSON.stringify({
        client_id: customer_for_client_id,
        type: 'customer_for_client',
        active: 1,
      }),
      dataType: "html"
    }).done(function (data) {
      $("#m-portlet__body").html(data);
    });
  }

}
