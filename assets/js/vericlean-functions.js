
$('#date_filter').change(function(){
  location.href = '?date='+$(this).val();
})
//Create Users
function addUser(event) {
  event.preventDefault();
  $(".loading").show();
  var first_name = $(".first_name").val();
  var last_name = document.getElementById("last_name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var confirm_password = document.getElementById("confirm_password").value;
  var company = document.getElementById("company")
  companySelected = company.options[company.selectedIndex].value;
  var role = document.getElementById("role")
  roleSelected = role.options[role.selectedIndex].value;
  var branch = document.getElementById("branch")
  branchSelected = branch.options[branch.selectedIndex].value;
  // console.log(branchSelected);
  // return
  emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  if (first_name != '' && last_name != '' && email != '' && password != '' && confirm_password != '' && role != '') {
    if (emailRegex.test(email)) {
      if (password == confirm_password) {
        if (password.length >= 8) {
          $.ajax({
            type: "POST",
            url: "../router/usuarios/crear.php",
            data: JSON.stringify({
              "user": {
                first_name: first_name,
                last_name: last_name,
                email: email,
                password: password,
                role_id: roleSelected,
                password_confirmed: confirm_password,
                company_id: companySelected,
                branch_id: branchSelected
              }
            }),
            dataType: "json"
          }).done(function (data) {
            $(".loading").hide();
            swal("success!", "Your user was saved successfully!", "success")
              .then((value) => {
                location.href = "../users/index.php";
              });
          });
        } else {
          $(".loading").hide();
          swal("Error!", "your password must contain at least 8 characters", "error")
            .then((value) => {});
        }
      } else {
        $(".loading").hide();
        swal("Error!", "Passwords do not match!", "error")
          .then((value) => {});
      }
    } else {
      $(".loading").hide();
      swal("Error!", "Check your email!", "error")
        .then((value) => {});
    }
  } else {
    $(".loading").hide();
    swal("Error!", "Please fill in the form!", "error")
      .then((value) => {});
  }
}



// Create Users

function addUser(event) {
  event.preventDefault();
  $(".loading").show();
  var first_name = $(".first_name").val();

  $.ajax({
    type: "POST",
    url: "../router/users/create.php",
    data: JSON.stringify({
      "user": {
        first_name: first_name
      }
    }),
    dataType: "json"
  }).done(function (data) {
    $(".loading").hide();
    swal("success!", "Your user was saved successfully!", "success")
      .then((value) => {
        location.href = "../users/index.php";
      });
  });
}

function sendPush(event, token){
  console.log(token)
  event.preventDefault();
  $.ajax({
    type: "POST",
    url: "../../router/push.php",
    data: JSON.stringify({
        token: token,
        message: "Testing Vericlean Notification"
    }),
    dataType: "json"
  }).done(function (data) {
    console.log(data);
  });
}

// function autoSendPush(token){
//   console.log(token)
//   $.ajax({
//     type: "POST",
//     url: "../../router/push.php",
//     data: JSON.stringify({
//         token: token
//     }),
//     dataType: "json"
//   }).done(function (data) {
//     console.log(data);
//   });
// }

function swalError() {
  swal({
    title: 'Error!',
    text: 'Something happened, please check and try again!',
    type: 'error',
    icon: "error",
    confirmButtonText: 'Ok'
  });
}
