function showDaleyed(event){
  event.preventDefault();
  if ($('#delay_check').is(':checked')) {

    $('.tr_cleaner').hide();

    

    $('.tr_branch').hide();
    $('.tr_branch').parent().parent().parent().parent().hide();

    $('.warning_yellow').show();
    $('.warning_red').show();
    $('.warning_orange').show();

    $('.warning_yellow').parent().parent().parent().parent().show();
    $('.warning_red').parent().parent().parent().parent().show();
    $('.warning_orange').parent().parent().parent().parent().show();
  }else{
    $('.tr_cleaner').show();
    $('.tr_branch').show();

    $('.tr_branch').parent().parent().parent().parent().show();
  }
}
function rescheduleConfirm(event, cleaner_id, branch_id){
  event.preventDefault();
  c = confirm('Do you want to reschedule this site?');
  if (c) {
    BtnRescheduled(event, cleaner_id, branch_id);
  }
}
function rescheduledSite(event, id_table) {
  event.preventDefault();
  $(".BtnRescheduled" + id_table).css('display', 'block');
}

function BtnRescheduled(event, cleaner_id,branch_id) {
  event.preventDefault();

  
  $.ajax({
    type: "POST",
    url: "../../../router/route_histories/rescheduled.php",
    data: JSON.stringify({
      assigned_id: cleaner_id,
      user_id: cleaner_id,
      branch_id: branch_id,
      scheduled_date: $('#date_filter').val(),
    }),
    dataType: "json"
  }).done(function (data) {

    fetch(`http://52.52.229.187/users/${cleaner_id}/token_notification`, {
              method: "GET",
              headers: {"Content-Type": "application/json"}
          })
          .then(res => res.json())
          .then(data => {
                
            console.log(data.token)
            autoSendPushRescheduled(data.token,"you have a reprogramming")
        })
    swal({
      title: "Success!",
      text: "Site Rescheduled!",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../routes_cleaners/index.php";
    });
  });

}


function autoSendPushRescheduled(token, message){
  var key = 'AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q';
  var to = token;
  var notification = {
    'title': 'Vericlean',
    'body': message
  };

  fetch('https://fcm.googleapis.com/fcm/send', {
    'method': 'POST',
    'headers': {
      'Authorization': 'key=AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q',
      'Content-Type': 'application/json'
    },
    'body': JSON.stringify({
      'data': notification,
      'to': to
    })
  }) 
  .then(res => res.json())
  .then(data => {
                
    console.log(data)  
  })  
}
