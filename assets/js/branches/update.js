function updateBranch(event, id) {
  event.preventDefault();
  var cleaners_exists = $("#cleaners_exists").val();
  var area_managers_exists = $("#area_managers_exists").val();
  var supervisors_exists = $("#supervisors_exists").val();

  if (cleaners_exists == 0 || area_managers_exists == 0 || supervisors_exists == 0) {
    if (cleaners_exists == 0) {
      $("#cleaner_should_exist").css("display", "block");
    }
    if (area_managers_exists == 0) {
      $("#area_manager_should_exist").css("display", "block");
    }
    if (supervisors_exists == 0) {
      $("#supervisor_should_exist").css("display", "block");
    }
  } else {
    service_type_id = []
    $('.check_service').each(function (i, serv) {
      if ($(serv).is(':checked')) {
        service_type_id.push($(serv).data('id'));
      }
    });
    frequencies = []
    $('.check_frequency').each(function (i, freq) {
      if ($(freq).is(':checked')) {
        frequencies.push($(freq).data('id'));
      }
    });

    var active = 0
    var muted = 0
    if($("#active").is(':checked')){
      active = 1;
    }
    if($("#muted").is(':checked')){
      muted = 1;
    }
    
    var frequency = frequencies.toString();
    var client_id = $("#client_id").val();
    var name = $("#name").val();
    var address = $("#address").val();
    var address2 = $("#address2").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var zipcode = $("#zipcode").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var contact_name = $("#contact_name").val();
    var status = $("#status").val();
    var max_time = $("#max_time").val();
    var min_time = $("#min_time").val();
    var site_type = $("#site_type").val();
    var site_code = $("#site_code").val();
    var weekly_frequency = $("#weekly_frequency").val();
    var cleaner_id = $("#cleaner_id").val();
    var area_manager_id = $("#area_manager_id").val();
    var supervisor_id = $("#supervisor_id").val();
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();
    var duration_max = $("#duration_max").val();
    var duration_min = $("#duration_min").val();
    var extra_cleaners = $("#extra_cleaner_ids").val();
    var extra_area_managers = $("#extra_area_manager_ids").val();
    var extra_supervisors = $("#extra_supervisor_ids").val();

    var hora = Number(max_time.substr(0,2)) - 6
    var minutos = max_time.substr(3,2)
    var time = max_time.substr(6,3)

    var arrivale_max = `${hora}:${minutos} ${time}`
    console.log(arrivale_max)
    let datos = ['site_code','state','address','city','zipcode','max_time','duration_min','duration_max','latitude','longitude']

    let inputs = document.getElementsByTagName('input')
    let inp = [...inputs]
    let filtrados = datos.map(el => {

        let input = inp.find(inp=>{
          return inp.id ==  el
        })
        if (input.value == "") {
          input.classList.add("is-invalid")
        }else if(input.classList.contains("is-invalid")){
          input.classList.remove("is-invalid")
        }
        return input
    })

    let verifica = (arr, fn) => arr.every(fn);
    let stat = verifica(filtrados,x => x.value != "")
    
    if (stat == true) {
      $.ajax({
        type: "POST",
        url: "../../../router/branches/update.php?id=" + id,
        data: JSON.stringify({
          client_id: client_id,
          frequency: frequency,
          weekly_frequency: weekly_frequency,
          name: name,
          address: address,
          address2: address2,
          state: state,
          city: city,
          zipcode: zipcode,
          phone: phone,
          email: email,
          contact_name: contact_name,
          status: status,
          max_time: max_time,
          min_time: min_time,
          service_types: service_type_id,
          site_type: site_type,
          site_code: site_code,
          cleaner_id: cleaner_id == '' ? null : cleaner_id,
          area_manager_id: area_manager_id == '' ? null : area_manager_id,
          supervisor_id: supervisor_id == '' ? null : supervisor_id,
          latitude: latitude,
          longitude: longitude,
          active: active,
          muted: muted,
          duration_max: duration_max,
          duration_min: duration_min,
          extra_cleaners: extra_cleaners,
          extra_area_managers: extra_area_managers,
          extra_supervisors: extra_supervisors,
        }),
        dataType: "json"
      }).done(function (data) {
        console.log(data)
        swal({
          title: "Success!",
          text: "Site updated",
          icon: "success",
          button: "Ok",
          closeOnEsc: true
        }).then(result => {
          window.location.href = "../../../branches/index.php";
        });
      }).error(function (err) {
        console.log(err)
        swal({
          title: 'Error!',
          text: 'Something happened!',
          type: 'error',
          confirmButtonText: 'Cool'
        })
      });
    }else{
      swal({
        title: 'Error!',
        text: 'Verify you data is complete!',
        type: 'error',
        confirmButtonText: 'ok'
      })
    }
  }
}

function updateBranchLocation(event, id, lat, lng) {
  event.preventDefault();



    var latitude = lat
    var longitude = lng
    var status = 2

      $.ajax({
        type: "POST",
        url: "../../../router/branches/update.php?id=" + id,
        data: JSON.stringify({
          latitude: latitude,
          longitude: longitude,
          status_request_update: status
        }),
        dataType: "json"
      }).done(function (data) {
        console.log(data)
        swal({
          title: "Success!",
          text: "Site updated",
          icon: "success",
          button: "Ok",
          closeOnEsc: true
        }).then(result => {
          window.location.href = "../../../branches/requestList.php";
        });
      })
    
}

function updateBranchLocationReject(event, id) {
  event.preventDefault();
    console.log("object")
    var status = 2
    
      $.ajax({
        type: "POST",
        url: "../../../router/branches/update.php?id=" + id,
        data: JSON.stringify({
          status_request_update: status
        }),
        dataType: "json"
      }).done(function (data) {
        console.log(data)
        swal({
          title: "Success!",
          text: "Site updated",
          icon: "success",
          button: "Ok",
          closeOnEsc: true
        }).then(result => {
          window.location.href = "../../../branches/requestList.php";
        });
      })
    
}



