function checkModal(type,branch_name, branch_id, user_id){
	if (type == 0) {
		$('.modal-title').text('CHECK IN');
		$('#checkModal .btn-primary').text('CHECK IN');
	}else{
		$('.modal-title').text('CHECK OUT');
		$('#checkModal .btn-primary').text('CHECK OUT');
	}
	$('#branch_check_name').val(branch_name);
	$('.branch_check_name').html('<b>'+branch_name+'</b>');
	$('#branch_check_id').val(branch_id);
	$('#branch_check_user_id').val(user_id);
	$('#branch_check_type').val(type);
	$('#checkModal').modal('show');
}

function check(event){
event.preventDefault();

  //enviar ajax para llenar el select
  if ($('#branch_check_type').val() == 0 ) {

    if ($('#branch_check_supervisor').val() == 1 ) {
      supervisor = 1;
    }else{
      supervisor = null;
    }

    $.ajax({
      type: "POST",
      url: "../../../router/route_histories/create.php",
      data: JSON.stringify({
        user_id: $('#branch_check_user_id').val(),
        branch_id: $('#branch_check_id').val(),
        status: 1,
        started_at: new Date(),
        supervisor: supervisor
      }),
      dataType: "json"
    }).done(function (data) {


      swal({
        title: "Success!",
        text: "CHECK IN",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
        window.location.href = "../../../route_sites/index.php";
      });
    });
  }else{
    $.ajax({
      type: "POST",
      url: "../../../router/route_histories/update.php?id=" + $('#branch_check_id').val(),
      data: JSON.stringify({
        status: 2,
        finished_at: new Date()
      }),
      dataType: "json"
    }).done(function (data) {


      swal({
        title: "Success!",
        text: "CHECK IN",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
        window.location.href = "../../../route_sites/index.php";
      });
    });
  }

}
