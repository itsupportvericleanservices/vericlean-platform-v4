function paginate(event, action) {
    event.preventDefault();
  
    var searchstr = $(".searchstr").val();
  
    var dataView = parseInt($("#page").val())
  
    if (action == 'next') {
      var pageData = dataView + 1
      $.ajax({
        type: "POST",
        url: "../../../router/branches/inactive_sites.php",
        data: JSON.stringify({
          page: pageData,
          searchstr: searchstr
        }),
        dataType: "html"
      }).done(function (data) {
        $("#m-portlet__body_inactive").html(data);
      });
    }
    if (action == 'back') {
      if (dataView > 1) {
        var pageData = dataView - 1
        $.ajax({
          type: "POST",
          url: "../../../router/branches/inactive_sites.php",
          data: JSON.stringify({
            page: pageData,
            searchstr: searchstr
          }),
          dataType: "html"
        }).done(function (data) {
          $("#m-portlet__body_inactive").html(data);
        });
      }
    }
  }
  