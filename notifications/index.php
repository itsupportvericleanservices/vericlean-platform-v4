<?php
include('../includes/header.php');
$notifications = htdevsGet($_SESSION['access-token'], 'notifications');
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Notifications</h3>
          
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      <div class="row">
        <div class="col-xl-12">

          <!--begin::Portlet-->
          <div class="m-portlet" id="m-portlet__body">
            <div class="table-responsive">          
              <table class="table table-hover table-striped">
                <thead>             
                  <tr>
                    <th>#</th>
                    <th>To</th>
                    <th>Message</th>
                    <th>Sent At</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach($notifications as $notification) { ?>
                  <tr>
                    <td><?= $notification['id']; ?></td>
                    <td><?= $notification['user']['first_name']; ?> <?= $notification['user']['last_name']; ?></td>
                    <td><?= $notification['message']; ?></td>
                    <td><?= utc_to_cst($notification['sent_at'], 'm-d-Y H:i'); ?></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
          

            <!--end::Form-->
          </div>
          <div id="cont" class="m-portlet">
          
          </div>
          <!--end::Portlet-->
          <!--End::Section-->
        </div>
      </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>
  <!--End::Content-->
  <!-- end:: Page -->

  
  

<?php include('../includes/footer.php');?>