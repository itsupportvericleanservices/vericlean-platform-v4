<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);
if(!empty($json['searchstr'])) {
  $branchesInactiveWithPage = vcGetBranchesInactiveWithPages($_SESSION['access-token'], $json['page'], $json['searchstr']);
} else {
  $branchesInactiveWithPage = vcGetBranchesInactiveWithPages($_SESSION['access-token'], $json['page']);
}
?>


                        <div class="m-section">
                            <div class="table-responsive">
                            <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="70px">Site Code</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Client</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Zipcode</th>
                                            <th>Frequency</th>
                                            <th>Cleaner</th>
                                            <th>Area Manager</th>
                                            <th>Supervisor</th>
                                            <th>Active</th>
                                            <th>Muted</th>
                                            <th>Min duration on site</th>
                                            <th>Max duration on site</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                           foreach ($branchesInactiveWithPage[0] as $key => $branch) {
                                        ?>
                                        <tr>
                                            <td scope="row">
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['site_code'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if($branch['client']['indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['client']['name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['address'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['city'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['state'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['zipcode'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                <?php if(empty($branch['frequency'])){ ?>
                                                    not assigned
                                                <?php }else{ ?>
                                                    <?php
													$frequencies = explode(",", $branch['frequency']);
														foreach($frequencies as $freq){
															if($freq == 1){
																echo "Monday ";
															}
															if($freq == 2){
																echo "Tuesday ";
															}
															if($freq == 3){
																echo "Wednesday ";
															}
															if($freq == 4){
																echo "Thursday ";
															}
															if($freq == 5){
																echo "Friday ";
															}
															if($freq == 6){
																echo "Saturday ";
															}
															if($freq == 7){
																echo "Sunday ";
															}
														}
													?>
                                                <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                <?php if(empty($branch['cleaner'])){ ?>
                                                    not assigned
                                                <?php }else{ ?>
                                                    <?= $branch['cleaner']['first_name'] ?>
                                                <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if(empty($branch['area_manager'])){ ?>
                                                        not assigned
                                                    <?php }else{ ?>
                                                        <?= $branch['area_manager']['first_name'] ?>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if(empty($branch['supervisor'])){ ?>
                                                        not assigned
                                                    <?php }else{ ?>
                                                        <?= $branch['supervisor']['first_name'] ?>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if($branch['active'] == 1){ ?>
                                                        <span class="m-badge m-badge--success m-badge--wide">Active</span>
                                                    <?php }else{ ?>
                                                        <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if($branch['muted'] == 1){ ?>
                                                        <span class="m-badge m-badge--danger m-badge--wide">Without alerts</span>
                                                    <?php }else{ ?>
                                                        <span class="m-badge m-badge--success m-badge--wide">With alerts</span>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['duration_min'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?= $branch['duration_max'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                        data-toggle="dropdown" aria-expanded="true">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="show.php?id=<?= $branch['id'] ?>"><i class="la la-crosshairs"></i>
                                                            View</a>
                                                        <a class="dropdown-item" href="update.php?id=<?= $branch['id'] ?>"><i class="la la-edit"></i>
                                                            Update</a>
                                                        <a onclick="deleteBranch(event, <?= $branch['id'] ?>)" class="dropdown-item"><i class="la la-trash"></i>
                                                            Delete</a>
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                            <?php
                                  //TOMA EL VALOR DE LA PAGINA QUE ME DEVUELVE LA API, PARA PASARLO AL JS Y PODER
                                  //EJECUTAR EL PAGINADO.
                                  foreach ($branchesInactiveWithPage as $key => $branch) {
                                    if(!empty($branch['page'])){
                                 ?>
                                    <input type="hidden" class="current_page" value="<?= $branch['page'] ?>" id="page">
                                  <?php } ?>
                                <?php } ?>

                                <div style="float:right">
                                    <a href="#" onclick="paginate(event, 'back')" id="back">
                                        <span>
                                            <span>Back</span>
                                        </span>
                                    </a>
                                    <a href="#" onclick="paginate(event, 'next')" id="next">
                                        <span>
                                            <span>Next</span>
                                        </span>
                                    </a>
                                    <a style="margin-left:20px;">Page: <?php echo $branch['page']; ?></a>
                                </div>

<!--begin::BRANCHES -->
<script src="../assets/js/branches/inactive_paginate.js" type="text/javascript"></script>
<!--end::BRANCHES -->
