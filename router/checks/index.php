<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

if(!empty($json['searchstr'])) {
  $checks = vcGetChecks($_SESSION['access-token'], $json['page'], $json['searchstr'], $json['from'], $json['to']);
} else {
  $checks = vcGetChecks($_SESSION['access-token'], $json['page'], NULL, $json['from'], $json['to']);
}
?>
<div class="m-section">
<div class="table-responsive">
<table class="table table-bordered table-hover table table-bordered table-striped">
    <thead>
        <tr>
            <th>Checking type</th>
            <th>Person</th>
            <th width="70px">Site Code</th>
            <th>Name</th>
            <th>Type</th>
            <th>Client</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <!-- <th>Frequency</th> -->
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <!-- <th>Active</th>
            <th>Muted</th>
            <th>Min duration on site</th>
            <th>Max duration on site</th> -->
            <th>Check in</th>
            <th>Check out</th>
            <th class="noPrint">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //var_dump($checks);
         if (count($checks)) {             
         
           foreach ($checks as $key => $check) {            
            //var_dump($check['check']['']);            
        ?>
        <tr>
            <td>
                <?= ($check['check']['supervisor'] == 1) ? 'Supervisor' : 'Cleaner' ?>
                <?= ($check['check']['reassigned'] == 1) ? ' - Reassined' : '' ?>
            </td>
            <td>
                <?= $check['user']['first_name'];?> <?= $check['user']['last_name'];?>
                <?php if ($check['user']['deleted'] == 1) { ?>
                <br>
                <a style="background: #ff0000a8;color: white !important;font-size: 10px;border-radius: 5px;padding: 2px;">Deleted</a>
                <?php } ?>
            </td>
            <td scope="row">
                
                <?= $check['branch']['site_code'] ?>
                
            </td>
            <td>
                
                <?= $check['branch']['name'] ?>
                
            </td>
            <td>
                
                <?php if($check['client']['indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
                
            </td>
            <td>
                
                <?= $check['client']['name'] ?>
                
            </td>
            <td>
                
                <?= $check['branch']['address'] ?>
                
            </td>
            <td>
                
                <?= $check['branch']['city'] ?>
                
            </td>
            <td>
                
                <?= $check['branch']['state'] ?>
                
            </td>
            <td>
                
                <?= $check['branch']['zipcode'] ?>
                
            </td>
           
            <td >
                
                <?php if(empty($check['cleaner'])){ ?>
                    not assigned
                <?php }else{ ?>
                    <?= $check['cleaner']['first_name'] ?>
                <?php } ?>
                
            </td>
            <td>
                
                <?php if(empty($check['area_manager'])){ ?>
                    not assigned
                <?php }else{ ?>
                    <?= $check['area_manager']['first_name'] ?>
                <?php } ?>
                
            </td>
            <td >
                
                <?php if(empty($check['supervisor'])){ ?>
                    not assigned
                <?php }else{ ?>
                    <?= $check['supervisor']['first_name'] ?>
                <?php } ?>
                
            </td>
            <td>
               
                
                <?= ($check['check']['started_at'] != null) ? utc_to_cst($check['check']['started_at']): '' ;?>

            </td>
            <td>
                <?= ($check['check']['finished_at'] != null) ? utc_to_cst($check['check']['finished_at']) :''; ?>
            </td>
            <td  class="noPrint">
                <span class="dropdown">
                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                        data-toggle="dropdown" aria-expanded="true">
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" onclick="showMap('<?= $check['check']['latitude_checkin'] ?>','<?= $check['check']['longitude_checkin'] ?>','<?= $check['check']['latitude_checkout'] ?>','<?= $check['check']['longitude_checkout'] ?>', '<?= $check['branch']['latitude'] ?>', '<?= $check['branch']['longitude'] ?>' )"><i class="la la-crosshairs"></i>
                            view map</a>
                            <?php        
                            if($_SESSION['role_id'] == 1  ){
                            ?>                            
                            <a class="dropdown-item" href="show.php?id=<?= $check['check']['id'] ?>&a=i"><i class="fas fa-share-square"></i>
                            Send Information</a>
                            <?php
                            }                            
                            ?>
                            <?php 
                            if($_SESSION['role_id'] == 1 &&  ( is_null($check['check']['finished_at'] ) || empty($check['check']['finished_at']) )  ){
                                ?>
                                <a class="dropdown-item" onclick="adminCheckOut('<?= $check['check']['latitude_checkout'] ?>','<?= $check['check']['longitude_checkout'] ?>','<?= $check['check']['id'] ?>', '<?= $check['branch']['id'] ?>')"><i class="fas fa-check-circle"></i>
                            SuperAdmin Check-out</a>
                            <?php
                            }                    
                            ?>                        
                    </div>
                </span>
            </td>
        </tr>
        <?php }}else{
        ?>
        <tr>
            <td colspan="16" align="center">
                   Information not found
           </td>
        </tr>
        <?php
        } ?>
    </tbody>
</table>
</div>
</div>

    <input type="hidden" class="current_page" value="<?php if (count($checks) != 0) {
        echo $checks[0]['page'];
    }else{
        echo 1;
    }?>" id="page">
<?php if (count($checks)>0){ ?>
<div style="float:right">
    <a href="#" onclick="paginate_checks(event, 'back')" id="back">
        <span>
            <span>Back</span>
        </span>
    </a>
    <a href="#" onclick="paginate_checks(event, 'next')" id="next">
        <span>
            <span>Next</span>
        </span>
    </a>
    <a style="margin-left:20px;">Page: <?php if (count($checks) != 0) {
        echo $checks[0]['page'];
    }else{
        echo 2;
    } ?></a>
</div>
<?php
	}
?>

<!--begin::BRANCHES -->
<script src="../assets/js/checks/paginate.js" type="text/javascript"></script>
<!--end::BRANCHES -->