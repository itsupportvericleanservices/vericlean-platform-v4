<?php
// Include Functions
include('../../includes/functions.php');

$data =getReportAllCheckins($_SESSION['access-token'],$_GET['by'],$_GET['id'],$_GET['start'],$_GET['finished']);
$txtDate = ( $_GET['start'] === $_GET['finished'] )  ? date('mdY',strtotime($_GET['start'])) : date('mdY',strtotime($_GET['start']))."-".date('mdY',strtotime($_GET['finished'])) ;
$sites = $data['sites'];
$clients = $data['clients'];
$checks = $data['without_checkins']; 
$with_checks = $data['with_checkins']; 

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0&#8243;");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0&#8243;");
header("content-disposition: attachment;filename=report-with-checkins-".$txtDate.".xls");
?>

<div class="m-section" id="app" >
    <div class="table-responsive" >
         <!-- <h5><small>Date:</small> <span style="background:green;color:white;padding:5px;border-radius:5px;"><?= $key?></span> </h5> -->
        <table  class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
          <thead>
            <tr>
              <th>ID</th>
              <th width="70px">Site Code</th>
              <th>Name</th>
              <th>Type</th>
              <th>Client</th>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Zipcode</th>
              <th>Frequency</th>
              <th>Cleaner</th>
              <th>Area Manager</th>
              <th>Supervisor</th>
              <th>Maximum arrival time</th>
              <th>Check-in</th>
              <th>Check-out</th>
              <th>Time on Site</th>
            </tr>
          </thead>
          <?php foreach ($with_checks as $key => $value) {             
            ?>
          <tbody>
            <tr>
              <td colspan="21">
                <p><small>Date:</small> <span style="background: #34bfa3;color: white;padding: 3px;border-radius: 2px;"><?= $key?></span> <?php if (count($value) == 0) {?><span style="background: #f4516c;color: white;padding: 3px;border-radius: 2px;">there are no records</span><?php } ?> </p>
            
              </td>    
            </tr>
           
            <?php foreach ($value as $branch){?>
                  
            <tr>
              <td scope="row">
                <?= $branch['id'] ?>
              </td>
              <td scope="row">
                <p >
                  <?= $branch['site_code'] ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php echo $branch['name'] ?>
                </p>
                
              </td>
              <td>
                <?php if($branch['client_indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
              </td>
              <td>
                <?= $branch['client_name'] ?>
              </td>
              <td>
                <p >
                  <?php echo $branch['address'] ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php echo $branch['city'] ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php echo $branch['state'] ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php echo $branch['zipcode'] ?>
                </p>
                
              </td>
              <td>
                <p>
                  <?php if(empty($branch['frequency'])){ ?>
                  not assigned
                  <?php }else{ ?>
                  <?php
                $frequencies = explode(",", $branch['frequency']);
                  foreach($frequencies as $freq){
                    if($freq == 1){
                      echo "Monday ";
                    }
                    if($freq == 2){
                      echo "Tuesday ";
                    }
                    if($freq == 3){
                      echo "Wednesday ";
                    }
                    if($freq == 4){
                      echo "Thursday ";
                    }
                    if($freq == 5){
                      echo "Friday ";
                    }
                    if($freq == 6){
                      echo "Saturday ";
                    }
                    if($freq == 7){
                      echo "Sunday ";
                    }
                  }
                ?>
                  <?php } ?>
                </p>
              </td>
              <td>
                <p >
                  <?php if(empty($branch['cl_name'])){ ?>
                  not assigned
                  <?php }else{ ?>
                  <?= $branch['cl_name'] ?>
                  <?php } ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php if(empty($branch['am_name'])){ ?>
                  not assigned
                  <?php }else{ ?>
                  <?= $branch['am_name']  ?>
                  <?php } ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php if(empty($branch['sup_name'])){ ?>
                  not assigned
                  <?php }else{ ?>
                  <?= $branch['sup_name']  ?>
                  <?php } ?>
                </p>
                
              </td>
              <!-- <td>
                <a href="show.php?id=<?= $branch['id'] ?>">
                  <?php if($branch['active'] == 1){ ?>
                  <span class="m-badge m-badge--success m-badge--wide">Active</span>
                  <?php }else{ ?>
                  <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                  <?php } ?>
                </a>
              </td> -->
              <td>
                <p >
                  <?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>
                </p>
                
              </td>

              <td>
                <p >
                  <?php $CheckIn = (empty( $branch['checkins']['started_at'])) ? "" : $branch['checkins']['started_at'];
                    $newDateIn = date("m-d-Y, H:i:s", strtotime($CheckIn. "UTC")); echo $newDateIn;
                    
                    ?>
                </p>
                
              </td>
              <td>
                <p >
                  <?php $CheckOut = (empty($branch['checkins']['finished_at'])) ?  "" : $branch['checkins']['finished_at']; 
                    $newDateOut = date("m-d-Y, H:i:s", strtotime($CheckOut. "UTC")); echo $newDateOut. " "; ?>
                </p>                
              </td>
              <td>
                <p >
                  <?php 
                  if($CheckOut!==""){
                    $date1 = new DateTime($CheckIn);
                    $date2 = new DateTime($CheckOut);
                    $diff = $date1->diff($date2);
                    echo $diff->format('%H:%i:%s'); 
                  } ?>
                </p>                
              </td>              
            </tr>
            <?php } ?>
          </tbody>
          <?php } ?>
        </table>          
      
      </div> 
  </div>

</div>
   

<!-- <script src="../assets/js/jquery.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script> -->

<!-- <script>

  /* Defaults */
  var exportTable = document.getElementsByTagName("table")
  var export_tables = new TableExport(exportTable, {
      formats: ['xlsx'],
      bootstrap: true,
      exportButtons: false,
      // ignoreRows: null,             
      // ignoreCols: 10,

  }); 
  var tables_data = export_tables.getExportData()
  var export_data = []
  var xlsx_info = {}
  for (table_id in tables_data){
      xlsx_info = tables_data[table_id]["xlsx"]
      export_data.push(tables_data[table_id]["xlsx"].data)
  }
  var fileExtension = xlsx_info.fileExtension
  var mimeType = xlsx_info.mimeType
  $(document).ready(function(){
      export_tables.exportmultisheet(export_data, mimeType, "Reporte", [],
                                    fileExtension, {}, [])
  })

</script> -->