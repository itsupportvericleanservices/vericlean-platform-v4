<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$workorders = vcGetWorkorders($_SESSION['access-token']);
if(!empty($json['searchstr'])) {
    $workorders = vcGetWorkordersWithPages($_SESSION['access-token'], $json['page'], $json['searchstr']);
  } else {
      if ($_SESSION['role_id'] == 10) {
        $workorders  = vcGetWOForUser($_SESSION['access-token'], $_SESSION['id']);
      } else {
        
        $workorders = vcGetWorkordersWithPages($_SESSION['access-token'], $json['page']);
      }
    
  }

  //today es para comparar el schedule date vs la fecha de hoy y definir si existe retraso. (color naranja)
  $today = new DateTime();
function readMoreHelper($story_desc, $chars = 100) {
    if (strlen($story_desc) > 100) {
        $story_desc_full = $story_desc;
        $story_desc = substr($story_desc,0,$chars);  
        $story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
        $story_desc = $story_desc." <a href='#' data-toggle='tooltip' data-placement='bottom' title='".$story_desc_full."'>...<strong>Read More</strong></a>";  
    }
    
    return $story_desc;  
} 
?>

<script>
  // delayed va permitir almacenar en localstorage todos los internal_id de los retrasos para luego pasarlo al filtro.
  var delayed = []
</script>

<div class="m-section">
  <div class="table-responsive">
    <table class="table table-bordered table-hover table table-bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Customer Work Order#</th>
          <th>User</th>
          <th>Site</th>
          <th>Client</th>
          <th>Cleaner</th>
          <th>Request date</th>
          <th>Due date</th>
          <th>Site contact</th>
          <th>Site phone</th>
          <th>Task Name</th>
          <th>Task Description</th>
          <th>Status</th>
          <th>Status email</th>
          <th>Type</th>
          <th>Priority</th>
          <th>Fee</th>
          <th>Schedule At</th>
          <th>Started At</th>
          <th>Finished At</th>
          <th>Started Lat</th>
          <th>Started Long</th>
          <th>Finished Lat</th>
          <th>Finished Long</th>
          <th>Created At</th>
          <th>Updated At</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php
                                        if ($_SESSION['role_id'] == 10) {
                                            $orders = $workorders;
                                        } else {
                                            $orders = $workorders[0];
                                        }
                                        
                                        foreach ($orders as $key => $work) {
                                    ?>
        <tr>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['id'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['internal_id'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['user_id'] ?>
            </a>
          </td>
          <td style="width: 300px;display: -webkit-box;">
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['branch']['address'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['client']['name'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?php if(empty($work['cleaner'])) { ?>
              Unnasigned
              <?php } else { ?>
              <?= $work['cleaner']['first_name'] ?> <?= $work['cleaner']['last_name'] ?>
              <?php } ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['requested_date'] != null) ? date('Y-m-d H:i:s', strtotime($work['requested_date'])) :''?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['due_date'] != null) ? date('Y-m-d H:i:s', strtotime($work['due_date'])) :''?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['site_contact'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['site_phone'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['task_name'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= readMoreHelper($work['task_description']); ?>

            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?php if($work['status'] == 0){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Opened</span>
              <?php } ?>
              <?php if($work['status'] == 1){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Quoted</span>
              <?php } ?>
              <?php if($work['status'] == 2){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Approved</span>
              <?php } ?>
              <?php if($work['status'] == 3 &&  $work['scheduled_date'] < $today->format('Y-m-d\TH:i:s.u')){ ?>
              <span class="m-badge m-badge--warning m-badge--wide">Scheduled</span>
              <script>
                delayed.push(<?= $work['internal_id'] ?>)
                localStorage.setItem('delayed', JSON.stringify(delayed))
              </script>
              <?php } ?>
              <?php if($work['status'] == 3 &&  $work['scheduled_date'] > $today->format('Y-m-d\TH:i:s.u')){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Scheduled</span>
              <?php } ?>

              <?php if($work['status'] == 4){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Assigned</span>
              <?php } ?>
              <?php if($work['status'] == 5){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Dispatched</span>
              <?php } ?>
              <?php if($work['status'] == 6){ ?>
              <span class="m-badge m-badge--info m-badge--wide">In Progress</span>
              <?php } ?>
              <?php if($work['status'] == 7){ ?>
              <span class="m-badge m-badge--success m-badge--wide">Completed</span>
              <?php } ?>
              <?php if($work['status'] == 8){ ?>
              <span class="m-badge m-badge--danger m-badge--wide">Refused</span>
              <?php } ?>
              <?php if($work['status'] == 9){ ?>
              <span class="m-badge m-badge--danger m-badge--wide">Rejected</span>
              <?php } ?>
              <?php if($work['status'] == 10){ ?>
              <span class="m-badge m-badge--danger m-badge--wide" style="background: #9d6363 !important">Invoiced</span>
              <?php } ?>
              <?php if($work['status'] == 11){ ?>
              <span class="m-badge m-badge--danger m-badge--wide" style="background: #c14a26 !important">Payed</span>
              <?php } ?>
              <?php if($work['status'] == 12){ ?>
              <span class="m-badge m-badge--info m-badge--wide" style="background: #c14a26 !important">Ready to verify</span>
              <?php } ?>
            </a>
          </td>
          <td>
            <?= $work['status_alt'] ?>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['main_type'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?php if($work['priority'] == 1){ ?>
              P1
              <?php } ?>
              <?php if($work['priority'] == 2){ ?>
              P2
              <?php } ?>
              <?php if($work['priority'] == 3){ ?>
              P3
              <?php } ?>
              <?php if($work['priority'] == 4){ ?>
              P4
              <?php } ?>
              <?php if($work['priority'] == 5){ ?>
              P5
              <?php } ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['fee'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['scheduled_date'] != null) ? date('Y-m-d H:i:s', strtotime($work['scheduled_date'])) :''?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['started_date'] != null) ? date('Y-m-d H:i:s', strtotime($work['started_date']) + 60*60*12 ) :'' ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['finished_date'] != null) ? date('Y-m-d H:i:s', strtotime($work['finished_date']) + 60*60*12 ) :''?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['started_lat'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['started_lng'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['finished_lat'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= $work['finished_lng'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['created_at'] != null) ? date('Y-m-d H:i:s', strtotime($work['created_at'])) :''?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $work['id'] ?>">
              <?= ($work['updated_at'] != null) ? date('Y-m-d H:i:s', strtotime($work['updated_at'])) :''?>
            </a>
          </td>
          <td>
            <span class="dropdown">
              <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                data-toggle="dropdown" aria-expanded="true">
                <i class="la la-ellipsis-h"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="show.php?id=<?= $work['id'] ?>"><i class="la la-crosshairs"></i>
                  View</a>
                <a class="dropdown-item" href="update.php?id=<?= $work['id'] ?>"><i class="la la-edit"></i>
                  Update</a>
                <a onclick="deleteWorkorder(event, <?= $work['id'] ?>)" class="dropdown-item"><i
                    class="la la-trash"></i>
                  Delete</a>
                <?php 
                  if ($work['status'] >= 7 && $work['status'] <= 9) {
                    
                ?>
                <a onclick="sendWoEmail(event, <?= $work['id'] ?>,false)" class="dropdown-item"><i
                    class="la la-send"></i>
                  Send Email</a>

                <?php } ?>
                <?php 
                  if ($work['status'] >= 10 && $work['status'] <= 11) {
                    
                ?>
                <a onclick="sendWoEmail(event, <?= $work['id'] ?>,true)" class="dropdown-item"><i
                    class="la la-send"></i>
                  Send Invoice</a>

                <?php } ?>
              </div>
            </span>
          </td>
        </tr>
        <?php }  ?>

      </tbody>
    </table>
  </div>
</div>

<?php if ($_SESSION['role_id'] != 10) { ?>
<input type="hidden" class="current_page" value="<?= $workorders[1]['page'];?>" id="page">

<div style="float:right">
  <a href="#" onclick="paginate(event, 'back')" id="back">
    <span>
      <span>Back</span>
    </span>
  </a>
  <a href="#" onclick="paginate(event, 'next')" id="next">
    <span>
      <span>Next</span>
    </span>
  </a>
  <a style="margin-left:20px;">Page: <?php echo $workorders[1]['page']; ?></a>
</div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="sendWoEmailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>To:</label>
        <input type="text" name="form-control m-input" id="email_to">
        <input type="hidden" name="form-control m-input" id="internal_id">
        <input type="hidden" name="form-control m-input" id="wo_id">
        <input type="hidden" name="form-control m-input" id="client_name">
        <input type="hidden" name="form-control m-input" id="client_address">
        <input type="hidden" name="form-control m-input" id="request_contact">
        <hr>
        <label>Subject:</label>
        <input type="text" name="form-control m-input" id="email_subject">
        <hr>
        <div id="summernote"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="sendEmail();">Send</button>
      </div>
    </div>
  </div>
</div>
<!--begin::WORKORDERS -->
<script src="../assets/js/workorders/paginate.js" type="text/javascript"></script>

<!--Se llama al index para poder pasarle el scheduled_date y poder filtrar por retraso -->
<script src="../assets/js/workorders/index.js" type="text/javascript"></script><!-- include summernote css/js -->

<link href="../assets/stylesheet/plugins/summernote/summernote.css" rel="stylesheet">
<script src="../assets/js/summernote.min.js"  type="text/javascript"></script>

<!--end::WORKORDERS -->