<?php
// Include Functions
include('../../includes/functions.php');
$filters = [
    ["name" => "today","show" => "Today"],
    ["name" => "week","show" => "Week"],
    ["name" => "month","show" => "Month"],
    ["name" => "year","show" => "Year"]
    ];
// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);


$filtro = vcGetSitesWhitFilter($_SESSION['access-token'], $json['id'] , $json['filter_by']);
    
$histories = $filtro['histories'];

$route = vcGetRouteSchedule($_SESSION['access-token'], $json['id']);

$branches = vcGetBranches($_SESSION['access-token']);
$clients = vcGetClients($_SESSION['access-token']);
$users = vcGetUsers($_SESSION['access-token']);

// $filter = 
// $routes_today = getSitesWhitFrequency($_SESSION['access-token'], $_GET['id'],$filter)

?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">
            Routes history
            <a class="btn btn-primary" onclick="javascript:window.print();">Imprimir</a>
          </h3>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <select class="form-control m-b" name="account" id="filter" onChange="loadFilter(<?php echo $json['id']?>)">
          <option value="today">Selected One</option>
          <?php foreach ($filters as $filter) { ?>
          <option value="<?= $filter['name'] ?>" <?php if ($filter['name']==$json['filter_by']) { echo "selected" ; };
            ?>>
            <?= $filter['show'] ?>
          </option>
          <?php } ?>
        </select>
      </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    All Report filter by <?= $json['filter_by']?>
                                </h3>
                            </div>
                        </div>
                        
                    </div>

                    <div class="m-portlet__body">

                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Site Code</th>
                                            <th>Name</th>
                                            <th>Assigned Employee</th>
                                            <th>Started At</th>
                                            <th>Finished At</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($histories as  $history) {
                                        ?>
                                        <tr>                                           
                                            <td scope="row">                                            
                                                TSP9
                                            </td>
                                            <td>
                                                <?php
                                                $branch_name;
                                                foreach ($branches as $branch) {
                                                  if ($branch['id'] == $history['branch_id'] ) {
                                                    $branch_name = $branch['name'];
                                                  }
                                                }?>
                                                    <?= $branch_name ?>
                                            </td>
                                            
                                            <td>
                                                <?php
                                                $user_name;
                                                foreach ($users as $user) {
                                                  if ($user['id'] == $history['user_id'] ) {
                                                    $user_name = $user['first_name'];
                                                  }
                                                }?>
                                                    <?= $user_name ?>
                                            </td>
                                            <td>
                                              <p> <?= $history['started_at']  ?> </p>
                                            </td>
                                              
                                            <td>
                                              <p> <?= $history['finished_at']  ?> </p>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>