<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);
// $sites = vcGetBranches($_SESSION['access-token']);
// $clients = vcGetClients($_SESSION['access-token']);
$data = getChecksClnr($_SESSION['access-token'],$json['id'],$json['branch'],$json['by'],$json['start'],$json['finished']);
$clnr = $data['cleaners'];
$checks = $data['checkins']; 
$supervisors = $data['supervisors']; 
// $completes = $data['completes'];
// $incompletes = $data['incompletes'];
$all = $data['all'];
$vals = [];
$vals_h = [];
$vals_m = [];
foreach ($checks as $check) {
  $fecha1 = new DateTime($check['route_histories_started']);
  $fecha2 = new DateTime($check['route_histories_finished']);

  $intervalo = $fecha1->diff($fecha2);

  $all_time = $intervalo->format('%H:%i');

  $h = substr($all_time,0,2);

  array_push($vals_h,(int)$h);

  $m = substr($all_time,3,2);

  array_push($vals_m,(int)$m);

  array_push($vals,new DateTime($all_time));
}
$total_h = 0;
$total_m = 0;

foreach ($vals_h as $value) {
 $total_h += $value;
}

foreach ($vals_m as $value) {
  $total_m += $value;
 }

$total_m_h = $total_m/60; 

// $hours_sum = (int)substr($total_m_h,0,2);
// $min_rest = $total_m - $hours_sum * 60 

$total_horas = $total_h + $total_m_h;
if (count($checks) > 0) {
  $count = $total_horas/count($checks);
}else {
  $count = 0;
}

// if ($all > 0) {
// $porsentaje =  $completes/$all*100;
// $comp = round($porsentaje);

// $porsentaje_in =  $incompletes/$all*100;
// $incomp = round($porsentaje_in);
// }else{
//   $comp = 0;
//   $incomp = 0;
// }
?>
    <div class="m-grid__item m-grid__item--fluid m-wrapper cards-info" style="    margin-bottom: 00px;">
      <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
          <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-sm-12" style="height: 130px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_first_left">
                    Total Time
                    </h5><br>
                    <h5 class="title_card_first_right">
                    Per Site
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                     Hours
                    </span>
                    <span class="m-widget24__desc" style="position:absolute; right:5%; top: 35px;">
                     Hours
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:18px; top:65px; font-size: 25px;">
                    <?= number_format($total_horas,2) ?>
                    </span>
                    <span class="m-widget24__stats m--font-info number_card_right" style="position:absolute; right:15px; top:65px; font-size: 25px;">
                    <?= number_format($count,2) ?> 
                    </span>
                  </div>
                  <!-- <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $comp.'%' ?>;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                  percentage
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                  <?php echo $comp ?>%
                  </span> -->
                </div>
              </div>
            </div>

            <!-- <div class="col-sm-6" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_second_left">
                      Checkins
                    </h5><br>
                    <h5 class="title_card_second_right">
                      Checkins
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc description_card_second description_card_second_left" style="position:absolute; left:0%; top:35px">
                      Total
                    </span>
                    <span class="m-widget24__desc description_card_second" style="position:absolute; right:2%; top: 35px;">
                      Incompletes
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      <?= $all ?>
                    </span>
                   
                    <span class="m-widget24__stats m--font-warning number_card_second_right" style="position:absolute; right:-3%; top:60px; font-size: 40px;">
                      <?= $incompletes?>
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-warning" role="progressbar" style="width: <?php echo $incomp.'%' ?>;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                  percentage
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                    <?php echo $incomp ?>%
                  </span>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
<!-- <pre><?php  print_r($count)?></pre> -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
      <div class="modal-dialog" role="document" style="max-width:650px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="margin-right: 10px;margin-top: 10px;">Filter by </h5>
             
            <select class="form-control select_ls" name="select_ls" id="select_ls" style="width:100px;" onchange="showList()">
              <option value="cleaner">Cleaner</option>
              <option value="supervisor">Supervisor</option>
            </select>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          
            <div class="row " style="float:left;width: 100%;">
              <div calss="col-md-6">
              <label>Range</label>
              <input type="text" name="range" id="from_date_filter"  class="form-control"  value="" style="margin-left: 5px;margin-right: 8px;width: 180px;">
              </div>
              <div class="col-md-3 sec_cle">
                <label> Select Cleaner</label>
                <select  name="cleaner_id" class="cleaner_id form-control" onchange="showSites('modal')">
                  <option value="0" selected>Show all</option>
         
                  <?php 
                usort($clnr, function($a, $b) {
                  return strtolower($a[1]) > strtolower($b[1]);
                  
                  });
                foreach ($clnr as $key => $clr) { ?>
                    <option value="<?= $clr[0] ?>" <?php if($json['id'] == $clr[0]){echo 'selected';}?>><?= $clr[1]." ".$clr[2] ?></option>
              
                <?php  } ?>
                </select>
              </div>

              <div class="col-md-3 sec_sup">
                <label> Select Supervisor</label>
                <select  name="sup_id" class="sup_id form-control" onchange="showSites('modal')">
                  <option value="0" selected>Show all</option>
  
                  <?php 
                usort($supervisors, function($a, $b) {
                  return strtolower($a[1]) > strtolower($b[1]);
                  
                  });
                foreach ($supervisors as $key => $sup) { ?>
                    <option value="<?= $sup[0] ?>" <?php if($json['id'] == $sup[0]){echo 'selected';}?>><?= $sup[1]." ".$sup[2] ?></option>
              
                <?php  } ?>
                </select>
              </div>
              <div class="col-md-3 " style="">
                <label> Select Site</label>
                <select  name="site_id" class="site_id form-control">

                  <option value="-1" >View All</option>
                  
                </select>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button"  class="boton_filtro btn btn-primary" data-dismiss="modal">Search Now</button>
          </div>
        </div>
      </div>
    </div>

<!-- -------------- -->
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">

        <div class="m-portlet__head-text">
          <div class="">
            <label style="font-size: 12px;font-weight: normal;" class="">Filter by:</label>
            <select name="" class="select_ls_form form-control" id="" onchange="showList()"> 
              <option value="cleaner"<?php if($json['by'] == 'cleaner'){echo 'selected';}?>>Cleaner</option>
              <option value="supervisor"<?php if($json['by'] == 'supervisor'){echo 'selected';}?>>Supervisor</option>
            </select>
          </div>
          <div>
            <label style="font-size: 12px;font-weight: normal;">Range</label>
            <input type="text" name="range" id="from_date_filter_form" class="form-control" value="<?= $json['full']?>"
              style="margin-left: 5px;margin-right: 8px;width: 180px;">
          </div>

          <div class="select_cleaner_form">
            <label style="font-size: 12px;font-weight: normal;"> Select Cleaner</label>
            <select name="cleaner_id_form" class="cleaner_id_form form-control" onchange="showSites('home')"
              style="width: 150px;margin-left: 6px;margin-right: 15px;">
              <option value="0" selected>Show all</option>
              <!-- <option value="-1" >View All</option> -->
              <?php 
              usort($clnr, function($a, $b) {
                return strtolower($a[1]) > strtolower($b[1]);
                
                });
              foreach ($clnr as $key => $sup) { ?>
              <option value="<?= $sup[0] ?>" <?php if($json['id'] == $sup[0]){echo 'selected';}?>>
                <?= $sup[1]." ".$sup[2] ?></option>

              <?php  } ?>
            </select>
          </div>
          <div class="select_supervisor_form" style="display:none;">
            <label style="font-size: 12px;font-weight: normal;"> Select Supervisor</label>
            <select name="sup_id" class="sup_id_form form-control" onchange="showSites('home')" style="width: 150px;margin-left: 6px;margin-right: 15px;">
              <option value="0" selected>Show all</option>

              <?php 
                usort($supervisors, function($a, $b) {
                  return strtolower($a[1]) > strtolower($b[1]);
                  
                  });
                foreach ($supervisors as $key => $sup) { ?>
              <option value="<?= $sup[0] ?>" <?php if($json['id'] == $sup[0]){echo 'selected';}?>>
                <?= $sup[1]." ".$sup[2] ?></option>

              <?php  } ?>
            </select>
          </div>
          
            

            <div class="filter_by_sites" style="float:left;width: 120px;">
              <label style="font-size: 12px;font-weight: normal;">Select Branch</label>
              <select name="site_id" class="site_id_form form-control">

                <option value="-1">Show all</option>

              </select>
            </div>



              <div class="filter" style="float:left; margin-left: 10px;">
                <button onclick="getClrChecks('form')" class="btn btn-info">Filter</button>
              </div>
              <div class="clear" style="float:left; margin-left: 10px;">
                <a href="index.php" class="btn btn-danger">Clear</a>
              </div>
            
          
        </div>

      </div>

    </div>
    <div class="m-portlet__head-tools">
      <ul class="m-portlet__nav">
        <li class="m-portlet__nav-item">
          <a class="btn btn-secondary" href='../../router/dur_checks/file.php?id=<?= $json['id'] ?>&start=<?= $json['start'] ?>&finished=<?= $json['finished'] ?>&branch=<?= $json['branch'] ?>&by=<?= $json['by'] ?>' >Export</a>
        
        </li>
      </ul>
    </div>
  </div> 

<div class="m-portlet__body">
  <!--begin::Section-->
  <!--end::Section-->

<!-- <pre><?php print_r($total_h)?></pre> -->
<!-- <pre><?php print_r($total_m)?></pre> -->
<!-- <pre><?php print_r($total_m_h)?></pre> -->
<!-- <pre><?php print_r($hours_sum)?></pre> -->

<!-- <pre><?php print_r($total_horas)?></pre> -->
<?php if ($checks != null) {?>
<div class="m-section" id="app" >
    <div class="table-responsive" style="<?php  if(count($branchesWithPage[0]) < 3){ echo "height: 260px;"; } ?>">
      
    

         <!-- <h5><small>Date:</small> <span style="background:green;color:white;padding:5px;border-radius:5px;"><?= $key?></span> </h5> -->
        <table  class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr >
            <th>ID</th>
            <th>Site Code</th>
            <th>Name</th>
            <th width="35px">Duration on site</th>
            <th width="35px">Started At</th>
            <th>Finished At</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Client</th>
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($checks as $key => $branch) { ?>
          <?php  
            $fecha1 = new DateTime($branch['route_histories_started']);//fecha inicial
            $fecha2 = new DateTime($branch['route_histories_finished']);//fecha de cierre

            $intervalo = $fecha1->diff($fecha2);

            
          ?>
          <tr <?php if($branch['route_histories_status'] == 1){echo 'style="background: #eee;"';}?>>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p >
                <?= $branch['site_code'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['name'] ?>
              </p>
              
            </td>
            <td>
            <p>
                
              <span style="width:75px;background-color: salmon;" class="m-badge m-badge--success m-badge--wide"><?= $intervalo->format(' %H : %i'); ?></span>
                
              </p>
              
            </td>
            <td>
              <p >
                
                <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['route_histories_finished'])){ ?>
                  No Info
                <?php }else{ ?>
                  <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <?php } ?>
              </p>
            </td>
            <td>
              <?= $branch['address'] ?>
            </td>
            <td>
              <?= $branch['city'] ?>
            </td>
            <td>
              <?= $branch['state'] ?>
            </td>
            <td>
              <?= $branch['client_name'] ?>
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo  $date =  date('H:i', strtotime($branch['max_time'])  - 21600); ?>
              </p>
              
            </td>
            
          </tr>
          <?php } ?>
        </tbody>
      </table>
     
    </div>
  </div>

</div>
<?php } ?>
<!--begin::BRANCHES -->

<script src="../assets/js/vue.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script>
<script src="../assets/js/dur_checks/index.js"></script>
<script>
  $('input[name="range"]').daterangepicker({
        locale: {
            format: 'MM-DD-YYYY'
        }
    });

    var all = <?php echo json_encode($all) ?>;

    function showSites(where){
      
      console.log(ls)
      $('.site_id').html('<option value="-1" selected>Show all</option>')
      $('.site_id_form').html('<option value="-1" selected>Show all</option>')
      var id = 0
      
      if (where == 'modal') {
        var ls = $('.select_ls').val()
        if (ls  == 'cleaner') {
        id = $('.cleaner_id').val()
        }else if(ls == 'supervisor'){
          id = $('.sup_id').val()
        }
      }else if(where == 'home'){
        var by = $('.select_ls_form').val()
        if (by == 'cleaner') {
          id = $('.cleaner_id_form').val()
        } else if (by == 'supervisor') {
          id = $('.sup_id_form').val()
        }
      }
      // if (id != 0) {
      //   $('.boton_filtro').show()
      // }else{
      //   $('.boton_filtro').hide()
      // }
      console.log(id)
      
      var options = all.filter(el => {
        return el[2] == Number(id) || el[3] == Number(id)
      })

      

      options.forEach(element => {
        var select= ''
        if (element.id == Number(id)) {
          select = 'selected'
        }
      $('.site_id').append(`<option value="${element[0]}" ${select}>${element[1] == '' ? `no name, id:${element[0]}` : element[1]}</option>`);
      $('.site_id_form').append(`<option value="${element[0]}" ${select}>${element[1] == '' ? `no name, id:${element[0]}` : element[1]}</option>`);
    });
    }

    
</script>

<script>

// $( document ).ready(function(){
    
//   /* Defaults */
//   var exportTable = $("table")
//   var export_tables = new TableExport(exportTable, {
//       formats: ['xlsx'],
//       bootstrap: true,
//       exportButtons: false,
//       // ignoreRows: null,             
//       // ignoreCols: 10,

//   }); 
//   var tables_data = export_tables.getExportData()
//   var export_data = []
//   var xlsx_info = {}
//   for (table_id in tables_data){
//       xlsx_info = tables_data[table_id]["xlsx"]
//       export_data.push(tables_data[table_id]["xlsx"].data)
//   }
//   var fileExtension = xlsx_info.fileExtension
//   var mimeType = xlsx_info.mimeType
//   $("#export_btn").click(function(){
//     console.log('object');
//     export_tables.exportmultisheet(export_data, mimeType, "Reporte - "+$('.title_report').text(), [],fileExtension, {}, [])
//   })
// })
</script>