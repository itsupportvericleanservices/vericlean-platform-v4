<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);
// $sites = vcGetBranches($_SESSION['access-token']);
// $clients = vcGetClients($_SESSION['access-token']);
$data = getChecksSup($_SESSION['access-token'],$json['id'],$json['start'],$json['finished']);
$sups = $data['supervisors'];
$checks = $data['checkins']; 
$completes = $data['completes'];
$incompletes = $data['incompletes'];
$all = $data['all'];
if ($all > 0) {
$porsentaje =  $completes/$all*100;
$comp = round($porsentaje);

$porsentaje_in =  $incompletes/$all*100;
$incomp = round($porsentaje_in);
}else{
  $comp = 0;
  $incomp = 0;
}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper cards-info">
      <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
          <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-sm-6" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_first_left">
                      Checkins
                    </h5><br>
                    <h5 class="title_card_first_right">
                    Checkins
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                      Total
                    </span>
                    <span class="m-widget24__desc" style="position:absolute; right:2%; top: 35px;">
                      Completes
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      <?= $data['all']?>
                    </span>
                    <span class="m-widget24__stats m--font-info number_card_right" style="position:absolute; right:0%; top:60px; font-size: 40px;">
                    <?= $data['completes']?>
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $comp.'%' ?>;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                  percentage
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                  <?php echo $comp ?>%
                  </span>
                </div>
              </div>
            </div>

            <div class="col-sm-6" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_second_left">
                      Checkins
                    </h5><br>
                    <h5 class="title_card_second_right">
                      Checkins
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc description_card_second description_card_second_left" style="position:absolute; left:0%; top:35px">
                      Total
                    </span>
                    <span class="m-widget24__desc description_card_second" style="position:absolute; right:2%; top: 35px;">
                      Incompletes
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      <?= $all ?>
                    </span>
                   
                    <span class="m-widget24__stats m--font-warning number_card_second_right" style="position:absolute; right:-3%; top:60px; font-size: 40px;">
                      <?= $incompletes?>
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-warning" role="progressbar" style="width: <?php echo $incomp.'%' ?>;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                  percentage
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                    <?php echo $incomp ?>%
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- <pre><?php  print_r($checks[0])?></pre> -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Filter by supervisor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           
            <div class="row" style=";float:left;width: 100%;">
            <div class="col-md-7 " style="padding-top: 5px;">
              <label>Range</label>
              <input type="text" name="range" id="from_date_filter" class="form-control" value="" style="margin-left: 5px;margin-right: 8px;">
            </div>
            <div class="col-md-5">
            <label> Select Supervisor</label>
              <select  name="client_id" class="sup_id form-control">
                <!-- <option value="0" selected>Select One</option> -->
                <option value="-1" >View All</option>
                <?php 
              usort($sups, function($a, $b) {
                return strtolower($a[1]) > strtolower($b[1]);
                
                });
              foreach ($sups as $key => $sup) { ?>
                  <option value="<?= $sup[0] ?>" <?php if($json['id'] == $sup[0]){echo 'selected';}?>><?= $sup[1]." ".$sup[2] ?></option>
            
              <?php  } ?>
              </select>
            </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Search Now</button>
          </div>
        </div>
      </div>
    </div>

<!-- -------------- -->
<div class="m-portlet__head">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">

      <h3 class=" m-portlet__head-text">
      <label>Range</label>
        <input type="text" name="range" id="from_date_filter_form"  class="form-control"  value="" style="margin-left: 5px;margin-right: 8px;width: 180px;">

        Select supervior
        <div class="">

          <div class="filter_by_sites" style=";float:left;width: 120px;">
            <select  name="client_id" class="sup_id_form form-control" onchange="showButton()">
              <option value="0" selected>Select One</option>
              <option value="-1" <?php if($json['id'] == -1){echo 'selected';}?>>View All</option>
              <?php 
              usort($sups, function($a, $b) {
                return strtolower($a[1]) > strtolower($b[1]);
                
                });
              foreach ($sups as $key => $sup) { ?>
                  <option value="<?= $sup[0] ?>" <?php if($json['id'] == $sup[0]){echo 'selected';}?>><?= $sup[1]." ".$sup[2] ?></option>
            
              <?php  } ?>
            </select>
          </div>
          

          <div class="filter" style="float:left; margin-left: 10px;">
            <button onclick="getSupChecks('form')" class="btn btn-info">Filter</button>
          </div>
          <div class="clear" style="float:left; margin-left: 10px;">
            <a href="index.php"class="btn btn-danger">Clear</a>
          </div>
        </div>
      </h3>
    </div>

  </div>
<div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary" href='../../router/sup_checks/file.php?id=<?= $json['id'] ?>&start=<?= $json['start'] ?>&finished=<?= $json['finished'] ?>' >Export</a>
        
      </li>
    </ul>
  </div>
</div>

<div class="m-portlet__body">
  <!--begin::Section-->
  <!--end::Section-->
<!-- <pre><?php print_r($sites)?></pre> -->
<?php if ($checks != null) {?>
<div class="m-section" id="app" >
    <div class="table-responsive" >
      
    

         <!-- <h5><small>Date:</small> <span style="background:green;color:white;padding:5px;border-radius:5px;"><?= $key?></span> </h5> -->
        <table  class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr >
            <th>ID</th>
            <th>Site Code</th>
            <th>Name</th>
            <th width="35px">Status</th>
            <th width="35px">Started At</th>
            <th>Finished At</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Client</th>
            <th>Checkin Person</th>
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($checks as $key => $branch) { ?>
          <tr <?php if($branch['route_histories_status'] == 1){echo 'style="background: #eee;"';}?>>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p >
                <?= $branch['site_code'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['name'] ?>
              </p>
              
            </td>
            <td>
            <p>
                <?php if(!empty($branch['route_histories_finished'])){ ?>
                <span class="m-badge m-badge--success m-badge--wide">Complete</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Inclomplete</span>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                
                <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['route_histories_finished'])){ ?>
                  No Info
                <?php }else{ ?>
                <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <?php } ?>
              </p>
            </td>
            <td>
              <?= $branch['address'] ?>
            </td>
            <td>
              <?= $branch['city'] ?>
            </td>
            <td>
              <?= $branch['state'] ?>
            </td>
            <td>
              <?= $branch['client_name'] ?>
            </td>
            <td>
              <?= $branch['assigned_name'] ?>
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $date =  date('H:i', strtotime($branch['max_time'])  - 21600);    //$rest = substr($branch['max_time'], 11, 5); echo $rest
                ?>
              </p>
              
            </td>
            
          </tr>
          <?php } ?>
        </tbody>
      </table>
     
    </div>
  </div>

</div>
<?php } ?>
<!--begin::BRANCHES -->

<script src="../assets/js/vue.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script>
<script src="../assets/js/reports/index.js"></script>
<script>
  $('input[name="range"]').daterangepicker({
        locale: {
            format: 'MM-DD-YYYY'
        }
    });
</script>

<script>

$( document ).ready(function(){
  
  /* Defaults */
  var exportTable = $("table")
  var export_tables = new TableExport(exportTable, {
      formats: ['xlsx'],
      bootstrap: true,
      exportButtons: false,
      // ignoreRows: null,             
      // ignoreCols: 10,

  }); 
  var tables_data = export_tables.getExportData()
  var export_data = []
  var xlsx_info = {}
  for (table_id in tables_data){
      xlsx_info = tables_data[table_id]["xlsx"]
      export_data.push(tables_data[table_id]["xlsx"].data)
  }
  var fileExtension = xlsx_info.fileExtension
  var mimeType = xlsx_info.mimeType
  $("#export_btn").click(function(){
    console.log('object');
    export_tables.exportmultisheet(export_data, mimeType, "Reporte - "+$('.title_report').text(), [],fileExtension, {}, [])
  })
})
</script>