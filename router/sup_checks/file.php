<?php
// Include Functions
include('../../includes/functions.php');



$data = getChecksSup($_SESSION['access-token'],$_GET['id'],$_GET['start'],$_GET['finished']);
$sups = $data['supervisors'];
$checks = $data['checkins']; 
$completes = $data['completes'];
$incompletes = $data['incompletes'];
$all = $data['all'];

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0&#8243;");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0&#8243;");
header("content-disposition: attachment;filename=report-checkins-supervisor".date('YmdHis').".xls");
?>

<?php if ($checks != null) {?>
<div class="m-section" id="app" >
    <div class="table-responsive" >

        <table  class=" table table-bordered table-hover table table-bordered table_edit" >
        <thead>
          <tr >
            <th>ID</th>
            <th>Site Code</th>
            <th>Name</th>
            <th width="35px">Status</th>
            <th width="35px">Started At</th>
            <th>Finished At</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Client</th>
            <th>Checkin Person</th>
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($checks as $key => $branch) { ?>
          <tr <?php if($branch['route_histories_status'] == 1){echo 'style="background: #eee;"';}?>>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p >
                <?= $branch['site_code'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['name'] ?>
              </p>
              
            </td>
            <td>
            <p>
                <?php if(!empty($branch['route_histories_finished'])){ ?>
                <span class="m-badge m-badge--success m-badge--wide">Complete</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Inclomplete</span>
                <?php } ?>
              </p>
              
            </td>
            <td>
            <p >
                
                <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['route_histories_finished'])){ ?>
                  No Info
                <?php }else{ ?>
                  <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <?php } ?>
              </p>
            </td>
            <td>
              <?= $branch['address'] ?>
            </td>
            <td>
              <?= $branch['city'] ?>
            </td>
            <td>
              <?= $branch['state'] ?>
            </td>
            <td>
              <?= $branch['client_name'] ?>
            </td>
            <td>
              <?= $branch['assigned_name'] ?>
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>
              </p>
              
            </td>
            
          </tr>
          <?php } ?>
        </tbody>
      </table>
     
    </div>
  </div>

</div>
<?php } ?>

<!-- <script src="../assets/js/jquery.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script> -->

<!-- <script>

  /* Defaults */
  var exportTable = document.getElementsByTagName("table")
  var export_tables = new TableExport(exportTable, {
      formats: ['xlsx'],
      bootstrap: true,
      exportButtons: false,
      // ignoreRows: null,             
      // ignoreCols: 10,

  }); 
  var tables_data = export_tables.getExportData()
  var export_data = []
  var xlsx_info = {}
  for (table_id in tables_data){
      xlsx_info = tables_data[table_id]["xlsx"]
      export_data.push(tables_data[table_id]["xlsx"].data)
  }
  var fileExtension = xlsx_info.fileExtension
  var mimeType = xlsx_info.mimeType
  $(document).ready(function(){
      export_tables.exportmultisheet(export_data, mimeType, "Reporte", [],
                                    fileExtension, {}, [])
  })

</script> -->