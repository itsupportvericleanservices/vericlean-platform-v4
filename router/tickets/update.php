<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$response = htdevsUpdate($_SESSION['access-token'], 'tickets',$_GET['id'], $json);
echo json_encode($response);
?>
