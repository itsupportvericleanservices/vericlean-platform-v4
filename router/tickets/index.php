<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$tickets = htdevsGet($_SESSION['access-token'], 'tickets');
// if(!empty($json['searchstr'])) {
//     $tickets = vcGetWorkordersWithPages($_SESSION['access-token'], $json['page'], $json['searchstr']);
// } else {
//     if ($_SESSION['role_id'] == 10) {
//       $tickets  = vcGetWOForUser($_SESSION['access-token'], $_SESSION['id']);
//     } else {
      
//       $tickets = vcGetWorkordersWithPages($_SESSION['access-token'], $json['page']);
//     }
  
// }

  //today es para comparar el schedule date vs la fecha de hoy y definir si existe retraso. (color naranja)
  $today = new DateTime();
function readMoreHelper($story_desc, $chars = 100) {
    if (strlen($story_desc) > 100) {
        $story_desc_full = $story_desc;
        $story_desc = substr($story_desc,0,$chars);  
        $story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
        $story_desc = $story_desc." <a href='#' data-toggle='tooltip' data-placement='bottom' title='".$story_desc_full."'>...<strong>Read More</strong></a>";  
    }
    
    return $story_desc;  
} 
?>

<script>
  // delayed va permitir almacenar en localstorage todos los internal_id de los retrasos para luego pasarlo al filtro.
  var delayed = []
</script>

<div class="m-section">
  <div class="table-responsive">
    <table class="table table-bordered table-hover table table-bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Site</th>
          <th>Client</th>
          <th>Event date</th>
          <th>Due date</th>
          <th>Request Contact</th>
          <th>Request Contact email</th>
          <th>Request Contact phone</th>
          <th>Short Description</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php
            
            
            foreach ($tickets as $key => $ticket) {
        ?>
        <tr>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['id'] ?>
            </a>
          </td>
          <td style="width: 300px;display: -webkit-box;">
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['branch']['address'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['branch']['client']['name'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= date('m-d-Y', strtotime($ticket['event_date'])) ?>
              
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= date('m-d-Y', strtotime($ticket['due_date'])) ?>

            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['request_contact'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['request_contact_email'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['request_contact_phone'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?= $ticket['short_description'] ?>
            </a>
          </td>
          <td>
            <a href="show.php?id=<?= $ticket['id'] ?>">
              <?php if($ticket['status'] == 0){ ?>
              <span class="m-badge m-badge--metal m-badge--wide">Opened</span>
              <?php } ?>
              <?php if($ticket['status'] == 1){ ?>
              <span class="m-badge m-badge--info m-badge--wide">In Progress</span>
              <?php } ?>
              <?php if($ticket['status'] == 2){ ?>
              <span class="m-badge m-badge--info m-badge--wide" style="background: #c14a26 !important">Ready to verify</span>
              <?php } ?>
              <?php if($ticket['status'] == 3){ ?>
              <span class="m-badge m-badge--success m-badge--wide">Completed</span>
              <?php } ?>
            </a>
          </td>
          <td>
            <span class="dropdown">
              <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                data-toggle="dropdown" aria-expanded="true">
                <i class="la la-ellipsis-h"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="show.php?id=<?= $ticket['id'] ?>"><i class="la la-crosshairs"></i>
                  View</a>
                <a class="dropdown-item" href="update.php?id=<?= $ticket['id'] ?>"><i class="la la-edit"></i>
                  Update</a>
                <a onclick="deleteTicket(event, <?= $ticket['id'] ?>)" class="dropdown-item"><i
                    class="la la-trash"></i>
                  Delete</a>
                <?php 
                  if ($ticket['status'] >= 7 && $ticket['status'] <= 9) {
                    
                ?>
                <a onclick="sendWoEmail(event, <?= $ticket['id'] ?>,false)" class="dropdown-item"><i
                    class="la la-send"></i>
                  Send Email</a>

                <?php } ?>
                <?php 
                  if ($ticket['status'] >= 10 && $ticket['status'] <= 11) {
                    
                ?>
                <a onclick="sendWoEmail(event, <?= $ticket['id'] ?>,true)" class="dropdown-item"><i
                    class="la la-send"></i>
                  Send Invoice</a>

                <?php } ?>
              </div>
            </span>
          </td>
        </tr>
        <?php }  ?>

      </tbody>
    </table>
  </div>
</div>

<?php if ($_SESSION['role_id'] != 10) { ?>
<input type="hidden" class="current_page" value="<?= $tickets[1]['page'];?>" id="page">

<div style="float:right">
  <a href="#" onclick="paginate(event, 'back')" id="back">
    <span>
      <span>Back</span>
    </span>
  </a>
  <a href="#" onclick="paginate(event, 'next')" id="next">
    <span>
      <span>Next</span>
    </span>
  </a>
  <a style="margin-left:20px;">Page: <?php echo $tickets[1]['page']; ?></a>
</div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="sendWoEmailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>To:</label>
        <input type="text" name="form-control m-input" id="email_to">
        <input type="hidden" name="form-control m-input" id="internal_id">
        <input type="hidden" name="form-control m-input" id="wo_id">
        <input type="hidden" name="form-control m-input" id="client_name">
        <input type="hidden" name="form-control m-input" id="client_address">
        <input type="hidden" name="form-control m-input" id="request_contact">
        <hr>
        <label>Subject:</label>
        <input type="text" name="form-control m-input" id="email_subject">
        <hr>
        <div id="summernote"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="sendEmail();">Send</button>
      </div>
    </div>
  </div>
</div>
<!--begin::WORKORDERS -->
<script src="../assets/js/workorders/paginate.js" type="text/javascript"></script>

<!--Se llama al index para poder pasarle el scheduled_date y poder filtrar por retraso -->
<script src="../assets/js/workorders/index.js" type="text/javascript"></script><!-- include summernote css/js -->

<link href="../assets/stylesheet/plugins/summernote/summernote.css" rel="stylesheet">
<script src="../assets/js/summernote.min.js"  type="text/javascript"></script>

<!--end::WORKORDERS -->