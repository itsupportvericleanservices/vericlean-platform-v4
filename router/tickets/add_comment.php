<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$response = htdevsCustomPost($_SESSION['access-token'], 'tickets/addcomment', $json);

echo json_encode($response);
?>
