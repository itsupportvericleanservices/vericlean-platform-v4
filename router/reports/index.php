<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);
// $sites = vcGetBranches($_SESSION['access-token']);
// $clients = vcGetClients($_SESSION['access-token']);
$data =getReportNoCheckins($_SESSION['access-token'],$json['by'],$json['id'],$json['start'],$json['finished']);
$sites = $data['sites'];
$clients = $data['clients'];
$checks = $data['checkins'];
$cleaners = $data['cleaners'];
$supervisors = $data['supervisors'];
?>
<!-- <pre><?php print_r($json)?></pre> -->
<div class="m-portlet__head">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">

      <h3 class=" m-portlet__head-text">
      <label>Range</label>
        <input type="text" name="range"class="form-control" id="from_date_filter" value="" style="margin-left: 5px;margin-right: 8px;    width: 180px;">

        Filters
        <div class="">
          <div style="float:left; margin-left:10px;width: 90px;">
            <select name="filter_by" class="filter_by form-control" onchange="showSelect()">
              <option value="" selected>Select filter</option>
              <option  value="client" <?php if($json['by'] == 'client'){echo 'selected';}?>>Client</option>
              <option  value="sites" <?php if($json['by'] == 'sites'){echo 'selected';}?>>sites</option>
              <option  value="supervisor" <?php if($json['by'] == 'supervisor'){echo 'selected';}?>>supervisors</option>
              <option  value="cleaner" <?php if($json['by'] == 'cleaner'){echo 'selected';}?>>cleaners</option>
            </select>
          </div>

          <div class="filter_by_sites" style="display:none;float:left;width: 120px;<?php if($json['by'] == 'sites'){echo 'display:inline-block;';}?>">
            <select  name="client_id" class="site_id form-control" onchange="showButton()">
              <option value="" selected>Select One</option>
              <?php foreach ($sites as $key => $site) { ?>
                <?php if($site[1] == ""){?>
                  <option value="<?= $site[0] ?>" <?php if($json['id'] == $site[0]){echo 'selected';}?>>no name, id:<?= $site[0] ?></option>
                <?php }else{ ?>
                  <option value="<?= $site[0] ?>" <?php if($json['id'] == $site[0]){echo 'selected';}?>><?= $site[1] ?></option>
                <?php }  ?>
              <?php  } ?>
            </select>
          </div>
          
          <div class="filter_by_client" style="display:none;float:left;width: 120px;<?php if($json['by' == '']){echo 'display:inline-block;';}?>">
            <select  name="client_id"  class="client_id  form-control" onchange="showButton()">
              <option value="" selected>Select One</option>
              <?php foreach ($clients as $key => $client) { if($client[1] == ""){?>
                
                  <option value="<?= $client[0] ?>" <?php if($json['id'] == $client[0]){echo 'selected';}?>>no name, id:<?= $client[0] ?></option>
                <?php }else{ ?>
                  <option value="<?= $client[0] ?>" <?php if($json['id'] == $client[0]){echo 'selected';}?>><?= $client[1] ?></option>
                
              <?php } } ?>
            </select>
          </div>

          <div class="filter_by_sup" style="display:none;float:left;width: 120px;<?php if($json['by' == '']){echo 'display:inline-block;';}?>">
            <select  name="supervisor_id" class="supervisor_id form-control" onchange="showButton()">
              <option value="" selected>Select One</option>
              <?php usort($supervisors, function($a, $b) {
                return strtolower($a[1]) > strtolower($b[1]);
                
                });
               foreach ($supervisors as $key => $site) { ?>
                <?php if($site[1] == ""){?>
                  <option value="<?= $site[0] ?>" <?php if($json['id'] == $site[0]){echo 'selected';}?>>no name, id:<?= $site[0] ?></option>
                <?php }else{ ?>
                  <option value="<?= $site[0] ?>" <?php if($json['id'] == $site[0]){echo 'selected';}?>><?= $site[1]." ".$site[2] ?></option>
                <?php }  ?>
              <?php  } ?>
            </select>
          </div>

          <div class="filter_by_cleaner" style="display:none;float:left;width: 120px;<?php if($json['by' == '']){echo 'display:inline-block;';}?>">
            <select  name="cleaner_id" class="cleaner_id form-control" onchange="showButton()">
              <option value="" selected>Select One</option>
              <?php usort($cleaners, function($a, $b) {
                return strtolower($a[1]) > strtolower($b[1]);
                
                });
               foreach ($cleaners as $key => $site) { ?>
                <?php if($site[1] == ""){?>
                  <option value="<?= $site[0] ?>" <?php if($json['id'] == $site[0]){echo 'selected';}?>>no name, id:<?= $site[0] ?></option>
                <?php }else{ ?>
                  <option value="<?= $site[0] ?>" <?php if($json['id'] == $site[0]){echo 'selected';}?>><?= $site[1]." ".$site[2] ?></option>
                <?php }  ?>
              <?php  } ?>
            </select>
          </div>
          <div class="filter" style="float:left; margin-left: 10px;">
            <button onclick="getReports('all',0)" class="btn btn-info">Filter</button>
          </div>
          <div class="clear" style="float:left; margin-left: 10px;">
            <a href="index.php"class="btn btn-danger">Clear</a>
          </div>
        </div>
      </h3>
    </div>
  </div>

  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary" href='../../router/reports/file.php?by=<?= $json['by'] ?>&id=<?= $json['id'] ?>&start=<?= $json['start'] ?>&finished=<?= $json['finished'] ?>' >Export</a>
        
      </li>
    </ul>
  </div>
</div>

<div class="m-portlet__body">
  <!--begin::Section-->
  <!--end::Section-->
<!-- <pre><?php print_r($data)?></pre> -->
 
<div class="m-section" id="app" >
    <div class="table-responsive" >
      <?php foreach ($checks as $key => $value) { ?>


         <!-- <h5><small>Date:</small> <span style="background:green;color:white;padding:5px;border-radius:5px;"><?= $key?></span> </h5> -->
        <table  class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr>
            <th>ID</th>
            <th width="70px">Site Code</th>
            <th>Name</th>
            <th>Type</th>
            <th>Client</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <th>Frequency</th>
            <th>Cleaner</th>
            <th>Cleaner Phone</th>
            <th>Area Manager</th>
            <th>Area Manager Phone</th>
            <th>Supervisor</th>
            <th>Supervisor Phone</th>
            <th>Active</th>
            <th>Muted</th>
            <th>Min duration on site</th>
            <th>Max duration on site</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
          <?php
                   foreach ($value as $key => $branch) {
                ?>
          <tr>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p >
                <?= $branch['site_code'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['name'] ?>
              </p>
              
            </td>
            <td>
              <?php if($branch['client_indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
            </td>
            <td>
              <?= $branch['client_name'] ?>
            </td>
            <td>
              <p >
                <?php echo $branch['address'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['city'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['state'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['zipcode'] ?>
              </p>
              
            </td>
            <td>
              <p>
                <?php if(empty($branch['frequency'])){ ?>
                not assigned
                <?php }else{ ?>
                <?php
							$frequencies = explode(",", $branch['frequency']);
								foreach($frequencies as $freq){
									if($freq == 1){
										echo "Monday ";
									}
									if($freq == 2){
										echo "Tuesday ";
									}
									if($freq == 3){
										echo "Wednesday ";
									}
									if($freq == 4){
										echo "Thursday ";
									}
									if($freq == 5){
										echo "Friday ";
									}
									if($freq == 6){
										echo "Saturday ";
									}
									if($freq == 7){
										echo "Sunday ";
									}
								}
							?>
                <?php } ?>
              </p>
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['cleaner_phone']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['areamanager_phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['areamanager_phone']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['supervisor_phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['supervisor_phone']  ?>
                <?php } ?>
              </p>
             
            </td>
            <td>
              <a href="show.php?id=<?= $branch['id'] ?>">
                <?php if($branch['active'] == 1){ ?>
                <span class="m-badge m-badge--success m-badge--wide">Active</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                <?php } ?>
              </a>
            </td>
            <td>
              <a href="show.php?id=<?= $branch['id'] ?>">
                <?php if($branch['muted'] == 1){ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Without alerts</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--success m-badge--wide">With alerts</span>
                <?php } ?>
              </a>
            </td>
            <td>
              <p >
                <?= $branch['duration_min'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?= $branch['duration_max'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>
              </p>
              
            </td>
            
          </tr>
          <?php } ?>
        </tbody>
      
      <?php } ?>
    </div>
  </div>

</div>
<!--begin::BRANCHES -->

<script src="../assets/js/vue.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script>
<script src="../assets/js/reports/index.js"></script>
<script>

  $('input[name="range"]').daterangepicker({
        locale: {
            format: 'MM-DD-YYYY'
        }
    });
</script>
<script>
   
  new Vue({
    el: "#app",
    data: {
      mostrar: false
    },
    methods: {
      mostrarUno(id) {
        this.mostrar = id
      },
      updateBranchInline(event, id) {
        event.preventDefault();
        var name = $("#name").val();
        var address = $("#address").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var zipcode = $("#zipcode").val();
        var site_code = $("#site_code").val();
        var cleaner_id = $("#cleaner_id").val();
        var area_manager_id = $("#area_manager_id").val();
        var supervisor_id = $("#supervisor_id").val();
        var duration_max = $("#duration_max").val();
        var duration_min = $("#duration_min").val();
        var max_time = $("#max_time").val();
        var hora = Number(max_time.substr(0, 2)) - 6
        var minutos = max_time.substr(3, 2)
        var time = max_time.substr(6, 3)

        var arrivale_max = `${hora}:${minutos} ${time}`
        $.ajax({
          type: "POST",
          url: "../../../router/branches/update.php?id=" + id,
          data: JSON.stringify({
            name: name,
            address: address,
            state: state,
            city: city,
            zipcode: zipcode,
            site_code: site_code,
            cleaner_id: cleaner_id,
            area_manager_id: area_manager_id,
            supervisor_id: supervisor_id,
            duration_max: duration_max,
            duration_min: duration_min,
            max_time: max_time
          }),
          dataType: "json"
        }).done(function (data) {
          console.log(data)
          swal({
            title: "Success!",
            text: "Site updated",
            icon: "success",
            button: "Ok",
            closeOnEsc: true
          }).then(result => {
            window.location.href = "../../../branches/index.php";
          });
        }).error(function (err) {
          console.log(err)
          swal({
            title: 'Error!',
            text: 'Something happened!',
            type: 'error',
            confirmButtonText: 'Cool'
          })
        });

      }
    },
  })

  
</script>
<script>


  // /* Defaults */
  // var exportTable = $("table")
  // var export_tables = new TableExport(exportTable, {
  //     formats: ['xlsx'],
  //     bootstrap: true,
  //     exportButtons: false,
  //     // ignoreRows: null,             
  //     // ignoreCols: 10,

  // }); 
  // var tables_data = export_tables.getExportData()
  // var export_data = []
  // var xlsx_info = {}
  // for (table_id in tables_data){
  //     xlsx_info = tables_data[table_id]["xlsx"]
  //     export_data.push(tables_data[table_id]["xlsx"].data)
  // }
  // var fileExtension = xlsx_info.fileExtension
  // var mimeType = xlsx_info.mimeType
  // $("#export_btn").click(function(){
  //     export_tables.exportmultisheet(export_data, mimeType, "Reporte - "+$('.title_report').text(), [],
  //                                   fileExtension, {}, [])
  // })

</script>