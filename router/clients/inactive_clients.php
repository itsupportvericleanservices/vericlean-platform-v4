<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

if($json['type'] == 'client' || $json['type'] == 'customer' || $json['type'] == 'customer_for_client'){
    $clients = vcGetClientFilter($_SESSION['access-token'], $json['id']);
}

if(empty($json['type'])){
    $clients = vcGetInactiveClients($_SESSION['access-token']);
}
?>


                        <div class="m-section">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Addres2</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>ZipCode</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Active</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($clients as $key => $client) {
                                        ?>
                                        <tr>
                                            <td scope="row">
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['id'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['address'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['address2'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['state'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['city'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['zipcode'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['phone'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['email'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if($client['active'] == 1){ ?>
                                                        <span class="m-badge m-badge--success m-badge--wide">Active</span>
                                                    <?php }else{ ?>
                                                        <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                        data-toggle="dropdown" aria-expanded="true">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="show.php?id=<?= $client['id'] ?>"><i class="la la-crosshairs"></i>
                                                            View</a>
                                                        <a class="dropdown-item" href="update.php?id=<?= $client['id'] ?>"><i class="la la-edit"></i>
                                                            Update</a>
                                                        <a onclick="deleteClient(event, <?= $client['id'] ?>)" class="dropdown-item"><i class="la la-trash"></i>
                                                            Delete</a>
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
