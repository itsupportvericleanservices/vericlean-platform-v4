<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$response = vcCreateClient($_SESSION['access-token'], $json);
echo json_encode($response);
?>
