<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);


if($json['type'] == "client"){
    $response = vcGetAllClientsOrCustomers($_SESSION['access-token'], $json['indirect']);
    echo json_encode($response);
}

if($json['type'] == "customer"){
    $response = vcGetAllClientsOrCustomers($_SESSION['access-token'], $json['indirect']);
    echo json_encode($response);
}

?>
