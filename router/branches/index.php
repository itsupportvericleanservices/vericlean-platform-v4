<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);
$cleaners = vcGetCleaners($_SESSION['access-token']);
$area_managers = vcGetAreaManagers($_SESSION['access-token']);
$supervisors = vcGetSupervisors($_SESSION['access-token']);
if(!empty($json['searchstr'])) {
  $branchesWithPage = vcGetBranchesWithPages($_SESSION['access-token'], $json['page'], $json['searchstr']);
} else {
  $branchesWithPage = vcGetBranchesWithPages($_SESSION['access-token'], $json['page']);
}

$filtro = '';
$valor = '';

if (isset($json['filter_by']) AND isset($json['value']) ) {
  
  $filtro = $json['filter_by'];
  $valor = $json['value'];
}

if (isset($json['filtro'])) {
  $filtro = $json['filtro'];
}

$select = "selected";



?>
<div class="m-portlet__head">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
      <h3 class="m-portlet__head-text">
        Filters
        <div>
          <div style="float:left; margin-left:10px;width: 120px;">
            <select name="filter_by" class="filter_by form-control" onchange="branches_filter_by();">
              <option value="" selected>Show All</option>
              <option <?php if($filtro == 'client'){ echo $select;} ?> value="client">Client</option>
              <option <?php if($filtro == 'customer'){ echo $select;} ?>  value="customer">Customer</option>
              <option <?php if($filtro == 'customer_for_client'){ echo $select;} ?>  value="customer_for_client">Client Customers</option>
              <option <?php if($filtro == 'supervisor'){ echo $select;} ?>  value="supervisor">Supervisor</option>
              <option <?php if($filtro == 'cleaner'){ echo $select;} ?>  value="cleaner">Cleaner</option>
              <option <?php if($filtro == 'state'){ echo $select;} ?>  value="state">State</option>
              <option <?php if($filtro == 'city'){ echo $select;} ?>  value="city">City</option>
              <option <?php if($filtro == 'address'){ echo $select;} ?>  value="address">Address</option>
              <option <?php if($filtro == 'site_name'){ echo $select;} ?>  value="site_name">Site Name</option>
              <option <?php if($filtro == 'site_code'){ echo $select;} ?>  value="site_code">Site code</option>
            </select>
          </div>
          
          <div class="filter_by_custom" style="margin-left: 20px;display:none;float:left;width: 120px;">
            <input type="text" id="filter_custom" class="form-control" value="<?php echo $valor; ?>">
          </div>
          <div class="filter_by_client" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="client_id" class="client_id form-control">
              <option value="" selected>Select One</option>
            </select>
          </div>
          <div class="filter_by_customer" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="customer_id" class="customer_id form-control">
              <option value="" selected>Select One</option>
            </select>
          </div>
          <div class="filter_by_cleaner" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="cleaner_id" class="cleaner_id form-control">
              <option value="" selected>Select One</option>
            </select>
          </div>
          <div class="filter_by_supervisor" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="supervisor_id" class="supervisor_id form-control">
              <option value="" selected>Select One</option>
            </select>
          </div>
          <div class="filter_by_state" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="state" class="state form-control">
              <option value="" selected>Select One</option>
            </select>
          </div>
          <div class="filter_by_city" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="city" class="city form-control">
              <option value="" selected>Select One</option>
            </select>
          </div>
          <div class="filter_by_client_for_customer" style="display:none;float:left;width: 120px;">
            <select onchange="showButton()" name="client_for_customer_id" class="client_for_customer_id form-control">
              <option value="" selected>Select Client</option>
            </select>
          </div>
          <div class="filter" style="display:none;float:left; margin-left: 10px;">
            <button onclick="branches_filter()" class="btn btn-info">Filter</button>
          </div>
          <div class="clear" style="display:none;float:left; margin-left: 10px;">
            <button onclick="getBranches(1)" class="btn btn-danger">Clear</button>
          </div>
          <div style="clear:both;"></div>
        </div>
      </h3>
    </div>
  </div>

  <?php if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3) {

                         ?>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <!-- <button onclick="getBranches(1)" class="btn btn-danger">Show all</button> -->
        <!-- <button class="btn-toolbar bottom" onclick="exportTableToExcel('table_sites', 'data')">export </button> -->
        <a class="btn btn-secondary" href="../../router/branches/file.php" >Export</a>
        <a href="create.php" class="btn btn-primary" style="margin-left:10px">
          <span>
            <i class="la la-plus"></i>
            <span>New Site</span>
          </span>
        </a>
        <a href="requestList.php" class="btn btn-primary" style="margin-left:10px;">
          <span>

            <span>Locations Requested</span>
          </span>
        </a>
      </li>
    </ul>
  </div>
  <?php } ?>
</div>

<div class="m-portlet__body">
  <!--begin::Section-->
  <!--end::Section-->
<pre>
    
</pre>
  <!-- s-->
  <div class="m-section" id="app" >
    <div class="table-responsive" style="<?php  if(count($branchesWithPage[0]) < 3){ echo "height: 260px;"; } ?>">
      <table class="table table-bordered table-hover table table-bordered table_edit" id="table_sites">
        <thead>
          <tr>
            <th>ID</th>
            <th width="70px">Site Code</th>
            <th>Name</th>
            <th>Type</th>
            <th>Client</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <th>Frequency</th>
            <th>Cleaner</th>
            <th>Cleaner Phone</th>
            <th>Area Manager</th>
            <th>Area Manager Phone</th>
            <th>Supervisor</th>
            <th>Supervisor Phone</th>
            <th>Active</th>
            <th>Muted</th>
            <th>Min duration on site</th>
            <th>Max duration on site</th>
            <th>Maximum arrival time</th>
            <th class="tableexport-ignore">Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php
                   foreach ($branchesWithPage[0] as $key => $branch) {
                ?>
          <tr>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?= $branch['site_code'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="site_code" value="<?= $branch['site_code'] ?>">
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php echo $branch['name'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="name" value="<?= $branch['name'] ?>">
            </td>
            <td>
              <?php if($branch['client']['indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
            </td>
            <td>
              <?= $branch['client']['name'] ?>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?> ">
                <?php echo $branch['address'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="address" value="<?= $branch['address'] ?>">
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php echo $branch['city'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="city" value="<?= $branch['city'] ?>">
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php echo $branch['state'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="state" value="<?= $branch['state'] ?>">
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php echo $branch['zipcode'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="zipcode" value="<?= $branch['zipcode'] ?>">
            </td>
            <td>
              <p>
                <?php if(empty($branch['frequency'])){ ?>
                not assigned
                <?php }else{ ?>
                <?php
							$frequencies = explode(",", $branch['frequency']);
								foreach($frequencies as $freq){
									if($freq == 1){
										echo "Monday ";
									}
									if($freq == 2){
										echo "Tuesday ";
									}
									if($freq == 3){
										echo "Wednesday ";
									}
									if($freq == 4){
										echo "Thursday ";
									}
									if($freq == 5){
										echo "Friday ";
									}
									if($freq == 6){
										echo "Saturday ";
									}
									if($freq == 7){
										echo "Sunday ";
									}
								}
							?>
                <?php } ?>
              </p>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php if(empty($branch['cleaner'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner']['first_name'] . " " . $branch['cleaner']['last_name']  ?>
                <?php } ?>
              </p>
              <select v-if="mostrar == <?= $branch['id'] ?>" id="cleaner_id" name="cleaner_id">
              <option value="">Select One</option>
                <?php foreach ($cleaners as $key => $cleaner) { ?>
                <option value="<?php echo $cleaner['id'] ?>" <?php if(!empty($branch['cleaner']['id']) &&
                  ($branch['cleaner']['id']==$cleaner['id'])){ ?>selected
                  <?php } ?>>
                  <?php echo $cleaner['first_name']." ".$cleaner['last_name'] ?>
                </option>
                <?php } ?>
              </select>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php if(empty($branch['cleaner']['phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['cleaner']['phone']  ?>
                <?php } ?>
              </p>
              <select v-if="mostrar == <?= $branch['id'] ?>" id="cleaner_id" name="cleaner_id">
              
              </select>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php if(empty($branch['area_manager'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['area_manager']['first_name']. " " . $branch['area_manager']['last_name']  ?>
                <?php } ?>
              </p>
              <select v-if="mostrar == <?= $branch['id'] ?>" id="area_manager_id" name="area_manager_id">
              <option value="">Select One</option>
                <?php foreach ($area_managers as $key => $area) { ?>
                <option value="<?php echo $area['id'] ?>" <?php if(!empty($branch['area_manager']['id']) &&
                  ($branch['area_manager']['id']==$area['id'])){ ?>selected
                  <?php } ?>>
                  <?php echo $area['first_name']." ".$area['last_name'] ?>
                </option>
                <?php } ?>
              </select>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php if(empty($branch['area_manager']['phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['area_manager']['phone']  ?>
                <?php } ?>
              </p>
              <select v-if="mostrar == <?= $branch['id'] ?>" id="cleaner_id" name="cleaner_id">
              
              </select>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php if(empty($branch['supervisor'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor']['first_name'] . " " . $branch['supervisor']['last_name']  ?>
                <?php } ?>
              </p>
              <select v-if="mostrar == <?= $branch['id'] ?>" id="supervisor_id" name="supervisor_id">
              <option value="">Select One</option>
                <?php foreach ($supervisors as $key => $sup) { ?>
                <option value="<?php echo $sup['id'] ?>" <?php if(!empty($branch['supervisor']['id']) &&
                  ($branch['supervisor']['id']==$sup['id'])){ ?>selected
                  <?php } ?> >
                  <?php echo $sup['first_name']." ".$sup['last_name'] ?>
                </option>
                <?php } ?>
              </select>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php if(empty($branch['supervisor']['phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['supervisor']['phone']  ?>
                <?php } ?>
              </p>
              <select v-if="mostrar == <?= $branch['id'] ?>" id="cleaner_id" name="cleaner_id">
              
              </select>
            </td>
            <td>
              <a href="show.php?id=<?= $branch['id'] ?>">
                <?php if($branch['active'] == 1){ ?>
                <span class="m-badge m-badge--success m-badge--wide">Active</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                <?php } ?>
              </a>
            </td>
            <td>
              <a href="show.php?id=<?= $branch['id'] ?>">
                <?php if($branch['muted'] == 1){ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Without alerts</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--success m-badge--wide">With alerts</span>
                <?php } ?>
              </a>
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?= $branch['duration_min'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="number" id="duration_min" placeholder="in minutes"
                value="<?= $branch['duration_min'] ?>">
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?= $branch['duration_max'] ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="number" id="duration_max" placeholder="in minutes"
                value="<?= $branch['duration_max'] ?>">
            </td>
            <td>
              <p v-if="mostrar != <?= $branch['id'] ?>">
                <?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>
              </p>
              <input v-if="mostrar == <?= $branch['id'] ?>" type="text" id="max_time" placeholder="in minutes" value="<?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>">
            </td>
            <td class="tableexport-ignore">
              <span class="dropdown">
                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"
                  aria-expanded="true">
                  <i class="la la-ellipsis-h"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right ">
                  <a class="dropdown-item " href="show.php?id=<?= $branch['id'] ?>"><i class="la la-crosshairs"></i>
                    View</a>
                  <?php if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3) {?>
                  <a class="dropdown-item " href="update.php?id=<?= $branch['id'] ?>"><i class="la la-edit"></i>
                    Update</a>
                  <a onclick="deleteBranch(event, <?= $branch['id'] ?>)" class="dropdown-item "><i class="la la-trash"></i>
                    Delete</a>
                  <?php
                    if ($_SESSION['role_id'] == 1) {?>
                  <a @click="mostrarUno( <?= $branch['id'] ?>)" class="dropdown-item "><i class="fas fa-pen-square"></i>
                    Edit Here</a>
                  <?php } ?>
                  <a v-if="mostrar == <?= $branch['id'] ?>" @click="mostrar = false" class="btn btn-danger" style="margin-left:25px;margin-bottom: 3px;">Cancel</a>
                  <a v-if="mostrar == <?= $branch['id'] ?>" @click="updateBranchInline(event, <?= $branch['id'] ?>)"
                    class="btn btn-primary" style="margin-left:3px;">Save changes</a>
                  <?php }?>
                </div>
              </span>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
                      
  <?php
          //TOMA EL VALOR DE LA PAGINA QUE ME DEVUELVE LA API, PARA PASARLO AL JS Y PODER
          //EJECUTAR EL PAGINADO.
          foreach ($branchesWithPage as $key => $branch) {
            if(!empty($branch['page'])){
         ?>
  <input type="hidden" class="current_page" value="<?= $branch['page'] ?>" id="page">
  <?php } ?>
  <?php } ?>

  <div style="float:right">
    <a href="#" onclick="paginate(event, 'back')" id="back">
      <span>
        <span>Back</span>
      </span>
    </a>
    <a href="#" onclick="paginate(event, 'next')" id="next">
      <span>
        <span>Next</span>
      </span>
    </a>
    <a style="margin-left:20px;">Page:
      <?php echo $branch['page']; ?></a>
  </div>

</div>
<!--begin::BRANCHES -->
<script src="../assets/js/branches/paginate.js" type="text/javascript"></script>
<script src="../assets/js/branches/update.js"></script>
<script src="../assets/js/vue.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script>
<script src="../../branches/file.js"></script>
<script>
    var filtro = JSON.parse(window.localStorage.getItem('filter_by'))
  var val = JSON.parse(window.localStorage.getItem('value'))
          document.addEventListener('DOMContentLoaded', branches_filter_by(filtro,val));
  new Vue({
    el: "#app",
    data: {
      mostrar: false
    },
    methods: {
      mostrarUno(id) {
        this.mostrar = id
      },
      updateBranchInline(event, id) {
        event.preventDefault();
        var name = $("#name").val();
        var address = $("#address").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var zipcode = $("#zipcode").val();
        var site_code = $("#site_code").val();
        var cleaner_id = $("#cleaner_id").val();
        var area_manager_id = $("#area_manager_id").val();
        var supervisor_id = $("#supervisor_id").val();
        var duration_max = $("#duration_max").val();
        var duration_min = $("#duration_min").val();
        var max_time = $("#max_time").val();
        var hora = Number(max_time.substr(0, 2)) - 6
        var minutos = max_time.substr(3, 2)
        var time = max_time.substr(6, 3)

        var arrivale_max = `${hora}:${minutos} ${time}`
        $.ajax({
          type: "POST",
          url: "../../../router/branches/update.php?id=" + id,
          data: JSON.stringify({
            name: name,
            address: address,
            state: state,
            city: city,
            zipcode: zipcode,
            site_code: site_code,
            cleaner_id: cleaner_id,
            area_manager_id: area_manager_id,
            supervisor_id: supervisor_id,
            duration_max: duration_max,
            duration_min: duration_min,
            max_time: max_time
          }),
          dataType: "json"
        }).done(function (data) {
          console.log(data)
          swal({
            title: "Success!",
            text: "Site updated",
            icon: "success",
            button: "Ok",
            closeOnEsc: true
          }).then(result => {
            window.location.href = "../../../branches/index.php";
          });
        }).error(function (err) {
          console.log(err)
          swal({
            title: 'Error!',
            text: 'Something happened!',
            type: 'error',
            confirmButtonText: 'Cool'
          })
        });

      }
    },
  })

  
</script>
<script>


  // /* Defaults */
  // var exportTable = $("table")
  // var export_tables = new TableExport(exportTable, {
  //     formats: ['xlsx'],
  //     bootstrap: true,
  //     exportButtons: false,
  //     // ignoreRows: null,             
  //     // ignoreCols: 10,

  // }); 
  // var tables_data = export_tables.getExportData()
  // var export_data = []
  // var xlsx_info = {}
  // for (table_id in tables_data){
  //     xlsx_info = tables_data[table_id]["xlsx"]
  //     export_data.push(tables_data[table_id]["xlsx"].data)
  // }
  // var fileExtension = xlsx_info.fileExtension
  // var mimeType = xlsx_info.mimeType
  // $("#export_btn").click(function(){
  //     export_tables.exportmultisheet(export_data, mimeType, "Reporte - "+$('.title_report').text(), [],
  //                                   fileExtension, {}, [])
  // })

</script>