<?php 
include('../includes/header.php');
$clients_all = vcGetActiveClients($_SESSION['access-token']);
if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2) {
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Active Customers</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content" id="app2">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Filters
                                  <div>
                                    <div style="float:left; margin-left:10px;width:120px;">
                                      <select name="filter_by" class="filter_by form-control" onchange="clients_filter_by();">
                                        <option value="" selected>Show All</option>
                                        <option value="client">Client</option>
                                        <option value="customer">Customer</option>
                                        <option value="customer_for_client">Client Customer</option>
                                        
                                      </select>
                                    </div>
                                    <div class="filter_by_client" style="display:none;float:left;width: 120px;">
                                      <select name="client_id" class="client_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_customer" style="display:none;float:left;width: 120px;">
                                      <select name="customer_id" class="customer_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_client_for_customer" style="display:none;float:left;width: 120px;">
                                      <select name="client_for_customer_id" class="client_for_customer_id form-control">
                                        <option value="" selected>Select Client</option>
                                      </select>
                                    </div>
                                    <div class="filter" style="display:none;float:left; margin-left: 10px;">
                                      <button onclick="clients_filter()" class="btn btn-info">Filter</button>
                                      <button onclick="location.reload()" class="btn btn-danger">Clear</button>
                                    </div>
                                    <div style="clear:both;"></div>
                                  </div>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                <div style="margin-right: 10px;background: gainsboro;border-radius: 3px;padding: 5px;">
                                        <span>
                                            <i class="la la-search"></i>
                                            <input @keyup="filter_client" v-model="client" type="text" placeholder="search by name"></input>
                                        </span>
                                    </div>
                                    <a href="create.php" class="btn btn-primary">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>New Customer</span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                   
                    <div class="m-portlet__body" id="m-portlet__body"></div>
                    
                    
                </div>

                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
        <!--END XL12-->
    </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->



<!-- begin::Quick Sidebar -->
<!-- end::Quick Sidebar -->

<script src="../assets/js/clients/delete.js" type="text/javascript"></script>
<script src="../assets/js/vue.min.js"></script>
<script src="../assets/js/clients/index.js" type="text/javascript"></script>

<?php include('../includes/footer.php');?>

<script>


   

        document.addEventListener('DOMContentLoaded',getActiveClients());
      

</script>

<?php } ?>
