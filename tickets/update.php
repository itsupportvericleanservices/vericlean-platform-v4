<?php
include('../includes/header.php');
$clients = vcGetClients($_SESSION['access-token']);

$active_sites = vcGetActiveBranches($_SESSION['access-token']);
$ticket = htdevsGetSingle($_SESSION['access-token'],'tickets', $_GET['id']);
//$files = vcGetWorkorderfiles($_SESSION['access-token'], $_GET['id']);
$sites = vcGetSitesForClient($_SESSION['access-token'],$ticket['branch']['client']['id']);
$cleaners = vcGetUsers($_SESSION['access-token']);
?>


<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');

?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">Ticket <?php echo $ticket['id'] ?></h3>
            <input type="text" value="<?php echo $_SESSION['id'] ?>" id="user_id" style="display:none">
            <input type="text" value="<?php echo $ticket['id'] ?>" id="ticket_id" style="display:none">
        </div>
    </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">

            <!--begin::Portlet-->
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">

                            <div class="form-group m-form__group">
                                <label for="clients">Type of Client</label>
                                <select class="form-control m-input" id="type_client" name="type_client" onchange="selectClient()">
                                    <option value="">Select One</option>
                                    <option value="client" <?= ($ticket['branch']['client']['indirect'] == 0) ? "selected" : ""; ?>>Client</option>
                                    <option value="customer" <?= ($ticket['branch']['client']['indirect'] == 1) ? "selected" : ""; ?>>Customer</option>
                                </select>
                            </div>

                            <div class="form-group m-form__group" id="client_content"  onchange="sitesForClient()">
                                <label id="lbl_client_customer"><?= ($ticket['branch']['client']['indirect'] == 0) ? 'Clients' : 'Customers' ?></label>
                                <select class="form-control m-input" id="client_id" name="client_id">
                                    <option value="">Select One</option>
                                    <?php
                                        foreach ($clients as $key => $client) {
                                            
                                            $name = '';
                                            $name .= ($client['site_code']) ? $client['site_code'] : '';
                                            $name .= ($client['name']) ? ' - '.$client['name'] : '';
                                            $name .= ($client['address']) ? ' - '.$client['address'] : '';
                                            $selected = ($client['id'] == $ticket['branch']['client']['id']) ? 'selected' : '';
                                            echo "<option class='type_".$client['indirect']."' value='".$client['id']."' ".$selected.">".$name."</option>";
                                            
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group" id="branch_id_container" >
                                <label>Sites</label>
                                <select class="form-control m-input" id="branch_id" name="branch_id">
                                    <option value="" selected>Select One</option>
                                    <?php
                                        foreach ($sites as $key => $site) {


                                            $selected = ($site['id'] == $ticket['branch_id']) ? 'selected' : '';
                                            echo "<option  class='option_remove' value='".$site['id']."' ".$selected.">".$site['site_code']." - " .$site['name']." - ".$site['address']."</option>";

                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                              <label>Request Contact</label>
                              <input type="text" class="form-control m-input" id="request_contact" placeholder="Request Contact" value="<?= $ticket['request_contact'] ?>" >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Request Contact Email</label>
                              <input type="text" class="form-control m-input" id="request_contact_email" placeholder="Site Contact email"  value="<?= $ticket['request_contact_email'] ?>" >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Request Contact Phone</label>
                              <input type="text" class="form-control m-input" id="request_contact_phone" placeholder="Request Phone"  value="<?= $ticket['request_contact_phone'] ?>" >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Event Date</label>
                              <input type="date" class="form-control m-input" id="event_date" placeholder="Event Date"   value="<?= substr($ticket['event_date'], 0, 10);  ?>" >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Due Date</label>
                              <input type="date" class="form-control m-input" id="due_date" placeholder="Due date" value="<?= substr($ticket['due_date'], 0, 10);  ?>" >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Short Description</label>
                              <textarea class="form-control m-input" id="short_description" placeholder="Short Description" rows="4" cols="50" ><?= $ticket['short_description'] ?> </textarea  >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Description</label>
                              <textarea class="form-control m-input" id="description" placeholder="Description" rows="4" cols="50" > <?= $ticket['description'] ?> </textarea  >
                            </div>
                            <div class="form-group m-form__group">
                              <label>Status</label>
                              <select class="form-control m-input" id="status" name="status" >
                                <option value="">Select One</option>
                                <option value="0" <?php if($ticket['status'] == 0){ echo "selected"; } ?>>Opened</option>
                                <option value="1" <?php if($ticket['status'] == 1){ echo "selected"; } ?>>In Progress</option>
                                <option value="2" <?php if($ticket['status'] == 2){ echo "selected"; } ?>>Ready to verify</option>
                                <option value="3" <?php if($ticket['status'] == 3){ echo "selected"; } ?>>Completed</option>
                              </select>
                            </div>
                            <hr>
                            <div>
                              <h3>Comments</h3>
                              
                              <table class="table table-bordered table-hover table table-bordered">
                                <thead>
                                  <th>#</th>
                                  <th>Comment</th>
                                  <th>Date</th>
                                  <th>User</th>
                                  <th></th>
                                </thead>
                                <tbody>
                                  <?php 
                                    foreach ($ticket['comments'] as $key => $comment) {
                                  ?>
                                  <tr>
                                    <td><?= $comment['id'] ?></td>
                                    <td><?= $comment['comment'] ?></td>
                                    <td><?= date('m-d-Y H:i', strtotime($comment['created_at'])) ?></td>
                                    <td><?= $comment['user']['first_name'] ?></td>
                                    <td>
                                      <span class="dropdown">
                                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                          data-toggle="dropdown" aria-expanded="true">
                                          <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <a class="dropdown-item" href="../tickets/show.php?id=<?= $wo['id'] ?>"><i class="la la-crosshairs"></i>
                                            View</a>
                                          <a class="dropdown-item" href="../tickets/update.php?id=<?= $wo['id'] ?>"><i class="la la-edit"></i>
                                            Update</a>
                                          
                                        </div>
                                      </span>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_comment">Add Comment</button>
                            </div>
                            <hr>
                            <div>
                              <h3>Work Orders</h3>
                              
                              <table class="table table-bordered table-hover table table-bordered">
                                <thead>
                                  <th>#</th>
                                  <th>Requested Date</th>
                                  <th>Status</th>
                                  <th></th>
                                </thead>
                                <tbody>
                                  <?php 
                                    foreach ($ticket['work_orders'] as $key => $wo) {
                                  ?>
                                  <tr>
                                    
                                    <td><?= $wo['id'] ?></td>
                                    <td><?= date('m-d-Y H:i', strtotime($wo['requested_date'])) ?></td>
                                    <td>
                                      <?php if($wo['status'] == 0){ ?>
                                      <span class="m-badge m-badge--metal m-badge--wide">Opened</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 1){ ?>
                                      <span class="m-badge m-badge--metal m-badge--wide">Quoted</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 2){ ?>
                                      <span class="m-badge m-badge--metal m-badge--wide">Approved</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 3 &&  $wo['scheduled_date'] < $today->format('Y-m-d\TH:i:s.u')){ ?>
                                      <span class="m-badge m-badge--warning m-badge--wide">Scheduled</span>
                                      <script>
                                        delayed.push(<?= $wo['internal_id'] ?>)
                                        localStorage.setItem('delayed', JSON.stringify(delayed))
                                      </script>
                                      <?php } ?>
                                      <?php if($wo['status'] == 3 &&  $wo['scheduled_date'] > $today->format('Y-m-d\TH:i:s.u')){ ?>
                                      <span class="m-badge m-badge--metal m-badge--wide">Scheduled</span>
                                      <?php } ?>

                                      <?php if($wo['status'] == 4){ ?>
                                      <span class="m-badge m-badge--metal m-badge--wide">Assigned</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 5){ ?>
                                      <span class="m-badge m-badge--metal m-badge--wide">Dispatched</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 6){ ?>
                                      <span class="m-badge m-badge--info m-badge--wide">In Progress</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 7){ ?>
                                      <span class="m-badge m-badge--success m-badge--wide">Completed</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 8){ ?>
                                      <span class="m-badge m-badge--danger m-badge--wide">Refused</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 9){ ?>
                                      <span class="m-badge m-badge--danger m-badge--wide">Rejected</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 10){ ?>
                                      <span class="m-badge m-badge--danger m-badge--wide" style="background: #9d6363 !important">Invoiced</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 11){ ?>
                                      <span class="m-badge m-badge--danger m-badge--wide" style="background: #c14a26 !important">Payed</span>
                                      <?php } ?>
                                      <?php if($wo['status'] == 12){ ?>
                                      <span class="m-badge m-badge--info m-badge--wide" style="background: #c14a26 !important">Ready to verify</span>
                                      <?php } ?>
                                    </td>
                                    <td>
                                      <span class="dropdown">
                                        <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                          data-toggle="dropdown" aria-expanded="true">
                                          <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <a class="dropdown-item" href="../tickets/show.php?id=<?= $wo['id'] ?>"><i class="la la-crosshairs"></i>
                                            View</a>
                                          <a class="dropdown-item" href="../tickets/update.php?id=<?= $wo['id'] ?>"><i class="la la-edit"></i>
                                            Update</a>
                                          
                                        </div>
                                      </span>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_wo">Add Work Order</button>
                            </div>
                            <hr>
                            <div>
                              <h3>Timeline</h3>
                              <div id="myTimeline"></div>
                            </div>
                          <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                              <a onclick="updateTicket(event, <?= $_GET['id'] ?>);" class="btn btn-primary">Update</a>
                              <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a>
                            </div>
                          </div>
                        </div>

                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
    <!--END XL12-->
</div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->

<!-- Modal -->
<div class="modal fade" id="modal_add_wo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Work Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
        <div class="form-group m-form__group">
          <label>Customer Work Order#</label>
          <input type="text" class="form-control m-input" id="internal_id" placeholder="Internal ID"  >
        </div>
        <div class="form-group m-form__group">
          <label>Cleaners</label>
          <select class="form-control m-input" id="cleaner_id" name="cleaner_id"  >
            <option value="" selected>Select One</option>
            <?php  usort($cleaners, function($a, $b) {
                    return strtolower($a['first_name']) > strtolower($b['first_name']);
                    
                    });
                    
             foreach ($cleaners as $key => $cleaner) { ?>
                <option value="<?php echo $cleaner['id'] ?>"><?= $cleaner['first_name'].' '.$cleaner['last_name'] ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group m-form__group">
            <label>Request Date</label>
            <input type="date" class="form-control m-input" id="requested_date" placeholder="Request Date"  >
        </div>
        <div class="form-group m-form__group">
            <label>Due Date</label>
            <input type="date" class="form-control m-input" id="due_date" placeholder="Due date" >
        </div>
        <div class="form-group m-form__group">
            <label>Site Contact</label>
            <input type="text" class="form-control m-input" id="site_contact" placeholder="Site Contact" >
        </div>
        <div class="form-group m-form__group">
            <label>Site Phone</label>
            <input type="text" class="form-control m-input" id="site_phone" placeholder="Site Phone" >
        </div>
        <div class="form-group m-form__group">
            <label>Request Contact</label>
            <input type="text" class="form-control m-input" id="request_contact" placeholder="Request Contact" >
        </div>
        <div class="form-group m-form__group">
            <label>Request Contact Email</label>
            <input type="text" class="form-control m-input" id="request_contact_email" placeholder="Site Contact email" >
        </div>
        <div class="form-group m-form__group">
            <label>Request Phone</label>
            <input type="text" class="form-control m-input" id="request_phone" placeholder="Request Phone" >
        </div>
        <div class="form-group m-form__group">
            <label>Task Name</label>
            <input type="text" class="form-control m-input" id="task_name" placeholder="Task Name" >
        </div>
        <div class="form-group m-form__group">
            <label>Task Description</label>
            <textarea class="form-control m-input" id="task_description" placeholder="Task Description" rows="4" cols="50" ></textarea  >
        </div>
        <div class="form-group m-form__group">
            <label>Status</label>
            <select class="form-control m-input" id="status" name="status" >
                <option value="0" selected>Select One</option>
                <option value="0">Opened</option>
                <option value="1">Quoted</option>
                <option value="2">Approved</option>
                <option value="3">Scheduled</option>
                <option value="4">Assigned</option>
                <option value="5">Dispatched</option>
                <option value="6">In Progress</option>
                <option value="12" <?php if($workorder['status'] == 12){ echo "selected"; } ?>>Ready to verify</option>
                <option value="7">Completed</option>
                <option value="8">Refused</option>
                <option value="9">Rejected</option>
                <option value="10">Invoiced</option>
                <option value="11">Payed</option>
            </select>
        </div>
        <div class="form-group m-form__group">
            <label>Types</label>
            <select class="form-control m-input" id="type" name="type" >
                <option value="" selected>Select One</option>
                <option value="emergency">Emergency</option>
                <option value="complain">Complain</option>
                <option value="rush_request">Rush Request</option>
                <option value="request">Request</option>
                <option value="periodics">Periodics</option>
            </select>
        </div>
        <div class="form-group m-form__group">
            <label>Priority</label>
            <select class="form-control m-input" id="priority" name="priority" >     <option value="" selected>Select One</option>
                <option value="1">P1</option>
                <option value="2">P2</option>
                <option value="3">P3</option>
                <option value="4">P4</option>
                <option value="5">P5</option>
            </select>
        </div>
        <div class="form-group m-form__group">
            <label>Fee</label>
            <input type="number" class="form-control m-input" id="fee" placeholder="Fee" >
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="createWorkorder(event, 'tickets');">Create Work Order</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_add_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group m-form__group">
          <label>Comment</label>
          <textarea class="form-control m-input" id="comment"></textarea> 
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="saveComment(event, );">Save</button>
      </div>
    </div>
  </div>
</div>
<?php include('../includes/footer.php');?>

<!--begin::CLIENTS -->
<script src="../assets/js/plugins/cleave.min.js"></script>
<script src="../assets/js/tickets/update.js" type="text/javascript"></script>

<script src="../assets/js/workorders/create.js" type="text/javascript"></script>
<!--end::CLIENTS -->
<script src="../assets/js/plugins/timeline/jquery-albe-timeline.min.js"></script>
<script>
<?php
$wos = json_encode($ticket['work_orders']);
echo "var wos = ". $wos . ";\n";
$comments = json_encode($ticket['comments']);
echo "var comments = ". $comments . ";\n";
?>
var data = [];
for (const [key, value] of Object.entries(wos)) {
  console.log(key, value);
  data.push({
    time: value['created_at'].substring(0,10), 
    header: 'Work Order #'+value['id'],
    body: [{
      tag: 'h3',
      content: value['task_name']
    },
    {
      tag: 'p',
      content: value['task_description']
    }],
  });
  
}
for (const [key, value] of Object.entries(comments)) {
  console.log(key, value);
  data.push({
    time: value['created_at'].substring(0,10), 
    header: 'Comment',
    body: [{
      tag: 'h3',
      content: value['user']['first_name']
    },
    {
      tag: 'p',
      content: value['comment']
    }],
  });
  
}
console.log(data);
// var data = [
//   {
//     time: '2019-03-29',
//     header: 'Sample of header',
//     body: [{
//       tag: 'h1',
//       content: "Lorem ipsum"
//     },
//     {
//       tag: 'p',
//       content: 'Lorem ipsum dolor sit amet, nisl lorem, wisi egestas orci tempus class massa, suscipit eu elit urna in urna, gravida wisi aenean eros massa, cursus quisque leo quisque dui.'
//     }],
//     footer: 'Sample of footer. See <a href=\"https://github.com/Albejr/jquery-albe-timeline\" target=\"_blank\">more details</a>'
//   },
//   {
//     time: '2019-04-15',
//     body: [{
//       tag: 'h1',
//       content: "Basic content"
//     },
//     {
//       tag: 'p',
//       content: 'Lorem ipsum dolor sit amet, nisl lorem, wisi egestas orci tempus class massa, suscipit eu elit urna in urna, gravida wisi aenean eros massa, cursus quisque leo quisque dui.'
//     }],
//   },
//   {
//     time: '2019-01-20',
//     body: [{
//       tag: 'img',
//       attr: {
//         src: '../img/qrcode.png',
//         width: '150px',
//         cssclass: 'img-responsive'
//       }
//     },
//     {
//       tag: 'h2',
//       content: 'Sample with image'
//     },
//     {
//       tag: 'p',
//       content: 'Lorem ipsum dolor sit amet, nisl lorem, wisi egestas orci tempus class massa, suscipit eu elit urna in urna, gravida wisi aenean eros massa, cursus quisque leo quisque dui. See <a href=\"https://github.com/Albejr/jquery-albe-timeline\" target=\"_blank\" class=\"mylink\">more details</a>'
//     }]
//   },
//   {
//     time: '2019-04-20',
//     body: [{
//       tag: 'img',
//       attr: {
//         src: '../img/girl.png',
//         width: '100px',
//         cssclass: 'img-responsive'
//       }
//     },
//     {
//       tag: 'h2',
//       content: 'Sample with image rigth'
//     },
//     {
//       tag: 'p',
//       content: 'Lorem ipsum dolor sit amet, nisl lorem, wisi egestas orci tempus class massa, suscipit eu elit urna in urna, gravida wisi aenean eros massa, cursus quisque leo quisque dui.'
//     }]
//   }
// ];

$(document).ready(function () {

  $("#myTimeline").albeTimeline(data);

});
</script>
<script>
    $("#cleaner_id").chosen({no_results_text: "Oops, nothing found!"}); 
</script>