<?php
include('../includes/header.php');

if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 6) {

    //$sites = vcGetBranchesByCleaner($_SESSION['access-token'], $_SESSION['id']);
    //$all_u = vcGetUsers($_SESSION['access-token']);

?>
<script src="../assets/js/jquery.min.js"></script>
<script type="text/javascript">
    if (navigator.geolocation) {
        var lat = 0;
        var lng = 0;

        navigator.geolocation.getCurrentPosition(function(position) {
        //alert(position.coords.latitude, position.coords.longitude);
            lat = position.coords.latitude;
            lng = position.coords.longitude;
               // First Check Session Status.

            $.ajax({
                type: "POST",
                url: "coords.php",
                //async: false,
                data: {lat:lat, lng:lng}

            }).done(function (data) {
              console.log(data);
              $(".sites_list").html(data);
            });
        },function(error){
          //alert(error);
          console.log(error);
           $.ajax({
                type: "POST",
                url: "coords.php",
                // async: false,
                data: {lat:lat, lng:lng}

            }).done(function (data) {
              console.log(data);
              $(".sites_list").html(data);
            });

        });



    }else{
        console.log('geolocation is not supported');

    }

</script>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php'); ?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Check in/out</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <!-- <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Date
                                  <div>
                                    <div style="float:left; margin-left:10px">
                                      <input type="date" id="date_filter" value="<?= $date_value ?>">
                                      </select>
                                    </div>

                                  </div>
                                  to
                                  <div>
                                    <div style="float:left; margin-left:10px">
                                      <input type="date" id="date_filter_to" value="<?= $date_value2 ?>">
                                      </select>
                                    </div>

                                  </div>
                                </h3>

                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a id="search_by_date" class="btn btn-primary" style="color: #fff !important">
                                            <span>
                                                <i class="la la-search"></i>
                                                <span>Search</span>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div> -->
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Delayed
                                  <div>
                                    <div style="float:left; margin-left:10px">
                                      <input type="checkbox" id="delay_check" onchange="showDaleyed(event)">

                                    </div>

                                  </div>
                                </h3>
                            </div>
                        </div>

                    </div>

                    <div class="m-portlet__body">

                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table table-bordered" id="cleaner_">
                                    <thead>
                                        <tr>
                                            <th width="70px">Site Code</th>
                                            <th>Name</th>
                                            <th>Client</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Zipcode</th>
                                            <th>Time</th>
                                            <th>check-in</th>
                                            <th>check-out</th>

                                        </tr>
                                    </thead>
                                    <tbody class="sites_list">

                                    </tbody>
                                </table>

                            </div>
                        </div>

                        <!--end::Section-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
        <!--END XL12-->
    </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->




<div class="modal fade" id="checkModal" tabindex="-1" role="dialog" aria-labelledby="cleanersModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Check</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        You're checking in to: <br />
        Site: <span class="branch_check_name">site</span>
        <input type="hidden" id="branch_check_name" disabled>
        <input type="hidden" id="branch_check_id" disabled>
        <input type="hidden" id="branch_check_user_id" disabled>
        <input type="hidden" id="branch_check_type" disabled>
        <input type="hidden" id="branch_check_supervisor" disabled value="<?= ($_SESSION['role_id'] == 5) ? '1':'0' ?>">
        <br>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="check(event)">Check</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--begin::BRANCHES -->
<script src="../assets/js/branches/delete.js" type="text/javascript"></script>
<script src="../assets/js/branches/reassigned.js" type="text/javascript"></script>
<script src="../assets/js/branches/rescheduled.js" type="text/javascript"></script>
<script src="../assets/js/branches/check.js" type="text/javascript"></script>
<!--end::BRANCHES -->

<?php include('../includes/footer.php');?>


<?php } ?>
