<?php
include('../includes/header.php');

?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Report no checkins</h3>
          
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      <div class="row">
        <div class="col-xl-12">

          <!--begin::Portlet-->
          <div class="m-portlet" id="m-portlet__body">

            

            <!--end::Form-->
          </div>
          <div id="cont" class="m-portlet">
          
          </div>
          <!--end::Portlet-->
          <!--End::Section-->
        </div>
      </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>
  <!--End::Content-->
  <!-- end:: Page -->

  <!-- begin::Quick Sidebar -->



  <?php include('../includes/footer.php');?>

<script src="../assets/moment.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="../assets/js/reports/index.js"></script>
<script>
  var today = moment().format('DD-MM-YYYY');
  console.log(today);
  document.addEventListener('DOMContentLoaded', getReports('all',0))
</script>