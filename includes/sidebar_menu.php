<!-- BEGIN: Left Aside -->
<button class="noPrint m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark noPrint">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
        m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <?php if ($_SESSION['role_id'] == 1) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                  <a href="/" onclick="saveMenuItem('none')" class="m-menu__link "><i
                            class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Dashboard</span>
                                <!-- <span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span> -->
                            </span></span></a>
                </li>
            <?php } ?>
            <li class="m-menu__section ">
                <h4 class="m-menu__section-text">Menu</h4>
                <i class="m-menu__section-icon flaticon-more-v2"></i>
            </li>

            <?php if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 12) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                  <a href="/cleaners" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-text">Employees</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                  </a>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2) { ?>
                <li class="m-menu__item  m-menu__item--submenu" id="customers_menu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-customer"></i><span class="m-menu__link-text">Customers</span><i
                    class="m-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/clients" class="m-menu__link" onclick="saveMenuItem('customers_active')"><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_link_customers_active">Active</span></a></li>
                        </ul>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/clients/inactive_clients.php" onclick="saveMenuItem('customers_inactive')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_link_customers_inactive">Inactive</span></a></li>
                        </ul>
                    </div>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5) { ?>
                <li class="m-menu__item  m-menu__item--submenu" id="sites_menu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-buildings"></i><span class="m-menu__link-text">Sites</span><i
                    class="m-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/branches" onclick="saveMenuItem('sites_active')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_link_sites_active">Active</span></a></li>
                        </ul>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/branches/inactive_sites.php"  onclick="saveMenuItem('sites_inactive')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_link_sites_inactive">Inactive</span></a></li>
                        </ul>
                        
                    </div>
                </li>
            <?php } ?>

            <li class="m-menu__item  m-menu__item--submenu" id="reports" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-buildings"></i><span class="m-menu__link-text">Reports</span><i
                    class="m-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/reports" onclick="saveMenuItem('reports')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_reports">No checkins</span></a></li>
                        </ul>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/sup_checks"  onclick="saveMenuItem('reports')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_reports">Checkins Supervisor</span></a></li>
                        </ul>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/dur_checks"  onclick="saveMenuItem('reports')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_reports">Checkins Duration</span></a></li>
                        </ul>
                        <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true"><a href="/all_reports"  onclick="saveMenuItem('reports')" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text" id="menu_reports">All Checkins</span></a></li>
                        </ul>
                    </div>
                </li>

            <?php if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 10) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/routes_cleaners" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-map-location"></i><span
                            class="m-menu__link-text">Routes</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5 || $_SESSION['role_id'] == 12) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/service_types" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-list"></i><span
                            class="m-menu__link-text">Service
                            Types</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 10) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/workorders" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file-1"></i><span
                            class="m-menu__link-text">Workorders</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>
            <?php if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 10) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/tickets" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file-1"></i><span
                            class="m-menu__link-text">Tickets</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 5 ) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/route_sites" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-placeholder-3"></i><span
                            class="m-menu__link-text">Check in/out</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>
            <?php if ($_SESSION['role_id'] < 5 || $_SESSION['role_id'] == 10 ) { ?>
            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/checks" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-placeholder-3"></i><span
                            class="m-menu__link-text">Check-in's</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/users" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-user"></i><span
                            class="m-menu__link-text">Users</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>

            <?php if ($_SESSION['role_id'] == 1) { ?>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                        href="/roles" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-lock"></i><span
                            class="m-menu__link-text">Roles</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                </li>
            <?php } ?>


            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a
                    href="/notifications" onclick="saveMenuItem('none')" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fas fa-bell"></i><span
                        class="m-menu__link-text">Notifications</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>


<!--begin::SIDEBAR -->
<script src="../assets/js/sidebar_menu/active_or_inactive.js" type="text/javascript"></script>
<!--end::SIDEBAR -->

<script src="../assets/js/jquery.min.js"></script>

<style>
    .active_menu_item{
        color: white !important;
    }
</style>

<script>
$( document ).ready(function() {
    var menu = localStorage.getItem('menu')

    if(menu == "customers_active" || menu == "customers_inactive"){
        $("#customers_menu").addClass('m-menu__item--open')

        if(menu=="customers_active"){
            $("#menu_link_customers_active").addClass('active_menu_item')
            $("#menu_link_customers_inactive").removeClass('active_menu_item')
            $("#menu_link_sites_inactive").removeClass('active_menu_item')
            $("#menu_link_sites_active").removeClass('active_menu_item')
        }
        if(menu=="customers_inactive"){
            $("#menu_link_customers_inactive").addClass('active_menu_item')
            $("#menu_link_customers_active").removeClass('active_menu_item')
            $("#menu_link_sites_inactive").removeClass('active_menu_item')
            $("#menu_link_sites_active").removeClass('active_menu_item')
        }
    }

    if(menu == "reports" ){
        $("#reports").addClass('m-menu__item--open')

            // $("#reports").addClass('active_menu_item')
            // $("#reports").removeClass('active_menu_item')
            $("#menu_reports").removeClass('active_menu_item')
            $("#menu_reports").removeClass('active_menu_item')
       
    }

    if(menu == "sites_active" || menu == "sites_inactive"){
        $("#sites_menu").addClass('m-menu__item--open')

        if(menu=="sites_active"){
            $("#menu_link_sites_active").addClass('active_menu_item')
            $("#menu_link_sites_inactive").removeClass('active_menu_item')
            $("#menu_link_customers_active").removeClass('active_menu_item')
            $("#menu_link_customers_inactive").removeClass('active_menu_item')
        }
        if(menu=="sites_inactive"){
            $("#menu_link_sites_inactive").addClass('active_menu_item')
            $("#menu_link_sites_active").removeClass('active_menu_item')
            $("#menu_link_customers_active").removeClass('active_menu_item')
            $("#menu_link_customers_inactive").removeClass('active_menu_item')
        }
    }
});
</script>
