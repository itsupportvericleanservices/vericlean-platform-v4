<?php
if(!empty($_POST['email']) && !empty($_POST['password'])) {
  // Include Vericlean Services API Helper
  include('vericlean-api-helper.php');

  // Remember me fix.
  if(empty($_POST['remember_me'])) {
    $_POST['remember_me'] = "off";
  }

  // Login

  if (strpos($_POST['email'], '@') !== false) {
    $post_data = [
      'email' => $_POST['email'],
      'password' => $_POST['password'],
      'remember_me' => $_POST['remember_me']
    ];
  } else {
    $post_data = [
      'username' => $_POST['email'],
      'password' => $_POST['password'],
      'remember_me' => $_POST['remember_me']
    ];
  }
  $login = vcLogin($post_data);

  if (!empty($login['access-token'])) {
    // Start Session
    session_start();

    // Clear any Session
		$_SESSION = [];

    // SET the session params
		$_SESSION['access-token'] = $login['access-token'];
    $_SESSION['client'] = $login['client'];
    $_SESSION['uid'] = $login['uid'];
    $_SESSION['user_email'] = $login['user-info']['email'];
    $_SESSION['first_name'] = $login['user-info']['first_name'];
    $_SESSION['last_name'] = $login['user-info']['last_name'];
    $_SESSION['user_name'] = $login['user-info']['first_name'] . " " . $login['user-info']['last_name'];
    $_SESSION['role_id'] = $login['user-info']['role_id'];
    $_SESSION['company_id'] = $login['user-info']['company_id'];
    $_SESSION['id'] = $login['user-info']['id'];
    //$_SESSION['company_name'] = $login['user-info']['company']['name'];


    // SET the remember me cookie.
		if($_POST['remember_me'] == "on") {
			// Set cookie
			$cookie_expire = time() + 60 * 60 * 24 * 90; // expires in 60 days
			setcookie('access-token', $login['access-token'], $cookie_expire, '/', '.vericleanservices.com');
			setcookie('client', $login['client'], $cookie_expire, '/', '.vericleanservices.com');
			setcookie('uid', $login['uid'], $cookie_expire, '/', '.vericleanservices.com');
			setcookie('cookie_expire', $cookie_expire, $cookie_expire, '/', '.vericleanservices.com');
			// LOCALHOST Cookie fix. ipv4 || ipv6
			if($_SERVER['REMOTE_ADDR'] == "127.0.0.1" || $_SERVER['REMOTE_ADDR'] == "::1") {
				setcookie('access-token', $login['access-token'], $cookie_expire, '/');
        setcookie('client', $login['client'], $cookie_expire, '/');
  			setcookie('uid', $login['uid'], $cookie_expire, '/');
        setcookie('cookie_expire', $cookie_expire, $cookie_expire, '/');
			}
		} else { // Just 1 day cookie.
			// Set cookie
			$cookie_expire = time() + 60 * 60 * 24 * 30; // expires in 1 day
			setcookie('access-token', $login['access-token'], $cookie_expire, '/', '.vericleanservices.com');
      setcookie('client', $login['client'], $cookie_expire, '/', '.vericleanservices.com');
			setcookie('uid', $login['uid'], $cookie_expire, '/', '.vericleanservices.com');
      setcookie('cookie_expire', $cookie_expire, $cookie_expire, '/', '.vericleanservices.com');

			// LOCALHOST Cookie fix. ipv4 || ipv6
			if($_SERVER['REMOTE_ADDR'] == "127.0.0.1" || $_SERVER['REMOTE_ADDR'] == "::1") {
				setcookie('access-token', $login['access-token'], $cookie_expire, '/');
        setcookie('client', $login['client'], $cookie_expire, '/');
  			setcookie('uid', $login['uid'], $cookie_expire, '/');
        setcookie('cookie_expire', $cookie_expire, $cookie_expire, '/');
			}
		} // END if remember_me

    // Redirect to Index
    if ($_SESSION['role_id'] == 6) {
      header('Location: /route_sites/index.php');
    }else{
      header('Location: /index.php');
    }


  } else {
    // Redirect to login
    header('Location: /login.php');
  }
} else {
  // Redirect to login
  header('Location: /login.php');
}
