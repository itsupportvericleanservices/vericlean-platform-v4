<?php
//
// Verciclean Services API PHP Helper Library
// Created by HT Devs
//

// Verciclean Services API URL
if ($_SERVER["SERVER_ADDR"] == "127.0.0.1" || $_SERVER["SERVER_ADDR"] == "::1") {
  $vcApiUrl = "http://localhost:3000/";
} else {
   // $vcApiUrl = "http://52.52.229.187/"; //staging APP
  //  $vcApiUrl = "http://54.183.95.99/"; //staging TEST
  $vcApiUrl = "http://api.vericleanservices.com/";
}

if (!function_exists('vcCall')) {
  // Main Vericlean Services API Call Function
  function vcCall($vcRoute, $token = '', $method = 'GET', $params = array()) {
    $vcCallHeaders[] = "Content-Type: application/json;charset=utf-8";

    if($vcRoute != "auth/sign_in") {
      // JSON and Apikey Headers
      if(!empty($token)) {
        $vcCallHeaders[] = "access-token: ".$_SESSION['access-token'];
        $vcCallHeaders[] = "client: ".$_SESSION['client'];
        $vcCallHeaders[] = "uid: ".$_SESSION['uid'];
      }
    }

    // Call the API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $GLOBALS['vcApiUrl'] . $vcRoute);

    if ($method == 'GET') {
      curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
    } else if ($method == 'DELETE') {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
    } else if ($method == 'PUT') {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    } else {
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $vcCallHeaders);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);

    // Get the response
    $response = curl_exec($ch);
    $response_info = curl_getinfo($ch);

    // Close cURL connection
    curl_close($ch);

    // If success status 200
    if($response_info['http_code'] == 200 || $response_info['http_code'] == 500 || $response_info['http_code'] == 422) {
      list($headers, $content) = explode("\r\n\r\n", $response, 2);

      // Read Headers
      $response_headers =  explode("\r\n", $headers);
      foreach ($response_headers as $response_header) {
        $response_header = explode(":", $response_header);
        if(trim($response_header[0]) == "access-token") {
          $access_token = trim($response_header[1]);
        }
        if(trim($response_header[0]) == "client") {
          $client = trim($response_header[1]);
        }
        if(trim($response_header[0]) == "uid") {
          $uid = trim($response_header[1]);
        }
      }

      // Decode the response (Transform it to an Array)
      $response = json_decode($content, true);

      if($vcRoute == "auth/sign_in") {
        $response = $response['data'];
        $response_arr = [
          "access-token" => $access_token,
          "client" => $client,
          "uid" => $uid,
          "user-info" => $response
        ];
        $response = $response_arr;
      }

      // Return response
      return $response;
    } else {
      // If token expired, not exists, or user should logout
      if ($response_info['http_code'] == 403 || $response_info['http_code'] == 401) {
        echo '<script type="text/javascript">
             window.location = "/includes/logout.php?error=token_expired"
        </script>';
        die();
      }
    }

  }
}

//
// Current API Version and Info
//
function vc() {
	// Make the Call
	$response = vcCall();

	// Return Response
	return $response;
}

/* ===============================
            AUTH
================================*/
function vcLogin($post_data) {
	// Make the Call
	$response = vcCall("auth/sign_in", "", 'POST', $post_data);

	// Return Response
	return $response;
}

/* =====================
        USERS
========================*/

// Get Users
function vcGetUsers($token) {
  // Make the Call
  $response = vcCall("users/", $token);
  // Return Response
  return $response;
}

// Get User
function vcGetUser($token, $id) {
  // Make the Call
  $response = vcCall("users/$id", $token);
  // Return Response
  return $response;
}

// Create User
function vcCreateUser($token, $data) {
  // Make the Call
  $response = vcCall("users/", $token, "POST", $data);
  // Return Response
  return $response;
}

// Update User
function vcUpdateUser($token, $id, $json) {
  // Make the Call
  $response = vcCall("users/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete User
function vcDeleteUser($token, $id) {
  // Make the Call
  $response = vcCall("users/$id", $token, 'DELETE');
  // Return Response
  return $response;
}
function vcGetCleanersByFrecuency($token, $f, $date){
  $response = vcCall("users/$f/$date/getbyfrecuency", $token);
  // Return Response
  return $response;
}

/* =====================
        CLEANERS
========================*/

// Get Cleaners
function vcGetCleaners($token) {
  // Make the Call
  $response = vcCall("cleaners/", $token);
  // Return Response
  return $response;
}

// Get Cleaner
function vcGetCleaner($token, $id) {
  // Make the Call
  $response = vcCall("cleaners/$id", $token);
  // Return Response
  return $response;
}

// Create Cleaner
function vcCreateCleaner($token, $data) {
  // Make the Call
  $response = vcCall("cleaners/", $token, "POST", $data);
  // Return Response
  return $response;
}

// Update Cleaner
function vcUpdateCleaner($token, $id, $json) {
  // Make the Call
  $response = vcCall("cleaners/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Cleaner
function vcDeleteCleaner($token, $id) {
  // Make the Call
  $response = vcCall("cleaners/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/* =====================
      AREA MANAGERS
========================*/

// Get AreaManagers
function vcGetAreaManagers($token) {
  // Make the Call
  $response = vcCall("area_managers/", $token);
  // Return Response
  return $response;
}

// Get AreaManager
function vcGetAreaManager($token, $id) {
  // Make the Call
  $response = vcCall("area_managers/$id", $token);
  // Return Response
  return $response;
}

// Create AreaManager
function vcCreateAreaManager($token, $data) {
  // Make the Call
  $response = vcCall("area_managers/", $token, "POST", $data);
  // Return Response
  return $response;
}

// Update AreaManager
function vcUpdateAreaManager($token, $id, $json) {
  // Make the Call
  $response = vcCall("area_managers/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete AreaManager
function vcDeleteAreaManager($token, $id) {
  // Make the Call
  $response = vcCall("area_managers/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/* =====================
      SUPERVISORS
========================*/

// Get Supervisors
function vcGetSupervisors($token) {
  // Make the Call
  $response = vcCall("supervisors/", $token);
  // Return Response
  return $response;
}

// Get Supervisor
function vcGetSupervisor($token, $id) {
  // Make the Call
  $response = vcCall("supervisors/$id", $token);
  // Return Response
  return $response;
}

// Create Supervisor
function vcCreateSupervisor($token, $data) {
  // Make the Call
  $response = vcCall("supervisors/", $token, "POST", $data);
  // Return Response
  return $response;
}

// Update Supervisor
function vcUpdateSupervisor($token, $id, $json) {
  // Make the Call
  $response = vcCall("supervisors/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Supervisor
function vcDeleteSupervisor($token, $id) {
  // Make the Call
  $response = vcCall("supervisors/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/* =====================
        ROLES
========================*/

// Get Roles
function vcGetRoles($token) {
  // Make the Call
  $response = vcCall("roles/", $token);
  // Return Response
  return $response;
}

// Get Role
function vcGetRole($token, $id) {
  // Make the Call
  $response = vcCall("roles/$id", $token);
  // Return Response
  return $response;
}

// Create Role
function vcCreateRole($token, $data) {
  // Make the Call
  $response = vcCall("roles/", $token, "POST", $data);
  // Return Response
  return $response;
}

// Update Role
function vcUpdateRole($token, $id, $json) {
  // Make the Call
  $response = vcCall("roles/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Role
function vcDeleteRole($token, $id) {
  // Make the Call
  $response = vcCall("roles/$id", $token, 'DELETE');
  // Return Response
  return $response;
}


/* =====================
        CLIENTS
========================*/

// Get Clients
function vcGetClients($token) {
  // Make the Call
  $response = vcCall("clients/", $token);
  // Return Response
  return $response;
}

// Get Active Clients
function vcGetActiveClients($token) {
  // Make the Call
  $response = vcCall("clients/active", $token);
  // Return Response
  return $response;
}

// Get Inactive Clients
function vcGetInactiveClients($token) {
  // Make the Call
  $response = vcCall("clients/inactive", $token);
  // Return Response
  return $response;
}

// Get Sites for Client
function vcGetSitesForClient($token, $id) {
  // Make the Call
  $response = vcCall("clients/sites/$id", $token);
  // Return Response
  return $response;
}

// Get Client
function vcGetClient($token, $id) {
  // Make the Call
  $response = vcCall("clients/$id", $token);
  // Return Response
  return $response;
}

// Create Client
function vcCreateClient($token, $data) {
  // Make the Call
  $response = vcCall("clients/", $token, "POST", $data);
  // Return Response
  return $response;
}

// Update Client
function vcUpdateClient($token, $id, $json) {
  // Make the Call
  $response = vcCall("clients/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Client
function vcDeleteClient($token, $id) {
  // Make the Call
  $response = vcCall("clients/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

// Get Direct Clients for Status
function vcGetDirectClientsForStatus($token, $status) {
  // Make the Call
  $response = vcCall("clients/filter/$status", $token);

  // Return Response
  return $response;
}

// Get Direct Clients for Status
function vcGetDirectClientsForStatusSites($token, $status) {
  // Make the Call
  $response = vcCall("direct_client", $token);

  // Return Response
  return $response;
}

// Get Indirect Clients for Status
function vcGetIndirectClientsForStatusSites($token, $status) {
  // Make the Call
  $response = vcCall("indirect_client", $token);

  // Return Response
  return $response;
}

// Get Indirect Clients for Status
function vcGetIndirectClientsForStatus($token, $status) {
  // Make the Call
  $response = vcCall("clients/customers/filter/$status", $token);

  // Return Response
  return $response;
}

// Get Customers For Client Filter
function vcGetCustomersForClientFilter($token, $client_id, $active) {
  // Make the Call
  $response = vcCall("clients/customers/filter/$client_id/$active", $token);
  // Return Response

  return $response;
}

// Get Clients Filter
function vcGetClientFilter($token, $id) {
  // Make the Call
  $response = vcCall("clients/client_or_customer/$id", $token);

  // Return Response
  return $response;
}

//type = client or customer / indirect = 0 or indirect = 1
function vcGetAllClientsOrCustomers($token, $indirect) {
  // Make the Call
  $response = vcCall("clients/all/$indirect", $token);

  // Return Response
  return $response;
}


/* =====================
        BRANCHES
========================*/

// Get Branches
function vcGetBranches($token) {
  // Make the Call
  $response = vcCall("branches/", $token);
  // Return Response
  return $response;
}

// Get Active Branches with Page
function vcGetBranchesWithPages($token, $page, $searchstr = "") {
  if(!empty($searchstr)) {
    // Make the Call
    $response = vcCall("/branches/paginate/$page?$searchstr", $token);
  } else {
    // Make the Call
    $response = vcCall("/branches/paginate/$page", $token);
  }

  // Return Response
  return $response;
}

// Get Inactive Branches with Page
function vcGetBranchesInactiveWithPages($token, $page, $searchstr = "") {
  if(!empty($searchstr)) {
    // Make the Call
    $response = vcCall("/branches/inactive/paginate/$page?$searchstr", $token);
  } else {
    // Make the Call
    $response = vcCall("/branches/inactive/paginate/$page", $token);
  }

  // Return Response
  return $response;
}


// Get Branches States
function vcGetBranchesStates($token) {
  // Make the Call
  $response = vcCall("branches/states", $token);
  // Return Response
  return $response;
}

// Get Branches Cities
function vcGetBranchesCities($token) {
  // Make the Call
  $response = vcCall("branches/cities", $token);
  // Return Response
  return $response;
}

// Get Branch
function vcGetBranch($token, $id) {
  // Make the Call
  $response = vcCall("branches/$id", $token);
  // Return Response
  return $response;
}

// Create Branch
function vcCreateBranches($token, $json) {
  // Make the Call
  $response = vcCall("branches/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update Branch
function vcUpdateBranch($token, $id, $json) {
  // Make the Call
  $response = vcCall("branches/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Branch
function vcDeleteBranch($token, $id) {
  // Make the Call
  $response = vcCall("branches/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

// Get Active Sites
function vcGetActiveBranches($token) {
  // Make the Call
  $response = vcCall("branches/active", $token);
  // Return Response
  return $response;
}

function vcGetAllActiveBranches($token) {
  // Make the Call
  $response = vcCall("export_active_sites", $token);
  // Return Response
  return $response;
}
// add_service_types_to_branches
function vcAddServiceTypesToBranches($token, $json) {
  // Make the Call
  $response = vcCall("add_service_types_to_branches/", $token, "POST", $json);
  // Return Response
  return $response;
}
function vcGetBranchesByCleaner($token, $id, $lat, $lng){
  // Make the Call
  $response = vcCall("branches/$id/$lat/$lng/getbycleanerinclude", $token);
  // Return Response
  return $response;
}
function vcGetBranchesBySupervisor($token, $id, $lat, $lng){
  // Make the Call
  $response = vcCall("branches/$id/$lat/$lng/getbysupervisorinclude", $token);
  // Return Response
  return $response;
}

// Get Branches List Request Update Location
function vcGetBranchesListRequest($token) {
  // Make the Call
  $response = vcCall("showRequestList/", $token);
  // Return Response
  return $response;
}
/* =====================
        CHECKS
========================*/
// Get checks with Page
function vcGetChecks($token, $page, $searchstr = "", $from, $to, $print=0) {
  if(!empty($searchstr)) {
    // Make the Call
    $response = vcCall("route_histories/$print/$from/$to/paginate/$page?$searchstr", $token);
  } else {
    // Make the Call
    $response = vcCall("route_histories/$print/$from/$to/paginate/$page", $token);
  }

  // Return Response
  return $response;
}

/* =====================
        ROUTES
========================*/

// Get Routes
function vcGetRoutes($token) {
  // Make the Call
  $response = vcCall("routes/", $token);
  // Return Response
  return $response;
}

// Get Routes
function vcGetRoute($token, $id) {
  // Make the Call
  $response = vcCall("routes/$id", $token);
  // Return Response
  return $response;
}

// Create Routes
function vcCreateRoute($token, $json) {
  // Make the Call
  $response = vcCall("routes/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update Routes
function vcUpdateRoute($token, $id, $json) {
  // Make the Call
  $response = vcCall("routes/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Routes
function vcDeleteRoute($token, $id) {
  // Make the Call
  $response = vcCall("routes/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/* ========================
      ROUTES HISTORIES
===========================*/
// Create checkin
function vcCreateRouteHistory($token, $json) {
  // Make the Call
  $response = vcCall("route_histories/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Get checkin
function vcGetRouteHistory($token, $id) {
  // Make the Call
  $response = vcCall("route_histories/$id", $token);
  // Return Response
  return $response;
}

// Update checkout
function vcUpdateRouteHistory($token, $id, $json) {
  // Make the Call
  $response = vcCall("route_histories/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}
// Reassigned
function vcReassignedRouteHistories($token, $json) {
  // Make the Call
  $response = vcCall("route_histories/reassigned/", $token, "POST", $json);
  // Return Response
  return $response;
}

//Rescheduled
function vcRescheduledRouteHistories($token, $json) {
  // Make the Call
  $response = vcCall("route_histories/rescheduled/", $token, "POST", $json);
  // Return Response
  return $response;
}

/* =====================
      FREQUENCIES
========================*/

// Get Frequencies
function vcGetFrequencies($token) {
  // Make the Call
  $response = vcCall("frequencies/", $token);
  // Return Response
  return $response;
}

// Get Frequency
function vcGetFrequency($token, $id) {
  // Make the Call
  $response = vcCall("frequencies/$id", $token);
  // Return Response
  return $response;
}

// Create Frequency
function vcCreateFrequency($token, $json) {
  // Make the Call
  $response = vcCall("frequencies/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update Frequency
function vcUpdateFrequency($token, $id, $json) {
  // Make the Call
  $response = vcCall("frequencies/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Frequency
function vcDeleteFrequency($token, $id) {
  // Make the Call
  $response = vcCall("frequencies/$id", $token, 'DELETE');
  // Return Response
  return $response;
}


/* =====================
      SERVICE_TYPES
========================*/

// Get ServiceTypes
function vcGetServiceTypes($token) {
  // Make the Call
  $response = vcCall("service_types/", $token);
  // Return Response
  return $response;
}

// Get ServiceType
function vcGetServiceType($token, $id) {
  // Make the Call
  $response = vcCall("service_types/$id", $token);
  // Return Response
  return $response;
}

// Create ServiceType
function vcCreateServiceType($token, $json) {
  // Make the Call
  $response = vcCall("service_types/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update ServiceType
function vcUpdateServiceType($token, $id, $json) {
  // Make the Call
  $response = vcCall("service_types/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete ServiceType
function vcDeleteServiceType($token, $id) {
  // Make the Call
  $response = vcCall("service_types/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/* =====================
      SERVICES
========================*/

// Get Services
function vcGetServices($token) {
  // Make the Call
  $response = vcCall("services/", $token);
  // Return Response
  return $response;
}

// Get ServiceType
function vcGetService($token, $id) {
  // Make the Call
  $response = vcCall("services/$id", $token);
  // Return Response
  return $response;
}

// Create Service
function vcCreateService($token, $json) {
  // Make the Call
  $response = vcCall("services/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update Service
function vcUpdateService($token, $id, $json) {
  // Make the Call
  $response = vcCall("services/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Service
function vcDeleteService($token, $id) {
  // Make the Call
  $response = vcCall("services/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/*==========================
      SERVICES - COMMENTS
===========================*/
// Get Comments
function vcGetComments($token) {
  // Make the Call
  $response = vcCall("comments/", $token);
  // Return Response
  return $response;
}

// Get Comments
function vcGetComment($token, $id) {
  // Make the Call
  $response = vcCall("comments/$id", $token);
  // Return Response
  return $response;
}

// Create Comment
function vcCreateComment($token, $json) {
  // Make the Call
  $response = vcCall("comments/", $token, "POST", $json);
  // Return Response
  return $response;
}



/* =====================
    ROUTES SCHEDULES
========================*/

// Get RouteSchedules
function vcGetRouteSchedules($token) {
  // Make the Call
  $response = vcCall("route_schedules/", $token);
  // Return Response
  return $response;
}

// Get RouteSchedule
function vcGetRouteSchedule($token, $id) {
  // Make the Call
  $response = vcCall("route_schedules/$id", $token);
  // Return Response
  return $response;
}

// Create RouteSchedule
function vcCreateRouteSchedule($token, $json) {
  // Make the Call
  $response = vcCall("route_schedules/", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update RouteSchedule
function vcUpdateRouteSchedule($token, $id, $json) {
  // Make the Call
  $response = vcCall("route_schedules/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete RouteSchedule
function vcDeleteRouteSchedule($token, $id) {
  // Make the Call
  $response = vcCall("route_schedules/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

/* =====================
    PHOTOS
========================*/

// Get Photos
function vcGetPhotos($token) {
  // Make the Call
  $response = vcCall("photos/", $token);
  // Return Response
  return $response;
}

// Get Photo
function vcGetPhoto($token, $id) {
  // Make the Call
  $response = vcCall("photos/$id", $token);
  // Return Response
  return $response;
}

/* ===========================
          ROUTES HISTORY
===============================*/

// Get RouteHistories
function vcGetRouteHistories($token) {
  // Make the Call
  $response = vcCall("route_histories/", $token);
  // Return Response
  return $response;
}


/* =====================================
      FILTERS - SITES WITH FREQUENCY
=======================================*/
function vcGetSitesWhitFrequency($token ,$route_schedule_id,$filter){
  $response = vcCall("route_schedules/$route_schedule_id/sites?day=$filter", $token);

  return $response;
}
function vcGetSitesWhitFilter($token ,$route_schedule_id,$filter){
  $response = vcCall("route_schedules/$route_schedule_id/history?filter_by=$filter", $token);

  return $response;
}



/* ===========================
        WORKORDERS
===============================*/

// Get Workorders
function vcGetWorkorders($token) {
  // Make the Call
  $response = vcCall("work_orders/", $token);
  // Return Response
  return $response;
}

// Get Workorders with Page
function vcGetWorkordersWithPages($token, $page, $searchstr = "") {
  if(!empty($searchstr)) {
    // Make the Call
    $response = vcCall("/work_orders/paginate/$page?$searchstr", $token);
  } else {
    // Make the Call
    $response = vcCall("/work_orders/paginate/$page", $token);
  }

  // Return Response
  return $response;
}

// Get Workorder
function vcGetWorkorder($token, $id) {
  // Make the Call
  $response = vcCall("work_orders/$id", $token);
  // Return Response
  return $response;
}
// Get Workorder files
function vcGetWorkorderfiles($token, $id) {
  // Make the Call
  $response = vcCall("work_orders/$id/getfiles", $token);
  // Return Response
  return $response;
}

// Create Workorder
function vcCreateWorkorder($token, $json) {
  // Make the Call
  $response = vcCall("work_orders/", $token, "POST", $json);
  // Return Response
  return $response;
}

// send email Workorder
function vcSendEmail($token, $json) {
  // Make the Call
  $response = vcCall("work_orders/email", $token, "POST", $json);
  // Return Response
  return $response;
}

// Update Workorder
function vcUpdateWorkorder($token, $id, $json) {
  // Make the Call
  $response = vcCall("work_orders/$id", $token, 'PUT', $json);
  // Return Response
  return $response;
}

// Delete Workorder
function vcDeleteWorkorder($token, $id) {
  // Make the Call
  $response = vcCall("work_orders/$id", $token, 'DELETE');
  // Return Response
  return $response;
}

function vcGetWOForUser($token, $user){
  // Make the Call
  $response = vcCall("getWOForUser/$user", $token);
  // Return Response
  return $response;
}

//NOTIFICATIONS

function getUsersToday(){
  // Make the Call
  $response = vcCall("/users_today");
  // Return Response
  return $response;
}

function getNotificationDelayed($id){
  // Make the Call
  $response = vcCall("branches/$id/notification_delayed");
  // Return Response
  return $response;
}


function getNotificationAfter($id){
  // Make the Call
  $response = vcCall("branches/$id/notification_after");
  // Return Response
  return $response;
}


function getNotificationAfterDuration($id){
  // Make the Call
  $response = vcCall("branches/$id/notification_after_duration");
  // Return Response
  return $response;
}

function createNotification($json) {
  // Make the Call
  $response = vcCall("notifications/", "POST", $json);
  // Return Response
  return $response;
}

function getReportNoCheckins($token,$by,$id,$start,$finished) {
  // Make the Call
  $response = vcCall("no_chekin/$by/$id/$start/$finished", $token);
  // Return Response
  return $response;
}

function getChecksSup($token,$id,$start,$finished) {
  // Make the Call
  $response = vcCall("by_supervisor/$id/$start/$finished", $token);
  // Return Response
  return $response;
}

function getChecksClnr($token,$id,$branch,$by,$start,$finished) {
  // Make the Call
  $response = vcCall("by_cleaner/$id/$branch/$by/$start/$finished", $token);
  // Return Response
  return $response;
}

function getReportAllCheckins($token,$by,$id,$start,$finished) {
  // Make the Call
  $response = vcCall("by_all_check/$by/$id/$start/$finished", $token);
  // Return Response
  return $response;
}
/***************************************/
// Get all
function htdevsGet($token, $model) {
  // Make the Call
  $response = vcCall("/".$model, $token);
  // Return Response
  return $response;
}
// Get single
function htdevsGetSingle($token, $model, $id) {
  // Make the Call
  $response = vcCall("/".$model."/".$id, $token);
  // Return Response
  return $response;
}
// Create 
function htdevsCreate($token, $model, $post_data) {
  // Make the Call
  $response = vcCall("/".$model, $token, 'POST', $post_data);
  // Return Response
  return $response;
}

// update 
function htdevsUpdate($token, $model, $id, $post_data) {
  // Make the Call
  $response = vcCall("/".$model."/".$id, $token, 'PUT', $post_data);
  // Return Response
  return $response;
}

// Get cutom
function htdevsCustom($token, $route) {
  // Make the Call
  $response = vcCall($route, $token);
  // Return Response
  return $response;
}
// Get cutom
function htdevsCustomPost($token, $route,$post_data) {
  // Make the Call
  $response = vcCall($route, $token, 'POST',$post_data);

  // Return Response
  return $response;
}
// Get cutom
function htdevsDelete($token, $model, $id) {
  // Make the Call
  $response = vcCall($model."/".$id, $token, 'DELETE');
  // Return Response
  return $response;
}
?>
