<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<script src="../assets/js/jquery.min.js"></script>

<script src="../assets/js/vericlean-functions.js?v0481"></script>

<script src="../assets/js/popper.min.js"></script>
<!--begin::Global Theme Bundle -->
<script src="../assets/js/vendors.bundle.js" type="text/javascript"></script>
<script src="../assets/js/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<script src="../assets/js/sweetalert.min.js"></script>
<script src="../assets/js/modal-loading.js"></script>
<script src="../assets/js/jquery.timepicker.min.js"></script>
<script src="../assets/js/plugins/lightbox.min.js"></script>
<script src="../assets/chosen.jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $('input.timepicker').timepicker({});
  });
</script>
