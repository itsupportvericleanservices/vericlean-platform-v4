
const usuarios = null

fetch("http://52.52.229.187/users_today",{
  method: "GET",
  headers: {"Content-Type": "application/json"}
})
  .then(res => res.json())
    .then(users => {
      this.usuarios = users
      console.log(this.usuarios)
      this.usuarios.forEach(user => {
          
          fetch(`http://52.52.229.187/branches/${user.id}/notification_delayed`, {
            method: "GET",
            headers: {"Content-Type": "application/json"}
          })
          .then(res => res.json())
            .then(data => {
              console.log(user.id);
              if (data != null) {
                if (data.status == "notification") {
                  console.log(data.cleaner[0]);
                  cleaner = data.cleaner[0]
                  area_manager = data.area_manager[0]
                  supervisor = data.supervisor[0]
                  
                    autoSendPush(cleaner.device_token,"you have a pending route",cleaner.id)
                    autoSendPush(area_manager.device_token,`user ${cleaner.first_name} has not yet started his route`,area_manager.id)
                    autoSendPush(supervisor.device_token,`user ${cleaner.first_name} has not yet started his route`,supervisor.id)
                  
                }else{
                  console.log("no tienen nada");
                }
              } else {
                console.log("no ha nada");
              }

            })

            //repuesta la pasar 30 min de el ultimo chek-out
          fetch(`http://52.52.229.187/branches/${user.id}/notification_after`, {
              method: "GET",
              headers: {"Content-Type": "application/json"}
          })
          .then(res => res.json())
            .then(data => {
                  
                  if (data != null) {
                    if (data.status == "notification") {
                      console.log(data.cleaner[0]);
                      cleaner = data.cleaner[0]
                      area_manager = data.area_manager[0]
                      supervisor = data.supervisor[0]
                      
                        autoSendPush(cleaner.device_token,"you have a pending route",cleaner.id)
                        autoSendPush(area_manager.device_token,`user ${cleaner.first_name} has not yet started his route`,area_manager.id)
                        autoSendPush(supervisor.device_token,`user ${cleaner.first_name} has not yet started his route`,supervisor.id)
                      
                    }else{
                      console.log("no tienen nada");
                    }
                  } else {
                    console.log("no ha nada");
                  }

            })


          //repuesta la pasar el tiempo establesido como maxima duracion de el ultimo chek-in 
          fetch(`http://52.52.229.187/branches/${user.id}/notification_after_duration`, {
              method: "GET",
              headers: {"Content-Type": "application/json"}
          })
          .then(res => res.json())
            .then(data => {
                  
                  if (data != null) {
                    if (data.status == "notification") {
                      console.log(data.cleaner[0]);
                      cleaner = data.cleaner[0]
                      area_manager = data.area_manager[0]
                      supervisor = data.supervisor[0]
                      
                        autoSendPush(cleaner.device_token,"you have a pending route",cleaner.id)
                        autoSendPush(area_manager.device_token,`user ${cleaner.first_name} has not yet started his route`,area_manager.id)
                        autoSendPush(supervisor.device_token,`user ${cleaner.first_name} has not yet started his route`,supervisor.id)
                      
                    }else{
                      console.log("no tienen nada");
                    }
                  } else {
                    console.log("no ha nada");
                  }
          })
        
      })
    })
    
    function autoSendPush(token, message,id){
      var key = 'AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q';
      var to = token;
      var notification = {
        'title': 'Vericlean',
        'body': message
      };

      fetch('https://fcm.googleapis.com/fcm/send', {
        'method': 'POST',
        'headers': {
          'Authorization': 'key=AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q',
          'Content-Type': 'application/json'
        },
        'body': JSON.stringify({
          'data': notification,
          'to': to
        })
      }) 
      .then(res => res.json())
      .then(data => {
        let date = new Date(Date.now())
        fetch('http://52.52.229.187/notifications', {
          'method': 'POST',
          'headers': {
          'Content-Type': 'application/json'
          },
          'body': JSON.stringify({
            'user_id': id,
            'message': message,
            'device':token,
            'send_date':date
          })
        }).then(el=>{
          console.log(el)
        })            
        
      })  
    }
