<?php
//
// API Config.
//
date_default_timezone_set('America/Chicago');
// Verciclean Services API URL
if ($_SERVER["SERVER_ADDR"] == "127.0.0.1" || $_SERVER["SERVER_ADDR"] == "::1") {
  $vcApiUrl = "http://localhost:3000/";
} else {
  $vcApiUrl = "http://54.177.96.202/";
}

// Main vcCall (API) Call.
function vcCall($vcRoute, $token = '', $method = 'GET', $params = array()) {
  $vcCallHeaders[] = "Content-Type: application/json;charset=utf-8";

  // Call the API
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $GLOBALS['vcApiUrl'] . $vcRoute);

  if ($method == 'GET') {
    curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
  } else if ($method == 'DELETE') {
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
  } else if ($method == 'PUT') {
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
  } else {
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
  }
  curl_setopt($ch, CURLOPT_HTTPHEADER, $vcCallHeaders);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // Get the response
  $response = curl_exec($ch);

  // Close cURL connection
  curl_close($ch);

  // Return response
  return $response;
}

function FirebaseSendNotification($user, $message) {

  $msg = array(
    'body' => $message, 
    'title' => 'Vericlean'
  );

  $fields = array(
    'to'  => $user['device_token'], 
    'notification' => $msg, 
    'priority' => 'high'
  );

  $headers = array('Authorization: key=AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q','Content-Type: application/json');
  $ch = curl_init();
  curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
  curl_setopt( $ch,CURLOPT_POST, true );
  curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );

  return $result;
}

// 
// Start Cron.
//

// Cron FIX
if(!empty($argv[1])) {
  parse_str($argv[1], $_GET);
}

// Get params.
$allow_send = false;
if(!empty($_GET['allow_send'])) {
  $allow_send = $_GET['allow_send'];
}

// Start the counter.
$sent_notifications = 0;

// Get all the users working today.
$users_ids = vcCall('/users_today');

foreach(json_decode($users_ids, true) as $user_id) {
  // Chek if the user have notifications.
  $user_notifications = vcCall('notifications/delayed/'. $user_id);
  $user_notifications = json_decode($user_notifications, true);

  // If pending delayed notification
  if ($user_notifications['status'] == 'notification') {  
    $cleaner = $user_notifications['cleaner'][0];
    $area_manager = $user_notifications['area_manager'][0];
    $supervisor = $user_notifications['supervisor'][0];
    echo "Notificacion para: (" . $cleaner['id'] . ") " . $cleaner['first_name'] . " " . $cleaner['last_name'] . " Device: " . substr($cleaner['device_token'], 0, 10) . "<br><hr></br>";

    if($allow_send == true) {
      if(!empty($cleaner['device_token'])) {
        $message = 'Hello '.$cleaner['first_name'].' '.$cleaner['last_name'].', you have a pending route.';
        $notification = FirebaseSendNotification($cleaner, $message);
        $notification_decoded = json_decode($notification, true);
        $notification_params = [
          "user_id" => $cleaner['id'],
          "success" => $notification_decoded['success'],
          "message" => $message,
          "sent_at" => date("Y-m-d H:i:s"),
          "device_token" => $cleaner['device_token'],
          "full_response" => $notification,
        ];
        vcCall('/notifications', '', 'POST', $notification_params);

        // Add to the counter
        $sent_notifications++;
      }

      if(!empty($supervisor['device_token'])) {
        $message = 'Cleaner '.$cleaner['first_name'].' '.$cleaner['last_name'].' has not yet started his route.';
        $notification = FirebaseSendNotification($supervisor, $message);
        $notification_decoded = json_decode($notification, true);
        $notification_params = [
          "user_id" => $supervisor['id'],
          "success" => $notification_decoded['success'],
          "message" => $message,
          "sent_at" => date("Y-m-d H:i:s"),
          "device_token" => $supervisor['device_token'],
          "full_response" => $notification,
        ];
        vcCall('/notifications', '', 'POST', $notification_params);

        // Add to the counter
        $sent_notifications++;
      }

      if(!empty($area_manager['device_token'])) {
        $message = 'Supervisor: '.$supervisor['first_name'].' '.$supervisor['last_name'].', Cleaner: '.$cleaner['first_name'].' '.$cleaner['last_name'].' has not yet started his route.';
        FirebaseSendNotification($area_manager, $message);
        $notification_decoded = json_decode($notification, true);
        $notification_params = [
          "user_id" => $area_manager['id'],
          "success" => $notification_decoded['success'],
          "message" => $message,
          "sent_at" => date("Y-m-d H:i:s"),
          "device_token" => $area_manager['device_token'],
          "full_response" => $notification,
        ];
        vcCall('/notifications', '', 'POST', $notification_params);

        if($area_manager['id'] == 778) {
          $ada_token = 'c8SbBUWUdFw:APA91bFmQ0lwlLF397lKO_3udJpd6o5wVwt7ajCQz3cnxT5FHqHHZQnIBnnAmCAUfLVsEi0EJGHt1CY6sUnisjlpoaBBRzBXjp0xabgk549zbGtiWDpyBpPEOcY8YAwVMmAqKieGem0N';
          $area_manager = [
            'id' => 776,
            'device_token' => $ada_token
          ];
          FirebaseSendNotification($area_manager, $message);
          $notification_params = [
            "user_id" => 776,
            "success" => $notification_decoded['success'],
            "message" => $message,
            "sent_at" => date("Y-m-d H:i:s"),
            "device_token" => $ada_token,
            "full_response" => $notification,
          ];
          vcCall('/notifications', '', 'POST', $notification_params);
        }

        // Add to the counter
        $sent_notifications++;
      }
    }
  }

  // $notUsersAfter = getNotificationAfter($user['id']);
  // if (is_array($notUsersAfter)){
  //   if ($notUsersAfter['status'] == 'notification'){
  //     $cleaner = $notUsersAfter['cleaner'][0];
  //     $area_manager = $notUsersAfter['area_manager'][0];
  //     $supervisor = $notUsersAfter['supervisor'][0];
  //     FirebaseSendNotification($cleaner, 'You have a pending route');
  //     FirebaseSendNotification($area_manager, 'User '.$cleaner['first_name'].' has not yet started his route');
  //     FirebaseSendNotification($supervisor, 'User '.$cleaner['first_name'].' has not yet started h6is route');
  //   }
  // }

  // $notUsersAfterDuration = getNotificationAfterDuration($user['id']); //array
  // if (is_array(notUsersAfterDuration)){
  //   if (notUsersAfterDuration['status'] == 'notification'){
  //     $cleaner = notUsersAfterDuration['cleaner'][0];
  //     $area_manager = notUsersAfterDuration['area_manager'][0];
  //     $supervisor = notUsersAfterDuration['supervisor'][0];
  //     FirebaseSendNotification($cleaner, 'You have a pending route');
  //     FirebaseSendNotification($area_manager, 'User '.$cleaner['first_name'].' has not yet started his route');
  //     FirebaseSendNotification($supervisor, 'User '.$cleaner['first_name'].' has not yet started h6is route');
  //   }
  // }
}
  

  
if($allow_send == true) {
  // Send Alert to Slack
  $msg = "VeriClean Services CRON Executed, $sent_notifications notifications has been sent!";
  $url = "https://hooks.slack.com/services/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";
  $useragent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
  $payload = 'payload={"channel": "#services-monitoring", "username": "htdevs-services-bot", "text": "'.$msg.'", "icon_emoji": ":robot_face:"}';
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
  
  curl_exec($ch);
  curl_close($ch);
}
?>