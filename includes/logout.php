<?php
function delete_cookies() {
	if(isset($_SERVER['HTTP_COOKIE'])){

		$cookies = explode(';', $_SERVER['HTTP_COOKIE']);

		foreach($cookies as $cookie) {
			$parts = explode('=', $cookie);
			$name = trim($parts[0]);
			setcookie($name, '', time()-100000);
			setcookie($name, '', time()-100000, '/','.vericleanservices.com');
			setcookie($name, '', time()-100000, '/d/','.vericleanservices.com');
		}
	}
	return true;
}
// Start current session
session_start();

// If token exists
if (!empty($_SESSION['token'])) {
	// Include Yellow Elephant API Helper
  include('vericlean-api-helper.php');

  // Logout
  $logout = veLogout($_SESSION['token']);
}

// Unset all of the session variables.
$_SESSION = array();

// Destroy the session and the session data!
if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
		);
}

// Delete cookies
delete_cookies();

// Finally, destroy the session.
session_destroy();

// Redirect to login
if ($_GET['error'] != ''){
	header('Location: /login.php?error='.$_GET['error']);
} else {
	header('Location: /login.php');
}
