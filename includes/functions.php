<?php
// Start Session
session_start();

date_default_timezone_set('UTC');

if(empty($_SESSION['access-token'])) {
  header('Location: /includes/logout.php');
}

function utc_to_cst($datetime, $format = 'Y-m-d H:i:s') {
  $current_year = date('Y');
  $dst_start = date('Y-m-d H:i:s', strtotime("$current_year-04-09 00:00:00"));
  $dst_end = date('Y-m-d H:i:s', strtotime("$current_year-10-27 00:00:00"));
  $today = date('Y-m-d H:i:s');
  $time_offset = 21600;
  if($today >= $dst_start && $today <= $dst_end) {
    $time_offset = 18000;
  }
  $time = time($datetime);
  $dst = intval(date("I", $time));
  $tzOffset = intval(date('Z', time()));
  return date($format, strtotime($datetime)+ $tzOffset - $time_offset + $dst * 3600);
}

// Include the API Helper
include('vericlean-api-helper.php');
