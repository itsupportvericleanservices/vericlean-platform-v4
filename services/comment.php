<?php
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2) {
    $service = vcGetService($_SESSION['access-token'], $_GET['id']);
    $comments = vcGetComments($_SESSION['access-token']);
    $email_user = $_SESSION['uid'];
}
?>


<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

    <?php include('../includes/sidebar_menu.php');?>

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">Add Comment to Service </h3>
                </div>
            </div>
        </div>

        <!-- END: Subheader -->
        <div class="m-content">
            <!--Begin::Section-->
            <div class="row">
                <div class="col-xl-6 col-md-6">

                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="finished_at">Comment</label>
                                <textarea class="form-control" rows="5" id="comment"></textarea>
                                <input type="hidden" id="user_id" id="user_id" value="1">
                            </div>
                            <a onclick="createComments(event, <?= $_GET['id'] ?>)" class="btn btn-primary">Save</a>
                        </div>
                    </div>
                    <!--end::Portlet-->
                    <!--End::Section-->
                </div>

                <div class="col-md-6">
                    <!--begin:: Widgets/Support Tickets -->
                    <div class="m-portlet m-portlet--full-height ">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Comments
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <?php foreach ($comments as $key => $comment) { ?>
                                <?php if ($comment['service_id'] == $_GET['id']) { ?>
                                    <div class="m-widget3">
                                        <div class="m-widget3__item">
                                            <div class="m-widget3__header">
                                                <div class="m-widget3__user-img">
                                                    <img class="m-widget3__img" src="../assets/images/users/user1.jpg"
                                                        alt="">
                                                </div>
                                                <div class="m-widget3__info">
                                                    <span class="m-widget3__username">
                                                        Melania Trump
                                                    </span><br>
                                                    <span class="m-widget3__time">
                                                        2 day ago
                                                    </span>
                                                </div>
                                                <span class="m-widget3__status m--font-info">
                                                    Pending
                                                </span>
                                            </div>
                                            <div class="m-widget3__body">
                                                <p class="m-widget3__text">
                                                    <?= $comment['comment'] ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <!--end:: Widgets/Support Tickets -->
                </div>

            </div>
        </div>
        <!--END ROW-->
    </div>
    <!--End::Content-->
    <!-- end:: Page -->



    <?php include('../includes/footer.php');?>

    <!--begin::SERVICES -->
    <script src="../assets/js/comments/create.js" type="text/javascript"></script>
    <!--end::SERVICES -->