<?php
include('../includes/header.php');

?>
<style>
    .title_card_first_left{
      margin-top: 16px;
      color: #36a3f7;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_first_right{
      margin-top: 16px;
      color: #36a3f7;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .title_card_second_left{
      margin-top: 16px;
      color: #ffb822;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_second_right{
      margin-top: 16px;
      color: #ffb822;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .title_card_third_left{
      margin-top: 16px;
      color: #5867dd;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_third_right{
      margin-top: 16px;
      color: #5867dd;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .title_card_quarter_left{
      margin-top: 16px;
      color: #34bfa3;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_quarter_right{
      margin-top: 16px;
      color: #34bfa3;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .description_card{
      margin-left:0px !important;
      position: relative;
      top: 10px;
    }

    .number_card{
      font-size: 40px !important;
      float: none !important;
      margin-right: 0px !important;
    }
    .progress-margin{
      margin-top:0px !important;
    }
    /*Sticky del browser al lado derecho*/
    .m-nav-sticky{
      display: none;
    }
    .chartjs-render-monitor{
      border-radius: 0px !important;
    }
    .cleaners-options > .nav.nav-pills .nav-link, .nav.nav-tabs .nav-link{
      color: black;
    }
    @media (max-width: 1186px) and (min-width: 768px) {
      .description_card_second{
        font-size: 0.7em !important;
      }
      .title_card_second_left, .title_card_second_right{
        font-size: 1em !important;
      }
    }
    @media (max-width: 768px) {
      .cards-info-desktop {
        display: none;
      }
      .head-canvas{
        padding: 5px !important;
      }
      .number_card_right{
        right: 10% !important;
      }
      .number_card_second_right{
        right: 4% !important;
      }
      .number_card_second_center{
        right: 32% !important;
      }
      .description_card_second_left{
        left: 0 !important;
      }
    }
    @media (max-width: 560px) {
      .cleaners-items-desktop{
        display: none;
      }
    }
    @media (max-width: 500px) {
      .cleaner-card{
        padding-top: 5px !important;
        padding-left: 0px !important;
        padding-right: 15px !important;
      }
      .img-cleaner{
        min-width:120px !important;
        margin-right: 20px;
        margin-top: -25px;
      }
      .title-data-cleaners{
        font-size: 0.6em;
      }
    }
  </style>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Report Checkins by supervisor</h3>
          
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      
      <div class="row">
        <div class="col-xl-12">

          <!--begin::Portlet-->
          <div class="m-portlet" id="m-portlet__body">

          </div>
          <div id="cont" class="m-portlet">
          
          </div>
          <!--end::Portlet-->
          <!--End::Section-->
        </div>
      </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>
  <!--End::Content-->
  <!-- end:: Page -->

  <!-- begin::Quick Sidebar -->



  <?php include('../includes/footer.php');?>

<script src="../assets/moment.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="../assets/js/sup_checks/index.js"></script>
<script>

  document.addEventListener('DOMContentLoaded', getSupChecks('home'))
</script>