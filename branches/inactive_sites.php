<?php
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 ||  $_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5) {
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Inactive Sites</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin::Portlet-->
                <div class="m-portlet">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                  Filters
                                  <div>
                                    <div style="float:left; margin-left:10px">
                                      <select name="filter_by" class="filter_by form-control" onchange="branches_filter_by();">
                                        <option value="" selected>Show All</option>
                                        <option value="client">Client</option>
                                        <option value="customer">Customer</option>
                                        <option value="customer_for_client">Customers for Client</option>
                                        <option value="cleaner">Cleaner</option>
                                        <option value="state">State</option>
                                        <option value="city">City</option>
                                        <option value="address">Address</option>
                                        <option value="site_name">Site Name</option>
                                        <option value="site_code">Site code</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_custom" style="margin-left: 20px;display:none;float:left;">
                                      <input type="text" id="filter_custom" class="form-control">
                                    
                                    </div>
                                    <div class="filter_by_client" style="display:none;float:left;">
                                      <select name="client_id" class="client_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_customer" style="display:none;float:left;">
                                      <select name="customer_id" class="customer_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_cleaner" style="display:none;float:left;">
                                      <select name="cleaner_id" class="cleaner_id form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_state" style="display:none;float:left;">
                                      <select name="state" class="state form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_city" style="display:none;float:left;">
                                      <select name="city" class="city form-control">
                                        <option value="" selected>Select One</option>
                                      </select>
                                    </div>
                                    <div class="filter_by_client_for_customer" style="display:none;float:left;">
                                      <select name="client_for_customer_id" class="client_for_customer_id form-control">
                                        <option value="" selected>Select Client</option>
                                      </select>
                                    </div>
                                    <div class="filter" style="display:none;float:left; margin-left: 10px;">
                                      <button onclick="branches_filter()" class="btn btn-info">Filter</button>
                                      <button onclick="location.reload()" class="btn btn-danger">Clear</button>
                                    </div>
                                    <div style="clear:both;"></div>
                                  </div>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="create.php" class="btn btn-primary">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>New Site</span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="m-portlet__body" id="m-portlet__body_inactive">
                    <!--begin::Section-->
                    <!--end::Section-->
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                <!--End::Section-->
            </div>
        </div>
        <!--END XL12-->
    </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->

<input type="hidden" class="searchstr" name="searchstr" value="" />


<!--begin::BRANCHES -->
<script src="../assets/js/branches/delete.js" type="text/javascript"></script>
<script src="../assets/js/branches/inactive_sites.js" type="text/javascript"></script>
<!--end::BRANCHES -->

<?php include('../includes/footer.php');?>

<script>
$( document ).ready(function() {
    document.addEventListener('DOMContentLoaded',getInactiveSites(1));
});
</script>

<?php } ?>
