$(document).ready(function() {
  $("#boton1").on('click', function(e) {
    console.log('entro')
    Swal({
      title: '<h2>Are you sure you want to export data?</h2>',
      html: '<h5> This could take from 5 up to 10 minutes depending on your browser and internet speed. Therefore PLEASE WAIT and if your browser show alerts that "the page loading time is taking to long", please click on "Accept/Wait" to keep loading </h5>',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Export',
      width:'700px'
    }).then((result) => {
      if (result.value) {
        $(".loading").show()
        var url = "../router/branches/file.php";
        $.ajax({
          type: "POST",
          url: url,
          dataType: "html"
        }).done(function (data) {
          $(".loading").hide()
          
          $("#cont").html(data);
          
        }).error(function (err) {
          console.log(err)
          
        });
        }
      });
    })
})
