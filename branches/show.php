<?php
include('../includes/header.php');
$branch = vcGetBranch($_SESSION['access-token'], $_GET['id']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Site:
            <?php echo $branch['name'] ?>
          </h3>
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      <div class="row">
        <div class="col-xl-12">

          <!--Begin::Section-->
          <div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
              <div class="row m-row--no-padding m-row--col-separator-xl">

                <div class="col-md-12 col-lg-12 col-xl-4">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Client:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['client']['name'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Frequency:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?php 
																$frequencies = explode(",", $branch['frequency']); 
																foreach($frequencies as $freq){
																	if($freq == 1){
																		echo "Monday ";
																	}
																	if($freq == 2){
																		echo "Tuesday ";
																	}
																	if($freq == 3){
																		echo "Wednesday ";
																	}
																	if($freq == 4){
																		echo "Thursday ";
																	}
																	if($freq == 5){
																		echo "Friday ";
																	}
																	if($freq == 6){
																		echo "Saturday ";
																	}
																	if($freq == 7){
																		echo "Sunday ";
																	}
																}
															?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Site Type</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['site_type'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>

                <div class="col-md-12 col-lg-12 col-xl-4">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Name:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['name'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Address:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['address'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Address 2:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['address2'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>

                <div class="col-md-12 col-lg-12 col-xl-4">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Site Code</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['site_code'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">City:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['city'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">ZipCode:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['zipcode'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>

                <div class="col-md-12 col-lg-12 col-xl-4">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Phone:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['phone'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Email:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['email'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">MaxTime:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?php $time = substr($branch['max_time'], 11, 8); ?>
                            <?= $time ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>

                <div class="col-md-12 col-lg-12 col-xl-4">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Latitude:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['latitude'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Longitude:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['longitude'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                </div>

                <div class="col-md-12 col-lg-12 col-xl-4">
                  <!--begin:: Widgets/Stats2-1 -->
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Cleaner:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['cleaner']['first_name']. " " . $branch['cleaner']['last_name'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Area Manager:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['area_manager']['first_name']. " " . $branch['area_manager']['last_name'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Supervisor:</span>
                          <h3 class="m-widget1__title m--font-brand">
                            <?= $branch['supervisor']['first_name']. " " . $branch['supervisor']['last_name'] ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end:: Widgets/Stats2-1 -->
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!--END XL12-->
    </div>
    <h3 style="margin:25px;">Location in map</h3>
    <div id="dvMap" style="width: 100%; height: 500px"></div>
    <!--END ROW-->
  </div>
  <!--End::Content-->
  <!-- end:: Page -->

  <?php include('../includes/footer.php');?>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo86o_PxdPQqtFTP8_byHVF8ExqWfnqP4&callback=initMap"
    async defer></script>
  <script>
  const branch = <?php echo json_encode($branch) ?>;
 
   function initMap() {
    var location = new google.maps.LatLng(Number(branch.latitude), Number(branch.longitude));
      map = new google.maps.Map(document.getElementById('dvMap'), {
        center: {
          lat: Number(branch.latitude),
          lng: Number(branch.longitude)
        },
        zoom: 13
      });

        
        //Create a marker and placed it on the map.
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
       
      
   }
      
    
  </script>