<?php  
include('../includes/header.php'); 
$roles = vcGetRoles($_SESSION['access-token']);
$cleaner = vcGetCleaner($_SESSION['access-token'], $_GET['id']);
$area_manager = vcGetAreaManager($_SESSION['access-token'], $_GET['id']);
$supervisor = vcGetSupervisor($_SESSION['access-token'], $_GET['id']);

//SEND ENDPOINT CONFIGURATION TO UPDATE JS FOR COMPLETE THE AJAX URL
if($cleaner!=null){
    $endpoint = 'cleaners';
}
if($area_manager!=null){
    $endpoint = 'area_managers';
}
if($supervisor!=null){
    $endpoint = 'supervisors';
}

?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">Cleaner: 
            <?php 
                if($cleaner!=null){
                    echo $cleaner['first_name'].' '.$cleaner['last_name'];
                }
                if($area_manager!=null){
                    echo $area_manager['first_name'].' '.$area_manager['last_name'];
                }
                if($supervisor!=null){
                    echo $supervisor['first_name'].' '.$supervisor['last_name'];
                }
            ?> 
            </h3>
        </div>
    </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">

            <!--begin::Portlet-->
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <input type="hidden" id="endpoint" value="<?php echo $endpoint; ?>">

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="first_name">First Name</label>
                                <p id="first_name_error" style="color:red; display:none">This field is required</p>
                                <input type="text" class="form-control m-input" id="first_name" placeholder="First Name" 
                                value="<?php 
                                    if($cleaner!=null){
                                        echo $cleaner['first_name'];
                                    }
                                    if($area_manager!=null){
                                        echo $area_manager['first_name'];
                                    }
                                    if($supervisor!=null){
                                        echo $supervisor['first_name'];
                                    }
                                ?> ">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="last_name">Last Name</label>
                                <p id="last_name_error" style="color:red; display:none">This field is required</p>
                                <input type="text" class="form-control m-input" id="last_name" placeholder="Last Name"  
                                value="<?php 
                                    if($cleaner!=null){
                                        echo $cleaner['last_name'];
                                    }
                                    if($area_manager!=null){
                                        echo $area_manager['last_name'];
                                    }
                                    if($supervisor!=null){
                                        echo $supervisor['last_name'];
                                    }
                                ?> ">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="password">Username</label>
                                <p id="username_error" style="color:red; display:none">This field is required</p>
                                <input type="text" class="form-control m-input" pattern="[a-z\d]*" id="username" placeholder="Username"  
                                value="<?php 
                                    if($cleaner!=null){
                                        echo $cleaner['username'];
                                    }
                                    if($area_manager!=null){
                                        echo $area_manager['username'];
                                    }
                                    if($supervisor!=null){
                                        echo $supervisor['username'];
                                    }
                                ?> ">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="text">Email</label>
                                <p id="email_error" style="color:red; display:none">This field is required</p>
                                <input type="text" class="form-control m-input" id="email" 
                                value="<?php 
                                    if($cleaner!=null){
                                        echo $cleaner['email'];
                                    }
                                    if($area_manager!=null){
                                        echo $area_manager['email'];
                                    }
                                    if($supervisor!=null){
                                        echo $supervisor['email'];
                                    }
                                ?> ">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="password">Password</label>
                                <p id="password_error" style="color:red; display:none">This field is required</p>
                                <input type="password" class="form-control m-input" id="password"
                                    placeholder="Password">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="password">Password Repeat</label>
                                <p id="password_repeat_error" style="color:red; display:none">This field is required</p>
                                <p id="password_not_equal" style="color:red; display:none">The password should be equal to password repeat</p>
                                <input type="password" class="form-control m-input" id="password_repeat"
                                    placeholder="Password Repeat">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="text">Phone</label>
                                <p id="phone_error" style="color:red; display:none">This field is required</p>
                                <input type="text" class="form-control m-input" id="phone" placeholder="Phone"  
                                value="<?php 
                                    if($cleaner!=null){
                                        echo $cleaner['phone'];
                                    }
                                    if($area_manager!=null){
                                        echo $area_manager['phone'];
                                    }
                                    if($supervisor!=null){
                                        echo $supervisor['phone'];
                                    }
                                ?> ">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="text">Address</label>
                                <p id="address_error" style="color:red; display:none">This field is required</p>
                                <input type="text" class="form-control m-input" id="address" placeholder="Address"  
                                value="<?php 
                                    if($cleaner!=null){
                                        echo $cleaner['address'];
                                    }
                                    if($area_manager!=null){
                                        echo $area_manager['address'];
                                    }
                                    if($supervisor!=null){
                                        echo $supervisor['address'];
                                    }
                                ?> ">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="roles">Roles</label>                      
                                <select class="form-control m-input" id="role_id" name="role_id">  
                                    <?php foreach ($roles as $key => $rol) { ?> 
                                        <?php if($rol['name'] != 'superadmin' && $rol['name'] != 'admin' && $rol['name'] != 'staff' && $rol['name'] != 'dispatcher' && $rol['name'] != 'client' && $rol['name'] != 'user')  { ?>                             
                                            <option value="<?php echo $rol['id'] ?>" 
                                                <?php if($cleaner!=null){ ?>                                      
                                                    <?php if($cleaner['role']['id'] == $rol['id']){ ?>
                                                        selected
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php if($area_manager!=null){ ?>                                      
                                                    <?php if($area_manager['role']['id'] == $rol['id']){ ?>
                                                        selected
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php if($supervisor!=null){ ?>                                      
                                                    <?php if($supervisor['role']['id'] == $rol['id']){ ?>
                                                        selected
                                                    <?php } ?>
                                                <?php } ?>
                                            ><?php echo $rol['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>                
                            </div>                          
                            <div class="form-group m-form__group">
                                <label for="status">Status</label>

                                <?php if($cleaner!=null){ ?> 

                                    <select class="form-control m-input" id="status" name="status">
                                        <?php if($cleaner['status'] == true) { ?>
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        <?php }else{  ?>
                                            <option value="0" selected>Inactive</option>
                                            <option value="1">Active</option>
                                        <?php } ?>
                                    </select>

                                <?php } ?>
                                <?php if($area_manager!=null){ ?> 

                                    <select class="form-control m-input" id="status" name="status">
                                        <?php if($area_manager['status'] == true) { ?>
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        <?php }else{  ?>
                                            <option value="0" selected>Inactive</option>
                                            <option value="1">Active</option>
                                        <?php } ?>
                                    </select>

                                <?php } ?>
                                <?php if($supervisor!=null){ ?> 

                                    <select class="form-control m-input" id="status" name="status">
                                        <?php if($supervisor['status'] == true) { ?>
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        <?php }else{  ?>
                                            <option value="0" selected>Inactive</option>
                                            <option value="1">Active</option>
                                        <?php } ?>
                                    </select>

                                <?php } ?>

                            </div>
                            <div class="form-group m-form__group">
                                <input type="file" class="m-input" id="image"
                                    placeholder="Image">
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <p id="errors" style="color:red; display:none">Some fields are required - check UP</p>
                                <a onclick="updateCleaner(event, <?= $_GET['id'] ?>)" class="btn btn-primary">Update</a>
                                <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
    <!--END XL12-->
</div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->

<?php include('../includes/footer.php');?>

<!--begin::CLEANERS -->
<script src="../assets/js/cleaners/update.js" type="text/javascript"></script>
<!--end::CLEANERS -->