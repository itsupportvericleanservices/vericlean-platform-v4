<?php
date_default_timezone_set('America/Chicago');
include('../includes/header.php');
if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 3 || $_SESSION['role_id'] == 4 || $_SESSION['role_id'] == 5) {
    $cleaners = vcGetCleaners($_SESSION['access-token']);
    $area_managers = vcGetAreaManagers($_SESSION['access-token']);
    $supervisors = vcGetSupervisors($_SESSION['access-token']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Cleaners</h3>
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content" id="indexCleaners">
      <!--Begin::Section-->
      <div class="row">
        <div class="col-xl-12">

          <!--begin::Portlet-->
          <div class="m-portlet">

            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    Options
                  </h3>
                </div>
              </div>
              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                  <div style="margin-right: 10px;background: gainsboro;border-radius: 3px;padding: 5px;">
                      <span>
                          <i class="la la-search"></i>
                          <input @keyup="filter_users" v-model="user" type="text" placeholder="search by name"></input>
                      </span>
                  </div>
                    <a href="create.php" class="btn btn-primary">
                      <span>
                        <i class="la la-plus"></i>
                        <span>New Employee</span>
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <div class="m-portlet__body">

              <!--begin::Section-->

              <div class="row" v-if="user == ''">
                <?php
                            foreach ($cleaners as $key => $cleaner) {
                        ?>
                <div class="col-md-4" style="margin-top: 20px;">
                  <div class="card" style="width: 18rem;">
                    <!-- <img class="card-img-top" src="../assets/images/users/user4.jpg" alt="Card image cap"> -->
                    <div class="card-body">
                      <h5 class="card-title">
                        <?= $cleaner['first_name'].' '.$cleaner['last_name'] ?>
                        <span class="dropdown" style="float:right">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                            data-toggle="dropdown" aria-expanded="true">
                            <i class="la la-ellipsis-h"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="show.php?id=<?= $cleaner['id'] ?>"><i
                                class="la la-crosshairs"></i>
                              View</a>
                            <a class="dropdown-item" href="update.php?id=<?= $cleaner['id'] ?>"><i
                                class="la la-edit"></i>
                              Update</a>
                            <a onclick="deleteCleaner(event, <?= $cleaner['id'] ?>, 'cleaners')"
                              class="dropdown-item"><i class="la la-trash"></i>
                              Delete</a>
                            <?php if(!empty($cleaner['device_token'])) { ?>
                              <a class="dropdown-item" onclick="sendPush(event, '<?= $cleaner['device_token']; ?>')"><i class="la la-bell"></i>Send Notification</a>
                            <?php } else { ?>
                              <a class="dropdown-item disable"><i class="la la-bell"></i>Send Notification not available</a>
                            <?php } ?>
                          </div>
                        </span>
                      </h5>
                      <p class="card-text" style="margin:0;"><strong>Role:</strong> <?= $cleaner['role']['name'] ?></p>
                      <p class="card-text" style="margin:0"><strong>Phone:</strong> <?= $cleaner['phone'] ?></p>
                      <p class="card-text" style="margin:0;"><strong>Address:</strong> <?= $cleaner['address'] ?></p>
                      <p class="card-text" style="margin:0;">
                        <strong>Status:</strong>
                        <?php if($cleaner['status'] == 1){ ?>
                        Active
                        <?php }else {?>
                        Inactive
                        <?php } ?>
                      </p>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <?php
                            foreach ($area_managers as $key => $area) {
                        ?>
                <div class="col-md-4" style="margin-top: 20px;">
                  <div class="card" style="width: 18rem;">
                    <!-- <img class="card-img-top" src="../assets/images/users/user4.jpg" alt="Card image cap"> -->
                    <div class="card-body">
                      <h5 class="card-title">
                        <?= $area['first_name'].' '.$area['last_name'] ?>
                        <span class="dropdown" style="float:right">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                            data-toggle="dropdown" aria-expanded="true">
                            <i class="la la-ellipsis-h"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="show.php?id=<?= $area['id'] ?>"><i
                                class="la la-crosshairs"></i>
                              View</a>
                            <a class="dropdown-item" href="update.php?id=<?= $area['id'] ?>"><i class="la la-edit"></i>
                              Update</a>
                            <a onclick="deleteCleaner(event, <?= $area['id'] ?>, 'area_managers')"
                              class="dropdown-item"><i class="la la-trash"></i>
                              Delete</a>
                            <?php if(!empty($area['device_token'])) { ?>
                              <a class="dropdown-item" onclick="sendPush(event, '<?= $area['device_token']; ?>')"><i class="la la-bell"></i>Send Notification</a>
                            <?php } else { ?>
                              <a class="dropdown-item disable"><i class="la la-bell"></i>Send Notification not available</a>
                            <?php } ?>
                          </div>
                        </span>
                      </h5>
                      <p class="card-text" style="margin:0;"><strong>Role:</strong> <?= $area['role']['name'] ?></p>
                      <p class="card-text" style="margin:0"><strong>Phone:</strong> <?= $area['phone'] ?></p>
                      <p class="card-text" style="margin:0;"><strong>Address:</strong> <?= $area['address'] ?></p>
                      <p class="card-text" style="margin:0;">
                        <strong>Status:</strong>
                        <?php if($area['status'] == 1){ ?>
                        Active
                        <?php }else {?>
                        Inactive
                        <?php } ?>
                      </p>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <?php
                            foreach ($supervisors as $key => $super) {
                        ?>
                <div class="col-md-4" style="margin-top: 20px;">
                  <div class="card" style="width: 18rem;">
                    <!-- <img class="card-img-top" src="../assets/images/users/user4.jpg" alt="Card image cap"> -->
                    <div class="card-body">
                      <h5 class="card-title">
                        <?= $super['first_name'].' '.$super['last_name'] ?>
                        <span class="dropdown" style="float:right">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                            data-toggle="dropdown" aria-expanded="true">
                            <i class="la la-ellipsis-h"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="show.php?id=<?= $super['id'] ?>"><i
                                class="la la-crosshairs"></i>
                              View</a>
                            <a class="dropdown-item" href="update.php?id=<?= $super['id'] ?>"><i class="la la-edit"></i>
                              Update</a>
                            <a onclick="deleteCleaner(event, <?= $super['id'] ?>, 'supervisors')"
                              class="dropdown-item"><i class="la la-trash"></i>
                              Delete</a>
                            <?php if(!empty($super['device_token'])) { ?>
                              <a class="dropdown-item" onclick="sendPush(event, '<?= $super['device_token']; ?>')"><i class="la la-bell"></i>Send Notification</a>
                            <?php } else { ?>
                              <a class="dropdown-item disable"><i class="la la-bell"></i>Send Notification not available</a>
                            <?php } ?>
                          </div>
                        </span>
                      </h5>
                      <p class="card-text" style="margin:0;"><strong>Role:</strong> <?= $super['role']['name'] ?></p>
                      <p class="card-text" style="margin:0"><strong>Phone:</strong> <?= $super['phone'] ?></p>
                      <p class="card-text" style="margin:0;"><strong>Address:</strong> <?= $super['address'] ?></p>
                      <p class="card-text" style="margin:0;">
                        <strong>Status:</strong>
                        <?php if($super['status'] == 1){ ?>
                        Active
                        <?php }else {?>
                        Inactive
                        <?php } ?>
                      </p>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>

              <div class="row" v-if="user != ''">
                
                <div class="col-md-4" style="margin-top: 20px;" v-for="user in users">
                  <div class="card" style="width: 18rem;">
                    <!-- <img class="card-img-top" src="../assets/images/users/user1.jpg" alt="Card image cap"> -->
                    <div class="card-body">
                      <h5 class="card-title">
                        {{user.first_name}} {{user.last_name}}
                        <span class="dropdown" style="float:right">
                          <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                            data-toggle="dropdown" aria-expanded="true">
                            <i class="la la-ellipsis-h"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" @click="window.location = `show.php?id=${user.id}`"><i
                                class="la la-crosshairs"></i>
                              View</a>
                            <a class="dropdown-item" @click="window.location = `update.php?id=${user.id}`"><i class="la la-edit"></i>
                              Update</a>
                            <a @click="_deleteUser(event, user.id)" class="dropdown-item"><i
                                class="la la-trash"></i>
                              Delete</a>
                          </div>
                        </span>
                      </h5>
                      <p class="card-text" style="margin:0"><strong>Phone:</strong> {{user.phone}}</p>
                      <p class="card-text" style="margin:0;"><strong>Address:</strong> {{user.address}} </p>
                      <p class="card-text" style="margin:0;"><strong>Role:</strong> {{user.role.name}}</p>
                      <p class="card-text" style="margin:0;">
                        <strong>Status:</strong> {{user.status == 1 ? "Active" : "inactive"}}
                        
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <!--end::Section-->
            </div>

            <!--end::Form-->
          </div>
          <!--end::Portlet-->
          <!--End::Section-->
        </div>
      </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>
  <!--End::Content-->
  <!-- end:: Page -->



  <!-- begin::Quick Sidebar -->
  <div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
    <div class="m-quick-sidebar__content m--hide">
      <span id="m_quick_sidebar_close" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
      <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger"
            role="tab">Messages</a>
        </li>
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings"
            role="tab">Settings</a>
        </li>
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
          <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
            <div class="m-messenger__messages m-scrollable">
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/images//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Hi Bob. What time will be the meeting ?
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Hi Megan. It's at 2.30PM
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/images//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Will the development team be joining ?
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Yes sure. I invited them as well
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__datetime">2:30PM</div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/images//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Noted. For the Coca-Cola Mobile App project as well ?
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Yes, sure.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Please also prepare the quotation for the Loop CRM project as well.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__datetime">3:15PM</div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-no-pic m--bg-fill-danger">
                    <span>M</span>
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Noted. I will prepare it.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Thanks Megan. I will see you later.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/images//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Sure. See you in the meeting soon.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-messenger__seperator"></div>
            <div class="m-messenger__form">
              <div class="m-messenger__form-controls">
                <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
              </div>
              <div class="m-messenger__form-tools">
                <a href="" class="m-messenger__form-attachment">
                  <i class="la la-paperclip"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
          <div class="m-list-settings m-scrollable">
            <div class="m-list-settings__group">
              <div class="m-list-settings__heading">General Settings</div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Email Notifications</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" checked="checked" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Site Tracking</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">SMS Alerts</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Backup Storage</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Audit Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" checked="checked" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
            </div>
            <div class="m-list-settings__group">
              <div class="m-list-settings__heading">System Settings</div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">System Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Error Reporting</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Applications Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Backup Servers</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" checked="checked" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Audit Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
          <div class="m-list-timeline m-scrollable">
            <div class="m-list-timeline__group">
              <div class="m-list-timeline__heading">
                System Logs
              </div>
              <div class="m-list-timeline__items">
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">12 new users registered <span
                      class="m-badge m-badge--warning m-badge--wide">important</span></a>
                  <span class="m-list-timeline__time">Just now</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">System shutdown</a>
                  <span class="m-list-timeline__time">11 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                  <a href="" class="m-list-timeline__text">New invoice received</a>
                  <span class="m-list-timeline__time">20 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                  <a href="" class="m-list-timeline__text">Database overloaded 89% <span
                      class="m-badge m-badge--success m-badge--wide">resolved</span></a>
                  <span class="m-list-timeline__time">1 hr</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">System error</a>
                  <span class="m-list-timeline__time">2 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">Production server down <span
                      class="m-badge m-badge--danger m-badge--wide">pending</span></a>
                  <span class="m-list-timeline__time">3 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">Production server up</a>
                  <span class="m-list-timeline__time">5 hrs</span>
                </div>
              </div>
            </div>
            <div class="m-list-timeline__group">
              <div class="m-list-timeline__heading">
                Applications Logs
              </div>
              <div class="m-list-timeline__items">
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">New order received <span
                      class="m-badge m-badge--info m-badge--wide">urgent</span></a>
                  <span class="m-list-timeline__time">7 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">12 new users registered</a>
                  <span class="m-list-timeline__time">Just now</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">System shutdown</a>
                  <span class="m-list-timeline__time">11 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                  <a href="" class="m-list-timeline__text">New invoices received</a>
                  <span class="m-list-timeline__time">20 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                  <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                  <span class="m-list-timeline__time">1 hr</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">System error <span
                      class="m-badge m-badge--info m-badge--wide">pending</span></a>
                  <span class="m-list-timeline__time">2 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">Production server down</a>
                  <span class="m-list-timeline__time">3 hrs</span>
                </div>
              </div>
            </div>
            <div class="m-list-timeline__group">
              <div class="m-list-timeline__heading">
                Server Logs
              </div>
              <div class="m-list-timeline__items">
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">Production server up</a>
                  <span class="m-list-timeline__time">5 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">New order received</a>
                  <span class="m-list-timeline__time">7 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">12 new users registered</a>
                  <span class="m-list-timeline__time">Just now</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">System shutdown</a>
                  <span class="m-list-timeline__time">11 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                  <a href="" class="m-list-timeline__text">New invoice received</a>
                  <span class="m-list-timeline__time">20 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                  <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                  <span class="m-list-timeline__time">1 hr</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">System error</a>
                  <span class="m-list-timeline__time">2 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">Production server down</a>
                  <span class="m-list-timeline__time">3 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">Production server up</a>
                  <span class="m-list-timeline__time">5 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">New order received</a>
                  <span class="m-list-timeline__time">1117 hrs</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end::Quick Sidebar -->

  <!--begin::CLEANERS-->
  <script src="../assets/js/cleaners/delete.js" type="text/javascript"></script>
  <!--end::CLEANERS-->
  <script src="../assets/js/vue.min.js"></script>
  <script>
    const cleaners = <?php echo json_encode($cleaners) ?>;
    const area_managers = <?php echo json_encode($area_managers) ?>;
    const supervisors = <?php echo json_encode($supervisors) ?>;
    let users = [...cleaners,...area_managers,...supervisors]  
    console.log(users);                             
  new Vue({
    el:'#indexCleaners',
    data:{
      showAll: true,
      users: '',
      user:""
    },
    computed: {
      stg(){
        let string = this.user.toLowerCase()
          return string.charAt(0).toUpperCase() + string.slice(1);
        }
    },
    methods: {
      filter_users(){
        this.users = users.filter(el=>{
         return   el.first_name.includes(this.stg) || el.last_name.includes(this.stg) 
         
        })
        
      },
      _deleteUser(event, id) {
        event.preventDefault();

        Swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            $.ajax({
              type: "POST",
              url: "../../../router/users/delete.php?id=" + id,
              dataType: "json"
            }).done(function (data) {
              if (data == true) {
                swal({
                  title: "Success!",
                  text: "User Deleted",
                  icon: "success",
                  button: "Ok",
                  closeOnEsc: true
                }).then(result => {
                  location.reload()
                });
              } else {
                Swal({
                  type: 'error',
                  title: 'You can´t delete the user',
                  text: 'The user is in use',
                })
              }
            }).error(function (err) {
              swal({
                title: 'Error!',
                text: 'Something happened!',
                type: 'error',
                confirmButtonText: 'Cool'
              })
            });
          }

        })
      }
    }
  })
  </script>
  <?php include('../includes/footer.php');?>

  

  <?php } ?>