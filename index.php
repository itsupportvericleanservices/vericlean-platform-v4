<?php 
include('includes/header.php');
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('includes/sidebar_menu.php');?>

<?php if ($_SESSION['role_id'] == 1) { ?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader" style="margin-bottom: 20px">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">Dashboard</h3>
      </div>
      <div>
        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
          <span class="m-subheader__daterange-label">
            <span class="m-subheader__daterange-title"></span>
            <span class="m-subheader__daterange-date m--font-brand"></span>
          </span>
          <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
            <i class="la la-angle-down"></i>
          </a>
        </span>
      </div>
    </div>
  </div>


  <style>
    .title_card_first_left{
      margin-top: 16px;
      color: #36a3f7;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_first_right{
      margin-top: 16px;
      color: #36a3f7;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .title_card_second_left{
      margin-top: 16px;
      color: #ffb822;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_second_right{
      margin-top: 16px;
      color: #ffb822;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .title_card_third_left{
      margin-top: 16px;
      color: #5867dd;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_third_right{
      margin-top: 16px;
      color: #5867dd;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .title_card_quarter_left{
      margin-top: 16px;
      color: #34bfa3;
      text-align: left;
      margin-left: 10px;
      position: absolute;
      left: 0px;
    }
    .title_card_quarter_right{
      margin-top: 16px;
      color: #34bfa3;
      text-align: right;
      margin-right: 10px;
      position: absolute;
      right: 0px;
    }
    .description_card{
      margin-left:0px !important;
      position: relative;
      top: 10px;
    }

    .number_card{
      font-size: 40px !important;
      float: none !important;
      margin-right: 0px !important;
    }
    .progress-margin{
      margin-top:0px !important;
    }
    /*Sticky del browser al lado derecho*/
    .m-nav-sticky{
      display: none;
    }
    .chartjs-render-monitor{
      border-radius: 0px !important;
    }
    .cleaners-options > .nav.nav-pills .nav-link, .nav.nav-tabs .nav-link{
      color: black;
    }
    @media (max-width: 1186px) and (min-width: 768px) {
      .description_card_second{
        font-size: 0.7em !important;
      }
      .title_card_second_left, .title_card_second_right{
        font-size: 1em !important;
      }
    }
    @media (max-width: 768px) {
      .cards-info-desktop {
        display: none;
      }
      .head-canvas{
        padding: 5px !important;
      }
      .number_card_right{
        right: 10% !important;
      }
      .number_card_second_right{
        right: 4% !important;
      }
      .number_card_second_center{
        right: 32% !important;
      }
      .description_card_second_left{
        left: 0 !important;
      }
    }
    @media (max-width: 560px) {
      .cleaners-items-desktop{
        display: none;
      }
    }
    @media (max-width: 500px) {
      .cleaner-card{
        padding-top: 5px !important;
        padding-left: 0px !important;
        padding-right: 15px !important;
      }
      .img-cleaner{
        min-width:120px !important;
        margin-right: 20px;
        margin-top: -25px;
      }
      .title-data-cleaners{
        font-size: 0.6em;
      }
    }
  </style>

  <!--begin::row-->
  <div class="m-content">

    <div class="m-grid__item m-grid__item--fluid m-wrapper cards-info">
      <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
          <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_first_left">
                      Services
                    </h5><br>
                    <h5 class="title_card_first_right">
                      Today
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                      Scheduled
                    </span>
                    <span class="m-widget24__desc" style="position:absolute; right:10%; top: 35px;">
                      Finished
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      500
                    </span>
                    <span class="m-widget24__stats m--font-info number_card_right" style="position:absolute; right:0%; top:60px; font-size: 40px;">
                      375
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 78%;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                    Progress
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                    78%
                  </span>
                </div>
              </div>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_second_left">
                      Delayed Service
                    </h5><br>
                    <h5 class="title_card_second_right">
                      Today
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc description_card_second description_card_second_left" style="position:absolute; left:-3%; top:35px">
                      In Progress
                    </span>
                    <span class="m-widget24__desc description_card_second" style="position:absolute; right:35%; top: 35px;">
                      Finished
                    </span>
                    <span class="m-widget24__desc description_card_second" style="position:absolute; right:3%; top: 35px;">
                      Pending
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      20
                    </span>
                    <span class="m-widget24__stats number_card_second_center" style="position:absolute; right:28%; top:60px; font-size: 40px;">
                      10
                    </span>
                    <span class="m-widget24__stats m--font-warning number_card_second_right" style="position:absolute; right:-3%; top:60px; font-size: 40px;">
                      12
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-warning" role="progressbar" style="width: 78%;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                    Progress
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                    78%
                  </span>
                </div>
              </div>
            </div>


            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_third_left">
                      Services
                    </h5><br>
                    <h5 class="title_card_third_right">
                      Week
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                      Scheduled
                    </span>
                    <span class="m-widget24__desc" style="position:absolute; right:10%; top: 35px;">
                      Finished
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      500
                    </span>
                    <span class="m-widget24__stats m--font-primary number_card_right" style="position:absolute; right:0%; top:60px; font-size: 40px;">
                      375
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-primary" role="progressbar" style="width: 78%;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                    Progress
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                    78%
                  </span>
                </div>
              </div>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3" style="height: 160px;">
              <div class="m-widget24">

                <div class="m-widget24__item" style="text-align:center">
                  <div class="row" style="max-height: 10px;">
                    <h5 class="title_card_quarter_left">
                      Services
                    </h5><br>
                    <h5 class="title_card_quarter_right">
                      Month
                    </h5><br>
                  </div>
                  <div class="m--space-30"></div>
                  <div class="row">
                    <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                      Scheduled
                    </span>
                    <span class="m-widget24__desc" style="position:absolute; right:10%; top: 35px;">
                      Finished
                    </span>
                  </div>
                  <div class="row">
                    <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                      500
                    </span>
                    <span class="m-widget24__stats m--font-success number_card_right" style="position:absolute; right:0%; top:60px; font-size: 40px;">
                      375
                    </span>
                  </div>
                  <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 78%;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                    Progress
                  </span>
                  <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                    78%
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-top: -20px;">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
         aria-selected="true">Day</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
         aria-selected="false">Month</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
         aria-selected="false">Year</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="row m-portlet m-portlet--bordered-semi m-portlet--full-height " style="margin-top: -20px;">
          <div class="col-md-8">
            <div>
              <div class="m-portlet__head head-canvas">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      <span style="background: #ffb822; color:#ffb822">aaa</span>&nbsp;Finished Services vs &nbsp;
                      <span style="background: #34bfa3; color:#34bfa3;">aaa</span>&nbsp;Delayed Services
                    </h3>
                  </div>
                </div>
              </div>
              <div class="m-portlet__body" style="padding-bottom: 120px;">
                <div class="m-widget15">
                  <div class="m-widget15__chart" style="height:180px;">
                    <!--<canvas id="m_chart_sales_stats"></canvas>-->
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--end::Col-md-8-->

          <div class="col-md-4 details-canvas" style="padding: 50px 50px 50px 50px">
            <div class="m-widget15__items">
              <div class="row">
                <div class="m-widget15__item" style="width:90%; margin-bottom: 20px; margin-top: 10px;">
                  <div>
                    <span class="m-widget15__stats" style="font-size:25px;">
                      105
                    </span>
                  </div>
                  <div>
                    <span class="m-widget15__text">
                      Total of Scheduled Services
                    </span>
                  </div>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 40%;" aria-valuenow="25" aria-valuemin="0"
                     aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="m-widget15__item" style="width:90%; margin-bottom: 20px;">
                  <div>
                    <span class="m-widget15__stats" style="font-size:25px;">
                      5
                    </span>
                  </div>
                  <div>
                    <span class="m-widget15__text">
                      Total of Delayed Services
                    </span>
                  </div>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: 60%;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="m-widget15__item" style="width:90%;">
                  <div>
                    <span class="m-widget15__stats" style="font-size:25px;">
                      100
                    </span>
                  </div>
                  <div>
                    <span class="m-widget15__text">
                      Total of Finished Services
                    </span>
                  </div>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--end::Col-md-4-->
        </div>
      </div>
      <!--begin::other tab-->
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
      <!--begin::other tab-->
      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
    </div>

    <div class="m--space-40"></div>

    <!--begin::row-->
    <div class="row">
      <!--begin::col-md-6-->
      <div class="col-md-6">
        <!--begin::col-md-6-->

        <!--begin::caja-table-->
        <div>
          <div style="background-color: #34bfa3; padding: 16px 10px 4px 10px; color: white;">
            <p>
              <span style="font-weight: bold">Scheduled</span> services today
              <span style="float: right"> <a href="#" style="color:white">See More</a>
                <p>
          </div>
          <div style="background-color: white;">
            <!--begin::Portlet-->
            <div class="m-portlet">
              <div class="m-portlet__body" style="padding:10px">
                <!--begin::Section-->
                <div class="m-section">
                  <div class="m-section__content">
                    <table class="table table-hover table-responsive">
                      <thead style="background:#eee; text-align:center">
                        <tr>
                          <th>Service</th>
                          <th>Cleaner</th>
                          <th>Branch</th>
                          <th>Tasks</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody style="text-align:center">
                        <tr>
                          <th scope="row">154654</th>
                          <td>Jhon Miguel Pérez</td>
                          <td>A banck SA, Tx</td>
                          <td>5/5</td>
                          <td>
                            <span class="m-badge m-badge--warning m-badge--wide" style="color:white">Delayed</span>
                          </td>
                          <td style="text-align:center">...</td>
                        </tr>
                        <tr>
                          <th scope="row">154654</th>
                          <td>Jhon Miguel Pérez</td>
                          <td>A banck SA, Tx</td>
                          <td>5/5</td>
                          <td>
                            <span class="m-badge m-badge--success m-badge--wide" style="color:white">Finished</span>
                          </td>
                          <td style="text-align:center">...</td>
                        </tr>
                        <tr>
                          <th scope="row">154654</th>
                          <td>Jhon Miguel Pérez</td>
                          <td>A banck SA, Tx</td>
                          <td>5/5</td>
                          <td>
                            <span class="m-badge m-badge--info m-badge--wide" style="color:white">InProgress</span>
                          </td>
                          <td style="text-align:center">...</td>
                        </tr>
                        <tr>
                          <th scope="row">154654</th>
                          <td>Jhon Miguel Pérez</td>
                          <td>A banck SA, Tx</td>
                          <td>5/5</td>
                          <td>
                            <span class="m-badge m-badge--primary m-badge--wide" style="color:white">Scheduled</span>
                          </td>
                          <td style="text-align:center">...</td>
                        </tr>
                        <tr>
                          <th scope="row">154654</th>
                          <td>Jhon Miguel Pérez</td>
                          <td>A banck SA, Tx</td>
                          <td>5/5</td>
                          <td>
                            <span class="m-badge m-badge--danger m-badge--wide" style="color:white">ReScheduled</span>
                          </td>
                          <td style="text-align:center">...</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <!--end::Section-->
              </div>

              <!--end::Form-->
            </div>

            <!--end::Portlet-->
          </div>
        </div>
        <!--end::caja-table-->

        <style>
        .cleaners-card-productivity > .nav.nav-pills.m-nav-pills--btn-pill .m-tabs__link{
          color: white !important;
        }
        </style>


        <!--begin::caja-cleaners-->
        <!--begin:: Widgets/User Progress -->
        <div class="m-portlet m-portlet--full-height " style="height: 650px">
          <div class="m-portlet__head" style="background-color:#5867dd;">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text" style="color:white">
                  Cleaner Performance
                </h3>
              </div>
            </div>
            <div class="m-portlet__head-tools cleaners-card-productivity">
              <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm"
               role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget4_tab1_content" role="tab">
                    Today
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab2_content" role="tab">
                    Week
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab3_content" role="tab">
                    Month
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">
            <div class="tab-content">
              <div class="tab-pane active" id="m_widget4_tab1_content">
                <div class="m-widget4 m-widget4--progress">
                  <div class="m-widget4__item">
                    <div class="m-widget4__img m-widget4__img--pic">
                      <img src="./assets/images/users/100_4.jpg" alt="">
                    </div>
                    <div class="m-widget4__info">
                      <span class="m-widget4__title">
                        Anna Strong
                      </span><br>
                      <span class="m-widget4__sub">
                        Bank of America branch #1234
                      </span>
                    </div>
                    <div class="m-widget4__progress">
                      <div class="m-widget4__progress-wrapper">
                        <span class="m-widget17__progress-number">63%</span>
                        <span class="m-widget17__progress-label">Productivity</span>
                        <div class="progress m-progress--sm progress-margin">
                          <div class="progress-bar m--bg-info" role="progressbar" style="width: 78%;" aria-valuenow="50"
                           aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget4__ext">

                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__img m-widget4__img--pic">
                      <img src="./assets/images/users/100_14.jpg" alt="">
                    </div>
                    <div class="m-widget4__info">
                      <span class="m-widget4__title">
                        Milano Esco
                      </span><br>
                      <span class="m-widget4__sub">
                        Product Designer, Apple Inc
                      </span>
                    </div>
                    <div class="m-widget4__progress">
                      <div class="m-widget4__progress-wrapper">
                        <span class="m-widget17__progress-number">33%</span>
                        <span class="m-widget17__progress-label">Productivity</span>
                        <div class="progress m-progress--sm">
                          <div class="progress-bar bg-success" role="progressbar" style="width: 33%;" aria-valuenow="45"
                           aria-valuemin="0" aria-valuemax="33"></div>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget4__ext">

                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__img m-widget4__img--pic">
                      <img src="./assets/images/users/100_11.jpg" alt="">
                    </div>
                    <div class="m-widget4__info">
                      <span class="m-widget4__title">
                        Nick Bold
                      </span><br>
                      <span class="m-widget4__sub">
                        Web Developer, Facebook Inc
                      </span>
                    </div>
                    <div class="m-widget4__progress">
                      <div class="m-widget4__progress-wrapper">
                        <span class="m-widget17__progress-number">13%</span>
                        <span class="m-widget17__progress-label">Productivity</span>
                        <div class="progress m-progress--sm">
                          <div class="progress-bar bg-brand" role="progressbar" style="width: 13%;" aria-valuenow="65"
                           aria-valuemin="0" aria-valuemax="13"></div>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget4__ext">

                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__img m-widget4__img--pic">
                      <img src="./assets/images/users/100_1.jpg" alt="">
                    </div>
                    <div class="m-widget4__info">
                      <span class="m-widget4__title">
                        Wiltor Delton
                      </span><br>
                      <span class="m-widget4__sub">
                        Project Manager, Amazon Inc
                      </span>
                    </div>
                    <div class="m-widget4__progress">
                      <div class="m-widget4__progress-wrapper">
                        <span class="m-widget17__progress-number">93%</span>
                        <span class="m-widget17__progress-label">Productivity</span>
                        <div class="progress m-progress--sm">
                          <div class="progress-bar bg-danger" role="progressbar" style="width: 93%;" aria-valuenow="25"
                           aria-valuemin="0" aria-valuemax="93"></div>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget4__ext">

                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__img m-widget4__img--pic">
                      <img src="./assets/images/users/100_6.jpg" alt="">
                    </div>
                    <div class="m-widget4__info">
                      <span class="m-widget4__title">
                        Sam Stone
                      </span><br>
                      <span class="m-widget4__sub">
                        Project Manager, Kilpo Inc
                      </span>
                    </div>
                    <div class="m-widget4__progress">
                      <div class="m-widget4__progress-wrapper">
                        <span class="m-widget17__progress-number">50%</span>
                        <span class="m-widget17__progress-label">Productivity</span>
                        <div class="progress m-progress--sm">
                          <div class="progress-bar bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50"
                           aria-valuemin="0" aria-valuemax="50"></div>
                        </div>
                      </div>
                    </div>
                    <div class="m-widget4__ext">

                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="m_widget4_tab2_content">
              </div>
              <div class="tab-pane" id="m_widget4_tab3_content">
              </div>
            </div>
          </div>
        </div>

        <!--end:: Widgets/User Progress -->
        <!--end::caja-cleaners-->

      </div>
      <!--end::col-md-6-->


      <!--begin::col-md-6-->
      <div class="col-md-6">
        <!--begin:: Widgets/Support Tickets -->
        <div class="m-portlet m-portlet--full-height ">
          <div class="m-portlet__head" style="height:53.4px; background:#575962;">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-text" style="color:white">
                  Comments
                </span>
              </div>
            </div>
            <div class="m-portlet__head-tools">
              <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                 m-dropdown-toggle="hover" aria-expanded="true">
                  <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl m-dropdown__toggle">
                    <i class="la la-ellipsis-h m--font-brand" style="color:white !important"></i>
                  </a>
                  <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__body">
                        <div class="m-dropdown__content">
                          <ul class="m-nav">
                            <li class="m-nav__item">
                              <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-share"></i>
                                <span class="m-nav__link-text">Activity</span>
                              </a>
                            </li>
                            <li class="m-nav__item">
                              <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                <span class="m-nav__link-text">Messages</span>
                              </a>
                            </li>
                            <li class="m-nav__item">
                              <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-info"></i>
                                <span class="m-nav__link-text">FAQ</span>
                              </a>
                            </li>
                            <li class="m-nav__item">
                              <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                <span class="m-nav__link-text">Support</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">
            <div class="m-widget3">
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="./assets/images/users/user1.jpg" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      Melania Trump
                    </span> of Service #12121 Cesar Gajardo A. Bank 78006 <br>
                    <span class="m-widget3__time">
                      Today. 9:00am - 27.11.2018
                    </span>
                  </div>
                  <span class="m-widget3__status">
                    <a href="#" style="color:#575962; border: 1px solid #eee; padding: 5px; ">Details</a>
                  </span>
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text" style="background:#eee; padding:10px">
                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet
                    doloremagna aliquam erat volutpat.
                  </p>
                </div>
              </div>
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="./assets/images/users/user4.jpg" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      Lebron King James
                    </span> of Service #12121 Cesar Gajardo A. Bank 78006 <br>
                    <span class="m-widget3__time">
                      1 day ago
                    </span>
                  </div>
                  <span class="m-widget3__status">
                    <a href="#" style="color:#575962; border: 1px solid #eee; padding: 5px; ">Details</a>
                  </span>
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text" style="background:#eee; padding:10px">
                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet
                    doloremagna aliquam erat volutpat.
                  </p>
                </div>
              </div>
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="./assets/images/users/user5.jpg" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      Deb Gibson
                    </span> of Service #12121 Cesar Gajardo A. Bank 78006 <br>
                    <span class="m-widget3__time">
                      3 weeks ago
                    </span>
                  </div>
                  <span class="m-widget3__status">
                    <a href="#" style="color:#575962; border: 1px solid #eee; padding: 5px; ">Details</a>
                  </span>
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text" style="background:#eee; padding:10px">
                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet
                    doloremagna aliquam erat volutpat.
                  </p>
                </div>
              </div>
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="./assets/images/users/user5.jpg" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      Deb Gibson
                    </span> of Service #12121 Cesar Gajardo A. Bank 78006 <br>
                    <span class="m-widget3__time">
                      3 weeks ago
                    </span>
                  </div>
                  <span class="m-widget3__status">
                    <a href="#" style="color:#575962; border: 1px solid #eee; padding: 5px; ">Details</a>
                  </span>
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text" style="background:#eee; padding:10px">
                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet
                    doloremagna aliquam erat volutpat.
                  </p>
                </div>
              </div>
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="./assets/images/users/user5.jpg" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      Deb Gibson
                    </span> of Service #12121 Cesar Gajardo A. Bank 78006 <br>
                    <span class="m-widget3__time">
                      3 weeks ago
                    </span>
                  </div>
                  <span class="m-widget3__status">
                    <a href="#" style="color:#575962; border: 1px solid #eee; padding: 5px; ">Details</a>
                  </span>
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text" style="background:#eee; padding:10px">
                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet
                    doloremagna aliquam erat volutpat.
                  </p>
                </div>
              </div>
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="./assets/images/users/user5.jpg" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      Deb Gibson
                    </span> of Service #12121 Cesar Gajardo A. Bank 78006 <br>
                    <span class="m-widget3__time">
                      3 weeks ago
                    </span>
                  </div>
                  <span class="m-widget3__status">
                    <a href="#" style="color:#575962; border: 1px solid #eee; padding: 5px; ">Details</a>
                  </span>
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text" style="background:#eee; padding:10px">
                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet
                    doloremagna aliquam erat volutpat.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <button class="btn btn-success" style="width:100%"><i class="fa fa-arrow-down"></i> Show More</button>
        </div>
        <!--end:: Widgets/Support Tickets -->
      </div>
      <!--end::col-md-6-->
    </div>
    <!--end::row-->
  </div>
  <!--end::content-->





  <!-- begin::Quick Sidebar -->
  <div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
    <div class="m-quick-sidebar__content m--hide">
      <span id="m_quick_sidebar_close" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
      <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">Messages</a>
        </li>
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">Settings</a>
        </li>
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
          <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
            <div class="m-messenger__messages m-scrollable">
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Hi Bob. What time will be the meeting ?
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Hi Megan. It's at 2.30PM
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Will the development team be joining ?
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Yes sure. I invited them as well
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__datetime">2:30PM</div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Noted. For the Coca-Cola Mobile App project as well ?
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Yes, sure.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Please also prepare the quotation for the Loop CRM project as well.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__datetime">3:15PM</div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-no-pic m--bg-fill-danger">
                    <span>M</span>
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Noted. I will prepare it.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--out">
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-text">
                        Thanks Megan. I will see you later.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="m-messenger__wrapper">
                <div class="m-messenger__message m-messenger__message--in">
                  <div class="m-messenger__message-pic">
                    <img src="assets/app/media/img//users/user3.jpg" alt="" />
                  </div>
                  <div class="m-messenger__message-body">
                    <div class="m-messenger__message-arrow"></div>
                    <div class="m-messenger__message-content">
                      <div class="m-messenger__message-username">
                        Megan wrote
                      </div>
                      <div class="m-messenger__message-text">
                        Sure. See you in the meeting soon.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-messenger__seperator"></div>
            <div class="m-messenger__form">
              <div class="m-messenger__form-controls">
                <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
              </div>
              <div class="m-messenger__form-tools">
                <a href="" class="m-messenger__form-attachment">
                  <i class="la la-paperclip"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
          <div class="m-list-settings m-scrollable">
            <div class="m-list-settings__group">
              <div class="m-list-settings__heading">General Settings</div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Email Notifications</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" checked="checked" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Site Tracking</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">SMS Alerts</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Backup Storage</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Audit Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" checked="checked" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
            </div>
            <div class="m-list-settings__group">
              <div class="m-list-settings__heading">System Settings</div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">System Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Error Reporting</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Applications Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Backup Servers</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" checked="checked" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
              <div class="m-list-settings__item">
                <span class="m-list-settings__item-label">Audit Logs</span>
                <span class="m-list-settings__item-control">
                  <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                    <label>
                      <input type="checkbox" name="">
                      <span></span>
                    </label>
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
          <div class="m-list-timeline m-scrollable">
            <div class="m-list-timeline__group">
              <div class="m-list-timeline__heading">
                System Logs
              </div>
              <div class="m-list-timeline__items">
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">12 new users registered <span class="m-badge m-badge--warning m-badge--wide">important</span></a>
                  <span class="m-list-timeline__time">Just now</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">System shutdown</a>
                  <span class="m-list-timeline__time">11 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                  <a href="" class="m-list-timeline__text">New invoice received</a>
                  <span class="m-list-timeline__time">20 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                  <a href="" class="m-list-timeline__text">Database overloaded 89% <span class="m-badge m-badge--success m-badge--wide">resolved</span></a>
                  <span class="m-list-timeline__time">1 hr</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">System error</a>
                  <span class="m-list-timeline__time">2 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">Production server down <span class="m-badge m-badge--danger m-badge--wide">pending</span></a>
                  <span class="m-list-timeline__time">3 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">Production server up</a>
                  <span class="m-list-timeline__time">5 hrs</span>
                </div>
              </div>
            </div>
            <div class="m-list-timeline__group">
              <div class="m-list-timeline__heading">
                Applications Logs
              </div>
              <div class="m-list-timeline__items">
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--info m-badge--wide">urgent</span></a>
                  <span class="m-list-timeline__time">7 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">12 new users registered</a>
                  <span class="m-list-timeline__time">Just now</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">System shutdown</a>
                  <span class="m-list-timeline__time">11 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                  <a href="" class="m-list-timeline__text">New invoices received</a>
                  <span class="m-list-timeline__time">20 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                  <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                  <span class="m-list-timeline__time">1 hr</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">System error <span class="m-badge m-badge--info m-badge--wide">pending</span></a>
                  <span class="m-list-timeline__time">2 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">Production server down</a>
                  <span class="m-list-timeline__time">3 hrs</span>
                </div>
              </div>
            </div>
            <div class="m-list-timeline__group">
              <div class="m-list-timeline__heading">
                Server Logs
              </div>
              <div class="m-list-timeline__items">
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">Production server up</a>
                  <span class="m-list-timeline__time">5 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">New order received</a>
                  <span class="m-list-timeline__time">7 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">12 new users registered</a>
                  <span class="m-list-timeline__time">Just now</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">System shutdown</a>
                  <span class="m-list-timeline__time">11 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                  <a href="" class="m-list-timeline__text">New invoice received</a>
                  <span class="m-list-timeline__time">20 mins</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                  <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                  <span class="m-list-timeline__time">1 hr</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">System error</a>
                  <span class="m-list-timeline__time">2 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">Production server down</a>
                  <span class="m-list-timeline__time">3 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                  <a href="" class="m-list-timeline__text">Production server up</a>
                  <span class="m-list-timeline__time">5 hrs</span>
                </div>
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                  <a href="" class="m-list-timeline__text">New order received</a>
                  <span class="m-list-timeline__time">1117 hrs</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
  <!-- end::Quick Sidebar -->

<?php include('includes/footer.php');?>

<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script>
	window.onload = function () {

		var options = {
			animationEnabled: true,
			theme: "light2",
			title: {
				text: ""
			},
			axisX: {
				valueFormatString: "DD MMM"
			},
			axisY: {
				title: "",
			    suffix: "K",
			    minimum: 30
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				verticalAlign: "bottom",
				horizontalAlign: "left",
				dockInsidePlotArea: true,
				itemclick: toogleDataSeries
			},
			data: [{
				type: "line",
				showInLegend: true,
				name: "Finished Services",
				markerType: "square",
				xValueFormatString: "DD MMM, YYYY",
				color: "#ffb822",
				yValueFormatString: "#,##0K",
				dataPoints: [
				    { x: new Date(2017, 10, 1), y: 63 },
					{ x: new Date(2017, 10, 2), y: 69 },
					{ x: new Date(2017, 10, 3), y: 65 },
					{ x: new Date(2017, 10, 4), y: 70 },
					{ x: new Date(2017, 10, 5), y: 71 },
					{ x: new Date(2017, 10, 6), y: 65 },
					{ x: new Date(2017, 10, 7), y: 73 },
					{ x: new Date(2017, 10, 8), y: 96 },
					{ x: new Date(2017, 10, 9), y: 84 },
					{ x: new Date(2017, 10, 10), y: 85 },
					{ x: new Date(2017, 10, 11), y: 86 },
					{ x: new Date(2017, 10, 12), y: 94 },
					{ x: new Date(2017, 10, 13), y: 97 },
					{ x: new Date(2017, 10, 14), y: 86 },
					{ x: new Date(2017, 10, 15), y: 89 }
				]
			},
			{
			    type: "line",
				showInLegend: true,
				name: "Delayed Services",
				lineDashType: "dash",
				yValueFormatString: "#,##0K",
				dataPoints: [
					{ x: new Date(2017, 10, 1), y: 60 },
					{ x: new Date(2017, 10, 2), y: 57 },
					{ x: new Date(2017, 10, 3), y: 51 },
					{ x: new Date(2017, 10, 4), y: 56 },
					{ x: new Date(2017, 10, 5), y: 54 },
					{ x: new Date(2017, 10, 6), y: 55 },
					{ x: new Date(2017, 10, 7), y: 54 },
					{ x: new Date(2017, 10, 8), y: 69 },
					{ x: new Date(2017, 10, 9), y: 65 },
					{ x: new Date(2017, 10, 10), y: 66 },
					{ x: new Date(2017, 10, 11), y: 63 },
				    { x: new Date(2017, 10, 12), y: 67 },
					{ x: new Date(2017, 10, 13), y: 66 },
					{ x: new Date(2017, 10, 14), y: 56 },
					{ x: new Date(2017, 10, 15), y: 64 }
								]
				}]
			};
				$("#chartContainer").CanvasJSChart(options);

				function toogleDataSeries(e) {
					if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
						e.dataSeries.visible = false;
					} else {
						e.dataSeries.visible = true;
					}
					    e.chart.render();
					}
				}
</script>


